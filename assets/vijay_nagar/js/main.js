$(document).ready(function () {
	$(function () {
		$('.gallery-grid1 a').Chocolat();
	});
	$('.skillbar').skillBars({
		from: 0,
		speed: 4000,
		interval: 100,
		decimals: 0,
	});
	$(".scroll").click(function (event) {
		event.preventDefault();

		$('html,body').animate({
			scrollTop: $(this.hash).offset().top
		}, 1000);
	});
	
	// $(".phone").intlTelInput({
	// 	utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js",
	// 	separateDialCode: "true",
	// 	initialCountry: "IN",
	
	// });
	// $(".phone").on("countrychange", function (e, countryData) {
	// 	$(".hiddenCountry").val(countryData['dialCode']);
	// });
	
});
$(window).load(function () {
	$('.flexslider').flexslider({
		animation: "slide",
		start: function (slider) {
			$('body').removeClass('loading');
		}
	});
});
addEventListener("load", function () {
	setTimeout(hideURLbar, 0);
}, false);

function hideURLbar() {
	window.scrollTo(0, 1);
}

$(document).ready(function () {

	if(window.innerWidth > 600){
		setTimeout(function() {
		if ($('.popup-enquiry-form').length) {
			$.magnificPopup.open({
			items: {
				src: '#popupForm' 
			},
			type: 'inline'
				});
			}
		}, 900000);

	}

	$('.makeNewEnquiry').magnificPopup({
		type: 'inline',
		items: {
			src: '#popupForm'
		}

	});

	});

	function onMouseOut(event) {
		if (
			event.clientY < 50 && event.relatedTarget == null) {
			document.removeEventListener("mouseout", onMouseOut);
		
			// Show the popup
			console.log("Pop Form Shown");

			if ($('.popup-enquiry-form').length) {
				$.magnificPopup.open({
				items: {
					src: '#popupForm' 
				},
					type: 'inline'
				});
			}
		}
	}
	  
	document.addEventListener("mouseout", onMouseOut);


	$(document).ready(function () {

		var  _floatbox = $("#contact_form"), _floatbox_opener = $(".contact-opener");
		_floatbox.css("right", "-300px"); //initial contact form position
		_floatbox.css("display", "block");



		if(window.innerWidth < 600){
			_floatbox_opener.css("display", "none");
			_floatbox.css("right", "-300px");
		}

		//Contact form Opener button
		_floatbox_opener.click(function () {
			if (_floatbox.hasClass('visiable')) {
				_floatbox.animate({ "right": "-300px" }, { duration: 300 }).removeClass('visiable');		
			} else {
				_floatbox.animate({ "right": "0px" }, { duration: 300 }).addClass('visiable');			
			}
		});
		_floatbox.animate({ "top": "150px" }, { easing: "linear" }, { duration: 500 });
	});
	var $window = $(window);
	var nav = $('#contact_form');

	if ($window.scrollTop() <= 500) {
		nav.css("display", "none");
	}
	else {
		nav.css("display", "block");
	}


function chck_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


function go_digit_jsfrm(u,i, sc_url){
 
  var page ="";
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  
  var utm_campaign	= $("#utm_campaign").val();
 
   if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	if(phone.length != '10'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	} 
	
	
	
	
 	var r_ = new Array( "name","email","phone");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	


	if(typeof $("#siteurl").val()!="undefined"){
		var o=$("#siteurl").val()
	}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&campaign=" +utm_campaign,
			beforeSend:function(){
				//$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$("#frm-sbmtbtn"+i).prop('disabled', true);
			},
			success:function(e){
				
				$("#frm-sbmtbtn"+i).prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#phone_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   window.location.href = sc_url+"/success";
				   
				}
			}
		}
  
      )	  
}