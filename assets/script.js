
$(".goToArchitects").click(function(){
    location.href = "architects";
});


$(".goToFlooring").click(function(){
    location.href = "flooring";
});


$(".goToCarpentry").click(function(){
    location.href = "carpentry";
});


$(".goToBathroomRenovation").click(function(){
    location.href = "bathroom_renovation";
});


$(".goToKitchen").click(function(){
    location.href = "kitchen";
});

$(".goToFabrication").click(function(){
    location.href = "fabrication";
});

$(".goToCompound").click(function(){
    location.href = "compound_wall";
});

$(".goToContact").click(function(){
    location.href = "contact";
});

$('.goToAboutUs').click(function(){
    location.href='about_us'
})

$('.404Error').click(function(){
    location.href='error'
})

$('.goToService').click(function(){
    location.href='service'
})

$(".goToTerms").click(function(){
    window.open("terms_condition");
});

$(".goToPrivacy").click(function(){
    window.open("privacy");
});

$(".goToDisclaimer").click(function(){
    window.open("disclaimer");
});

$(".goToVijayNagar").click(function(){
    window.open("vijayanagar");
});

$(".goToInterior").click(function(){
    window.open("interiors");
});

$(".redirectButton").click(function(){
    window.open("ijay_nagar");
});

$(".goToMeadows").click(function(){
    window.open("smak_meadows");
});



$(function(){

    var current = location.pathname;
    var base_url = window.location.origin;
    var host = window.location.host;

    console.log("Host", host);

    console.log("Base URL", base_url);

    var completeURL = base_url + host;

    console.log("Complete URL",completeURL);



    if( current == "/"){
        $('.homeLink').addClass('active').siblings().removeClass('active');
    }

    if( current == "/home"){
        $('.homeLink').addClass('active').siblings().removeClass('active');
    }

    if( current == "/about_us"){
        $('.aboutLink').addClass('active').siblings().removeClass('active');
    }

    if( current == "/interior"){
        $('.interiorLink').addClass('active').siblings().removeClass('active');
    }

    if( current == "/service"){
        $('.serviceLink').addClass('active').siblings().removeClass('active');
    }
    
    if( current == "/contact"){
        $('.contactLink').addClass('active').siblings().removeClass('active');
    }

})

function jumpBg(){
   
    var delay = 1500;
    var id =0;
    
    setInterval(function() {
        if(id>3){
            id=0;
        }        

       var selector= $(`#Jumper${id}`)

       selector.removeClass('bg_blue');
        id++;

        selector= $(`#Jumper${id}`).addClass('bg_blue');
    
    }, delay);
   
    
}

function hover(ele){
    var id= ele.id;
    document.querySelector(`.${id}`).style.visibility='visible';    
}

function mouseOut(ele){
    var id= ele.id;
    document.querySelector(`.${id}`).style.visibility='hidden';    
}


function formChange(){
    var element =document.querySelector('#form_top');
    var style = window.getComputedStyle(element);
    var  position = style.getPropertyValue('right');

    if(position !==0){
        element.style.right= '0';
    }if(position === '0px'){

        element.style.right= '-264px';
    }

    console.log(position)
    


}

var $window = $(window);

if ($window.scrollTop() > 20) {          
    $(".navbar").addClass('nav_blue');
}


$("#scroll").click(function() {
    $('html, body').animate({
        scrollTop: $("#banner").offset().top
    }, 2000);
});

