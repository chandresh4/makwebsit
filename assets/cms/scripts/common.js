﻿$(document).ready(function(e) {
    $(".menu-toggle").click(function(e) {
		var togle_id =$(this).attr("data-id");
		if ( $("#togglePages" + togle_id).is( ".in" ) ) {
			$("#togglePages" + togle_id).removeClass( "in" );		 
		}else{
		    $("#togglePages" + togle_id).addClass( "in" );
		}
    });
});


function change_status(e,t,n,r,i,s)
{
	$.ajax({
		url: e+"/change_status/index/"+t+"/"+n+"/"+r+"/"+i+"/"+s,
		type: "post",
		data: "&data=send",
		beforeSend:function(){
			$("#"+s).html("Wait...")
		},
		success:function(e){
			if(e=="refresh"){
				alert("Please refresh your page and try again !!")
			}else{
				$("#"+s).html(e)
			}
		},
		error:function(e,t,n){
			alert(e.responseText)
		}
	})
}
