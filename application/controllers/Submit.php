<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submit extends CI_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model("Enquiry_Model");
		$this->load->library('email');
	}
	
	public function submitEnquiryFrom()
	{
		if ( isset($_POST) && isset($_POST["email"]) && isset($_POST["mobile_no"]) ) { 

			$res = $this->Enquiry_Model->get_enquiry_details();
		
			if(count($res) > 0){
				$result=array(
					'data'		=> '',
					'status'	=> true,
					'message'	=> "Your for requesting again",
				);
				echo  json_encode($result);
				die(0);
			}
	
			$res = $this->Enquiry_Model->submitEnquiryFrom();
			
			if ($res != '') {
	
				$data = $this->Enquiry_Model->get_enquiry_details_by_id($res);

				$adminMailSent = $this->Enquiry_Model->send_mail_admin($data);
				$userMailSent = $this->Enquiry_Model->send_mail_user($data);


			}
	
			echo json_encode([
				"status" => "success", 
				"host" => $_SERVER["HTTP_HOST"], 
				"isLocalhost" => strpos ( $_SERVER["HTTP_HOST"], "localhost" ),   
				"enquiry" => $res, 
				'adminMailSent' => $adminMailSent, 
				'userMailSent' => $userMailSent 
			]);
			
			die(0);
		}else {
			echo json_encode( ["status" => "error", "msg" => "sorry !! you dont have access "] );
		}
	}

	public function submitContactUs()
	{
		if ( isset($_POST) && isset($_POST["email"]) && isset($_POST["mobile_no"]) ) { 

			$res = $this->Enquiry_Model->get_enquiry_details();
		
			if(count($res) > 0){
				$result=array(
					'data'		=> '',
					'status'	=> true,
					'message'	=> "Your for requesting again",
				);
				echo  json_encode($result);
				die(0);
			}
	
			$res = $this->Enquiry_Model->submitEnquiryFrom();
			
			if ($res != '') {
	
				$data = $this->Enquiry_Model->get_enquiry_details_by_id($res);

				$adminMailSent = $this->Enquiry_Model->send_mail_admin_contact($data);
				$userMailSent = $this->Enquiry_Model->send_mail_user($data);
			}
	
			echo json_encode([
				"status" => "success", 
				"host" => $_SERVER["HTTP_HOST"], 
				"isLocalhost" => strpos ( $_SERVER["HTTP_HOST"], "localhost" ),   
				"enquiry" => $res, 
				'adminMailSent' => $adminMailSent, 
				'userMailSent' => $userMailSent 
			]);
			
			die(0);
		}else {
			echo json_encode( ["status" => "error", "msg" => "sorry !! you dont have access "] );
		}
	}

	public function submitArchitects()
	{
		if ( isset($_POST) && isset($_POST["service"]) && isset($_POST["location"]) && isset($_POST["mobile_no"]) && isset($_POST["email"]) ) { 

			$res = $this->Enquiry_Model->get_enquiry_details();
		
			if(count($res) > 0){
				$result=array(
					'data'		=> '',
					'status'	=> true,
					'message'	=> "Your for requesting again",
				);
				echo  json_encode($result);
				die(0);
			}
	
			$res = $this->Enquiry_Model->submitEnquiryFrom();
			
			if ($res != '') {
	
				$data = $this->Enquiry_Model->get_enquiry_details_by_id($res);

				$adminMailSent = $this->Enquiry_Model->architect_email_template($data);
				$userMailSent = $this->Enquiry_Model->send_mail_user($data);
			}
	
			echo json_encode([
				"status" => "success", 
				"host" => $_SERVER["HTTP_HOST"], 
				"isLocalhost" => strpos ( $_SERVER["HTTP_HOST"], "localhost" ),   
				"enquiry" => $res, 
				'adminMailSent' => $adminMailSent, 
				'userMailSent' => $userMailSent 
			]);
			
			die(0);
		}else {
			echo json_encode( ["status" => "error", "msg" => "sorry !! you dont have access "] );
		}
	}

	public function submitEnquiryForm()
	{
		if ( isset($_POST) && isset($_POST["email"]) && isset($_POST["mobile_no"])&&$_POST["email"]!=''&&$_POST["mobile_no"]!='') { 

			$res = $this->Enquiry_Model->get_enquiry_details();
		
			if(count($res) > 0){
				$result=array(
					'data'		=> 'already_exist',
					'status'	=> true,
					'message'	=> "Your for requesting again",
				);
				echo  json_encode($result);
				die(0);
			}
	
			$res = $this->Enquiry_Model->submitEnquiryFrom();
			
			if ($res != '') {
	
				$data = $this->Enquiry_Model->get_enquiry_details_by_id($res);

				$adminMailSent = $this->Enquiry_Model->send_mail_admin_contact($data);
				$userMailSent = $this->Enquiry_Model->send_mail_user($data);
			}
	
			echo json_encode([
				"status" => "success", 
				"host" => $_SERVER["HTTP_HOST"], 
				"isLocalhost" => strpos ( $_SERVER["HTTP_HOST"], "localhost" ),   
				"enquiry" => $res, 
				'adminMailSent' => $adminMailSent, 
				'userMailSent' => $userMailSent 
			]);
			
			die(0);
		}else {
			echo json_encode( ["status" => "error", "msg" => "sorry !! you dont have access "] );
		}
	}

	public function matchOtp()
	{
		if ( isset($_POST) && isset($_POST["id"]) ) {  

			$this->Enquiry_Model->matchOtp();
		}else {
			echo json_encode( ["status" => "error", "msg" => "sorry !! you dont have access "] );
		}
		
	}

	public function resendOtp()
	{
		if ( isset($_POST) && isset($_POST["id"]) ) {  

			$this->Enquiry_Model->resendOtp();
		}else {
			echo json_encode( ["status" => "error", "msg" => "sorry !! you dont have access "] );
		}
		
	}
	
}
