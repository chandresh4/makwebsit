<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Constructions extends CI_Controller {
	
	public function index()
	{
        $this->load->view('constructions');
        
	}

	public function constructions_success(){ 
        
        
        $this->load->view('constructions_success');
		
	}

    public function page_not_found(){
        $this->load->view('page_not_found');
    }

	public function get_enquiry_details($phone ,$status, $table, $source_lp){ //get the enquiry details
        $this->db->select('en_id');
        $this->db->from($table);
        $this->db->where('phone_no', $phone);
        $this->db->where('source_lp', $source_lp);
        $this->db->where('status', $status);
        $res = $this->db->get()->result();
        return $res;
    }

    public function submit_enquiry_form(){
        
        $table = 'tbl_constructions';
        $mail_sender = 'Constructions';

        $this->form_validation->set_rules('name','Name','trim|required|max_length[30]|min_length[3]');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email');
        $this->form_validation->set_rules('phone','Mobile number','trim|required|regex_match[/^[6-9]{1}[0-9]{9}$/]');
        $this->form_validation->set_rules('city','City Name','trim|required');

        if($this->form_validation->run() == false){
            if(validate_fields() == FALSE){
                return;
            }   
        }

        $source_lp = "Constructions";
        $name = $this->input->post('name'); 
        $email = $this->input->post('email'); 
        $countryCode = $this->input->post('countryCode'); 
        $phone = $this->input->post('phone'); 
        $city = $this->input->post('city'); 
        $utm_source = $this->input->post('utm_source');
        $utm_campaign = $this->input->post('utm_campaign');
        $utm_sub = $this->input->post('utm_sub');
        $utm_medium = $this->input->post('utm_medium');

        $res = $this->get_enquiry_details($phone, 1, $table, $source_lp);
        if(count($res) > 0){
            setResponse(true, '', 'Your for requesting again');
            exit;
        }
        $details['source_lp'] = $source_lp;
        $details['name'] = $name;
        $details['email'] = $email;
        $details['country_code'] = $countryCode;
        $details['city'] = $city;
        $details['phone_no'] = $phone;
        $details['status'] = 1;
        $details['date_created'] = date('Y-m-d H:i:s');
        $details['utm_source']   = $utm_source;
        $details['utm_campaign'] = $utm_campaign;
        $details['utm_sub']      = $utm_sub;
        $details['utm_medium']   = $utm_medium;

        $this->db->insert($table, $details);
        $insert_id = $this->db->insert_id();
        setResponse(true);
    }
}
?>