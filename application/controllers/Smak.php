<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Smak extends CI_Controller {
	
	public function index()
	{
		$this->load->view('index');
	}

	public function about(){
		$this->load->view('about');
	}

	public function interior(){
		$this->load->view('interior');
	}

	public function services(){
		$this->load->view('services');
	}

	public function contactUs(){
		$this->load->view('contactUs');
	}

	public function architects(){
		$this->load->view('architects');
	}

	public function flooring(){
		$this->load->view('flooring');
	}

	public function carpentry(){
		$this->load->view('carpentry');
	}

	public function bathroom_renovation(){
		$this->load->view('bathroom_renovation');
	}

	public function kitchen(){
		$this->load->view('kitchen');
	}

	public function fabrication(){
		$this->load->view('fabrication');
	}

	public function compound_wall(){
		$this->load->view('compound_wall');
	}

	public function terms_condition(){
		$this->load->view('terms_condition');
	}

	public function privacy(){
		$this->load->view('privacy');
	}

	public function disclaimer(){
		$this->load->view('disclaimer');
	}

	public function page_not_found(){
		$this->load->view('page_not_found');
	}

	public function submit_contact_form(){
		$this->form_validation->set_rules('user_name','Name','trim|required|max_length[30]|min_length[3]|alpha');		
		$this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('contact_no','Contact number','trim|required|regex_match[/^[6-9]{1}[0-9]{9}$/]');
		$this->form_validation->set_rules('message','Message','trim|required|max_length[500]|min_length[15]');
		if($this->form_validation->run() == false){
			if(validate_fields() == FALSE){
				return;
			}	
		}
		 
		$details['name'] = $this->input->post('user_name');
		$details['email_id'] = $this->input->post('email');
		$details['contact_no'] = $this->input->post('contact_no');
		$details['message'] = $this->input->post('message');
		$details['status'] = 1;
		$details['date_added'] = date('Y-m-d H:i:s');
		$this->db->insert('contact_us', $details);
		setResponse(true);
	}

	public function submit_home_contact_form(){
		$this->form_validation->set_rules('name','Name','trim|required|max_length[30]|min_length[3]|alpha');		
		$this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('contact_no','Contact number','trim|required|regex_match[/^[6-9]{1}[0-9]{9}$/]');
		//$this->form_validation->set_rules('message','Message','trim|required|max_length[500]|min_length[15]');
		if($this->form_validation->run() == false){
			if(validate_fields() == FALSE){
				return;
			}	
		}

		$details['name'] = $this->input->post('name');
		$details['email_id'] = $this->input->post('email');
		$details['contact_no'] = $this->input->post('contact_no');
		//$details['message'] = $this->input->post('message');
		$details['status'] = 1;
		$details['date_added'] = date('Y-m-d H:i:s');
		$this->db->insert('contact_us', $details);
		setResponse(true);
	}

	public function submit_professional_form(){
		$this->form_validation->set_rules('sel1','Service','trim|required');
		$this->form_validation->set_rules('sel2','Location','trim|required');
		$this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('contact_no','Contact number','trim|required|regex_match[/^[6789]\d{9}$/]');
		if($this->form_validation->run() == false){
			if(validate_fields() == FALSE){
				return;
			}	
		}
		$details['construction_proffessional'] = $this->input->post('profession_type');
		$details['service'] = $this->input->post('sel1');
		$details['location'] = $this->input->post('sel2');
		$details['contact_no'] = $this->input->post('contact_no');
		$details['email'] = $this->input->post('email');
		$details['date_added'] = date('Y-m-d H:i:s');
		$this->db->insert('professional_details', $details);
		setResponse(true);
	}
}
