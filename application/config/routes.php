<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Smak';
$route['404_override'] = 'Smak/page_not_found';
$route['translate_uri_dashes'] = FALSE;


$route['home'] = 'Smak/index';
$route['about_us'] = 'Smak/about';
$route['interior'] = 'Smak/interior';
$route['service'] = 'Smak/services';
$route['contact'] = 'Smak/contactUs';

$route['architects'] = 'Smak/architects';
$route['flooring'] = 'Smak/flooring';
$route['carpentry'] = 'Smak/carpentry';
$route['bathroom_renovation'] = 'Smak/bathroom_renovation';
$route['kitchen'] = 'Smak/kitchen';
$route['fabrication'] = 'Smak/fabrication';
$route['compound_wall'] = 'Smak/compound_wall';

$route['terms_condition'] = 'Smak/terms_condition';
$route['privacy'] = 'Smak/privacy';
$route['disclaimer'] = 'Smak/disclaimer';

$route['page_not_found'] = 'Smak/page_not_found';



$route['vijayanagar'] = 'Smak_vijay'; 

$route['vijayanagar_success'] = 'Smak_vijay/moulim_nagar_success';

$route['interiors'] = 'Smak_interiors'; 

$route['interiors_success'] = 'Smak_interiors/interiors_success';

$route['building_planner_success'] = 'Building_Planner/success';