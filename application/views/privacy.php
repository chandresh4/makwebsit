<!DOCTYPE html>
<html lang="en"> 
    <?php 
        $this->load->view('head');
    ?>
<body>
    <?php 
        $this->load->view('header');
    ?>
    <div class="clearfix"></div>

    <!-- Terms and Condition Banner -->

    <section id="banner">   
        <div class="aboutus_banner">
            <img src="./images/privacy_policy.jpg" alt="Banner" class="banner_img img-responsive webView">
            <img src="./images/privacy_policy_mobile.jpg" alt="Banner" class="banner_img img-responsive mobView">
            <div class=" container">
                <div class="banner-caption">
                    <div class="col-md-12 col-xs-12">
                        <h1> Privacy Policy </h1>
                    </div>
                </div>
            </div>       
        </div>
    </section>

    <div class="clearfix"></div>

    <!--terms and Condition Section -->

    <section class="terms">

        <div class="container">

            <h4>1. Statutory Message</h4>

            <div class="clearfix"></div>

            <p>
                The information found on this website is conceptual and not a legal offering. The promoter reserves the
                right to change, alter, add or delete any of the specifications mentioned herein without prior permission or
                notice.
            </p>

            <div class="clearfix"></div>

            <h4>2. What information do we collect?</h4>

            <p>
                We collect information from you when you register on our site or fill out a form.
                Any data we request that is not required will be specified as voluntary or optional.
                When filling out a form on our site, for any of the above-mentioned reasons, you may be asked to enter
                your name, e-mail address or phone number. You may, however, visit our site anonymously.
            </p>

            <div class="clearfix"></div>

            <p>
                Like most websites, we use cookies to enhance your experience, gather general visitor information, and
                track visits to our website. Please refer to the ‘do we use cookies?’ section below to learn about cookies
                and how we use them.
            </p>

            <div class="clearfix"></div>

            <h4>3. What do we use your information for?</h4>

            <p>
                Any of the information we collect from you may be used in one of the following ways:
                To personalize your experience (your information helps us to better respond to your individual needs)
                To improve our website (we continually strive to improve our website offerings based on the information
                and feedback we receive from you)
                To improve customer service (your information helps us to more effectively respond to your customer
                service requests and support needs)
                To process transactions (Your information, whether public or private, will not be sold, exchanged,
                transferred, or given to any other company for any reason whatsoever, without your consent, other than
                for the express purpose of delivering the purchased product or service requested by the customer)
                To send periodic emails.
            </p>

            <div class="clearfix"></div>

            <p>
                The email address you provide for order processing, may be used to send you information and updates
                pertaining to your order or request, in addition to receiving occasional company news, updates,
                promotions, related product or service information, etc.
                Note: If at any time you would like to unsubscribe from receiving future emails from us, simply respond to
                the email received letting us know the same &amp; we will have you taken off from our database.
                How do we protect your information?
                We implement a variety of security measures to maintain the safety of your personal information when
                you submit a request or enter, submit, or access your personal information.
                These security measures include: password protected directories and databases to safeguard your
                information.
            </p>

            <div class="clearfix"></div>

            <h4>4. Do we use cookies?</h4>

            <p>
                Yes (Cookies are small files that a site or its service provider transfers to your computer’s hard drive
                through your Web browser [if you allow] that enables the sites or service providers systems to recognize
                your browser and capture and remember certain information).
            </p>

            <div class="clearfix"></div>

            <h4>5. Do we disclose any information to outside parties?</h4>

            <p>
                We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This
                does not include trusted third parties who assist us in operating our website, conducting our business, or
                servicing you, so long as those parties agree to keep this information confidential. We may also release
                your information when we believe release is appropriate to comply with the law, enforce our site policies,
                or protect ours or others’ rights, property, or safety. However, non-personally identifiable visitor
                information may be provided to other parties for marketing, advertising, or other uses.
            </p>

            <div class="clearfix"></div>

            <h4>6. Third-party links</h4>

            <p>
                Occasionally, at our discretion, we may include or offer third party products or services on our website.
                These third party sites have separate and independent privacy policies. We therefore have no
                responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to
                protect the integrity of our site and welcome any feedback about these sites.
            </p>

            <div class="clearfix"></div>

            <h4>7. Online Privacy Policy Only</h4>

            <p>
                This online Privacy Policy applies only to information collected through our website and not to information
                collected offline. 
            </p>


            <div class="clearfix"></div>

            <h4>8. Terms and Conditions</h4>

            <p>
                Please also visit our Terms and Conditions section establishing the use, disclaimers, and limitations of
                liability governing the use of our website at <a href="https://www.smakconcepts.com/terms_condition">smakconcepts.com/terms_condition</a>
            </p>

            <p>
                Your Consent
                By using our site, you consent to our Privacy Policy.
            </p>

            <div class="clearfix"></div>

            <h4>9. Changes to our Privacy Policy</h4>

            <p>
                If we decide to change our Privacy Policy, we will post those changes on this page, and/or update the
                Privacy Policy modification date at the top of this page. Policy changes will apply only to information
                collected after the date of the change.
            </p>

            <div class="clearfix"></div>

            <h4>10. Privacy Policy Customer Pledge</h4>

            <p>
                We pledge to you, our customer, that we have made a dedicated effort to bring our Privacy Policy in line
                with the all the important privacy laws and initiatives.
            </p>

        </div>

    </section> 


    <div class="clearfix"></div>
   
    <?php 
        $this->load->view('footer');
        $this->load->view('script_links');
    ?>
    <script>

        $(document).ready(function(){

            var $window = $(window);
            
            $window.scroll(function () {
                if ($window.scrollTop() > 20) {          

                $(".navbar").addClass('nav_blue');

                }else{
                $(".navbar").removeClass('nav_blue');
                }
            });

        });

    </script>
</body>