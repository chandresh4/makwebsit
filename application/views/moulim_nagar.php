<!DOCTYPE html>

<html lang="en">
    <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" href="<?php echo SITE_URL?>assets/moulim_nagar/images/favicon.png" />
      <title>Smak Concepts | Moulim Nagar</title>
    
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/moulim_nagar/style/vendor.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">          
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/moulim_nagar/style/main.css">

      <style>
        .error{
            color:#ff6262;
        }
      </style>

      <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160640646-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-160640646-1');
        </script>

   </head> 
   <body> 
    <section>
        <header>

            <div class="logoSec">
                <a href="<?php echo SITE_URL."moulim_nagar"; ?>"><img src="<?php echo SITE_URL?>assets/moulim_nagar/images/white_logo.svg" alt="" class="handPointer"></a>
            </div>

            <div class="contactSec webView">
                <a href="tel:+91 9886644767">Call Us: +91 9886644767</a>
            </div>

        </header>

        <!-- banner -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">

                        <div class="webView">
                            <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/banner.png"  style="width:100%;" class="img-responsive" >
                        </div>

                        <div class="mobView">
                            <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/banner_mobile.jpg"  style="width:100%;" class="img-responsive">
                        </div>

                    <div class="staticForm webView">

                        <form role="form" id="feedbackForm3" class="feedbackForm" method="POST">
                            <h4>Contact us for a FREE Quote </h4>
                            <div class="group">
                                <div class="form-group">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <input type="text" class="form-control" id="name3" name="name" placeholder="Name" data-attr="Please enter correct name">
                                </div>


                                <div class="form-group">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                                </div>

                            </div>  

                            <div class="group">

                                <div class="form-group">
                                    <i class="fa fa-phone" aria-hidden="true"></i>  
                                    <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="countryCode3">
                                    <input type="text" class="form-control only_numeric phone" id="phone3" name="phone"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                                </div>

                            </div>  
                            
                            <div class="group">

                                <div class="form-group">
                                    <input type="text" class="form-control city" id="city3" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                                </div>
            
                            </div>  

                            <input type="hidden" id="utm_source3" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_sr"); ?>">
                            <input type="hidden" id="utm_medium3" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "d_sr"); ?>">
                            <input type="hidden" id="utm_sub3" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : "d_sr"); ?>">
                            <input type="hidden" id="utm_campaign3" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "d_sr"); ?>">

                            <div class="submitbtncontainer wrap">
                                <input type="submit" class="button" value="Submit" name="submit">
                            </div>
                        </form>

                    </div>

                    <div class="bannerFooter">
                        <div class="container">
                            <p> Moulim Nagar, Mysore - 2 BHK House </p> 
                        </div>
                    </div>
                
                </div>           
            </div>
            <!-- Left and right controls --> 
        </div>

        <div class="mobileFormDiv"> 
            <form role="form" id="feedbackForm2" class="feedbackForm mobView mobileForm" method="POST">
                <h4>Contact us for a FREE Quote </h4>
                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control" id="name2" name="name" placeholder="Name" data-attr="Please enter correct name">
                    </div>


                    <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                    </div>

                </div>  

                <div class="group">

                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                        <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="countryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone2" name="phone"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                    </div>

                </div>   

                <div class="group">

                    <div class="form-group">
                        <input type="text" class="form-control city" id="city2" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                    </div>

                </div>  

                <input type="hidden" id="utm_source3" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_sr"); ?>">
                <input type="hidden" id="utm_medium3" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "d_sr"); ?>">
                <input type="hidden" id="utm_sub3" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : "d_sr"); ?>">
                <input type="hidden" id="utm_campaign3" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "d_sr"); ?>">

                <div class="submitbtncontainer">
                    <input type="submit" value="Submit" name="submit">
                </div>
            </form>

        </div>

        <!-- //banner --> 
    </section>

    <!-- About -->

    <div class="clearfix"></div>

    <section class="videoSection">

        <div class="container">

            <div class="col-md-6 mtop30">

                <div id="myCarouselItems" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarouselItems" data-slide-to="0" class=""></li>
                        <li data-target="#myCarouselItems" data-slide-to="1" class=""></li>
                        <li data-target="#myCarouselItems" data-slide-to="2" class=""></li>
                        <li data-target="#myCarouselItems" data-slide-to="3" class="active"></li>
                        <li data-target="#myCarouselItems" data-slide-to="4" class=""></li>
                        <li data-target="#myCarouselItems" data-slide-to="5" class=""></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        
                        <div class="item">
                            <a href="<?php echo SITE_URL?>assets/moulim_nagar/images/living_area_big.png">
                                <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/living_area.png" alt="" style="width:100%;" class="img-responsive">
                            </a>
                            
                        </div>

                        <div class="item">
                            <a href="<?php echo SITE_URL?>assets/moulim_nagar/images/kitchen_big.png"> 
                                <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/kitchen.png" alt="" style="width:100%;" class="img-responsive">             
                            </a>
                        </div>

                        <div class="item active">
                            <a href="<?php echo SITE_URL?>assets/moulim_nagar/images/bedroom_1_big.png">
                                <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/bedroom_1.png" alt="" style="width:100%;" class="img-responsive">
                            </a>
                        </div>

                        <div class="item">
                            <a href="<?php echo SITE_URL?>assets/moulim_nagar/images/bedroom_2_big.png">
                                <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/bedroom_2.png" alt="" style="width:100%;" class="img-responsive">
                            </a>
                        </div>

                        <div class="item">
                            <a href="<?php echo SITE_URL?>assets/moulim_nagar/images/common_big.png">                            
                                <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/common.png" alt="" style="width:100%;" class="img-responsive">
                            </a>
                        </div>

                        <div class="item">
                            <a href="<?php echo SITE_URL?>assets/moulim_nagar/images/common_2_big.png">
                                <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/common_2.png" alt="" style="width:100%;" class="img-responsive">
                            </a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-5 mtop30">
                <video id="moulimNagar" poster="<?php echo SITE_URL?>assets/moulim_nagar/images/video_banner.jpg" controls="controls" preload="true" loop="loop" muted="muted" volume="0">
                    <source src="<?php echo SITE_URL?>assets/moulim_nagar/video/moulim_nagar.mp4" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>

            </div>
            
    </section>


    <div class="clearfix"></div>

    <section class="aboutSection">

        <div class="container-fluid">

        <!-- <h2 class="smakColor text-center"> Overview </h2> -->

            <div class="col-md-12 paddingZero">

                <div class="col-md-6 paddingZero leftAboutImage">

                    <div class="aboutImageSec">
                        <a href="<?php echo SITE_URL?>assets/moulim_nagar/images/big_house.png"><img src="<?php echo SITE_URL?>assets/moulim_nagar/images/about_us.png" alt="" style="width:100%;" class="img-responsive aboutImgHeight"></a>
                    </div>
                    
                </div>

                <div class="col-md-6 paddingZero aboutParaContent">

                    <h4 class="marginBottom10"> About </h4>

                    <h2 class="marginBottom10"> Moulim Nagar, Mysore <br> 2 BHK House </h2>

                    <p class="aboutPara marginBottom10">
                        Get inspired by aesthetically sophisticated & satisfying interior designing solutions. 
                        We are here with you to co-create your dream home with the help of qualified experts & first-class products.
                        We build long-lasting structures while understanding our clients’ needs and deliver the project on time. 
                        Because, we prioritize customer satisfaction. 
                    </p>

                    <br>

                    <p>

                        <ul class="aboutListItems">
                            <li>1200sq.ft 2 BHK MUDA property at Moulim Nagar, Mysore</li>
                            <li>Specially tailored furniture, custom-made lightings & furnishings </li>
                            <li>Sleek Wardrobe designs for the best usage of storage space</li>
                            <li>Flawless Modular kitchen to radiate warmth & affection </li>
                        </ul>

                    </p>

                    <br>

                    <p> <b>Experience the benefits of unrivaled project management while you sit back and relax. We get the job done for you!</b> </p>

                </div>

            </div>

        </div>

    </section>

    <div class="clearfix"></div>

    <section class="nearBy">

        <div class="container">

            <h2 class="smakColor text-center"> Near Location </h2>

            <div class="col-md-12 mtop30">

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/location_img_1.svg" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                            2.6 Acres of <br> Central Greens
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/location_img_2.svg" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                            82%  <br> Open Area
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/location_img_3.svg" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                            Proximity to <br> International Airport
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/location_img_4.svg" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                            Tourist Attractions <br> Nandi Hills
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/location_img_5.svg" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                        Exclusive Inventories <br> for Senior Citizen
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/moulim_nagar/images/location_img_6.svg" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                        Close to launch <br> Entertainment Park
                        </p>
                    </div>

                </div>

            </div>

        </div>
        
    </section>

    <div class="clearfix"></div>

 

    <div class="clearfix"></div>

    <section class="locationMap">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3898.0917804725427!2d76.7002920148452!3d12.309605832388003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3baf652fce5c9c3f%3A0xeea741ba68636258!2sRing%20Rd%2C%20Moulim%20Nagar%2C%20Yaraganahalli%2C%20Mysuru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1583921047598!5m2!1sen!2sin" width="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </section>

    <footer>
        <div class="container "> 
            <p class="">
               
                <span class="copyrightSec"> © 2020 Smak. All Rights Reserved.</span>

                <span class="footerLinks">
                    <a href="https://www.smakconcepts.com/terms_condition" target="_blank"> Terms & Conditions </a>
                    <a href="https://www.smakconcepts.com/privacy" target="_blank"> Privacy Policy </a>
                </span>
            </p>
        </div>
    </footer> 
    
        <!-- contact form start -->
    <div class="floating-form visiable" id="contact_form">
       <div class="contact-opener">Enquire Now</div>
            <form role="form" id="feedbackForm1" class="feedbackForm" method="POST">
            <h4> Contact us for a FREE Quote </h4>

                <div class="group">
                    <div class="form-group">
                    <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" data-attr="Please enter correct name">
                    </div>                    
                </div>  
                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                         <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                    </div>

                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                         <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="CountryCode">
                        <input type="text" class="form-control only_numeric phone" id="phone" name="phone" pattern="\d*"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="CountryCode_err2"> </span>
                    </div>
                    
                </div>

                <div class="group">
                    <div class="form-group">
                        <input type="text" class="form-control city" id="city" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                    </div>
                </div>  

                <input type="hidden" id="utm_source3" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_sr"); ?>">
                <input type="hidden" id="utm_medium3" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "d_sr"); ?>">
                <input type="hidden" id="utm_sub3" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : "d_sr"); ?>">
                <input type="hidden" id="utm_campaign3" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "d_sr"); ?>">
                <div class="submitbtncontainer">
                    <input type="submit" value="Submit" name="submit" class="button pulse">
                </div>

            </form>

            <div>


    <input type="hidden" name="siteurl" id="siteurl" value="https://www.smakconcepts.com/" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo SITE_URL?>assets/moulim_nagar/js/vendor.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>     
    <script src="<?php echo SITE_URL?>assets/moulim_nagar/js/main.js"></script>  
    <script src="<?php echo SITE_URL?>assets/moulim_nagar/js/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <script>
        window.addEventListener( "pageshow", function ( event ) {
            var historyTraversal = event.persisted || ( typeof window.performance != "undefined" && window.performance.navigation.type === 2 );
            if ( historyTraversal ) {
                // Handle page restore.
                window.location.reload();
            }
        });
        $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
        );
        jQuery.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "letters only");

        $("#feedbackForm1").validate({
            rules: {
              name : {
                required: true,
                minlength: 3,
                maxlength: 30,
                letterswithspace: /^[a-z][a-z\s]*$/
              },
              email: {
                required: true,
                email: true
              },
              phone: {
                required: true,
                number: true,
                regex:/^(0|91)?[6789]\d{9}$/, 
                minlength: 10,
                maxlength: 10
              },
              city: {
                required: true,
                letterswithspace: /^[a-z][a-z\s]*$/
              }
            },
            messages : {
                name: {
                    required: "The Name field is required.",
                    minlength: "Name should be at least 3 characters",
                    maxlength: "Name should be Maximum 30 characters",
                    letterswithspace: "Only alphabetic characters allowed"
                },
                email: {
                    required: "The Email field is required.",
                    email: "email should be in the format: abc@domain.tld"
                },
                phone: {
                    required: "The Mobile Number field is required.",
                    number: "Only digits allowed",
                     regex: "Please enter the valid Phone number",
                    minlength: "Minimum 10 digit number required",
                    maxlength: "Maximum 10 digit number can enter"
                },
                city: {
                    required: "The City name field is required",
                    letterswithspace: "Only alphabetic characters allowed"
                }
            }
        });
        $("#feedbackForm2").validate({
            rules: {
              name : {
                required: true,
                minlength: 3,
                maxlength: 30,
                letterswithspace: /^[a-z][a-z\s]*$/
              },
              email: {
                required: true,
                email: true
              },
              phone: {
                required: true,
                number: true,
                regex:/^(0|91)?[6789]\d{9}$/, 
                minlength: 10,
                maxlength: 10
              },
              city: {
                required: true,
                letterswithspace: /^[a-z][a-z\s]*$/
              }
            },
            messages : {
                name: {
                    required: "The Name field is required.",
                    minlength: "Name should be at least 3 characters",
                    maxlength: "Name should be Maximum 30 characters",
                    letterswithspace: "Only alphabetic characters allowed"
                },
                email: {
                    required: "The Email field is required.",
                    email: "email should be in the format: abc@domain.tld"
                },
                phone: {
                    required: "The Mobile Number field is required.",
                    number: "Only digits allowed",
                     regex: "Please enter the valid Phone number",
                    minlength: "Minimum 10 digit number required",
                    maxlength: "Maximum 10 digit number can enter"
                },
                city: {
                    required: "The City name field is required",
                    letterswithspace: "Only alphabetic characters allowed"
                }
            }
        });
        $("#feedbackForm3").validate({
            rules: {
              name : {
                required: true,
                minlength: 3,
                maxlength: 30,
                letterswithspace: /^[a-z][a-z\s]*$/
              },
              email: {
                required: true,
                email: true
              },
              phone: {
                required: true,
                number: true,
                regex:/^(0|91)?[6789]\d{9}$/, 
                minlength: 10,
                maxlength: 10
              },
              city: {
                required: true,
                letterswithspace: /^[a-z][a-z\s]*$/
              }
            },
            messages : {
                name: {
                    required: "The Name field is required.",
                    minlength: "Name should be at least 3 characters",
                    maxlength: "Name should be Maximum 30 characters",
                    letterswithspace: "Only alphabetic characters allowed"
                },
                email: {
                    required: "The Email field is required.",
                    email: "email should be in the format: abc@domain.tld"
                },
                phone: {
                    required: "The Mobile Number field is required.",
                    number: "Only digits allowed",
                     regex: "Please enter the valid Phone number",
                    minlength: "Minimum 10 digit number required",
                    maxlength: "Maximum 10 digit number can enter"
                },
                city: {
                    required: "The City name field is required",
                    letterswithspace: "Only alphabetic characters allowed"
                }
            }
        });

        //submit form
        $('#feedbackForm1, #feedbackForm2, #feedbackForm3').submit(function(event){
            event.preventDefault();
            $(this).html();
            //get the input data
            input = $(this).serialize();
            base_url = '<?php echo SITE_URL; ?>'; //'https://www.amazepromos.com/'
            var $form = $(this);
            var id = $form.attr('id');
            //submit
            $.ajax({
                type: 'POST',
                url: base_url+"Smak_moulim/submit_enquiry_form",
                data: input,
                success : function(response){ 
                    if(response.status == false){
                        $.each(response.data['error'], function(key, value){
                             $('#'+id+' .'+key+'_err').html(value);
                        });
                    }else if(response.status == false && response.message == 'failed'){
                        $.dialog({
                             title: 'Sorry!',
                            content: 'Something went wrong. Try again',
                        });
                    }else if(response.status == true && response.message != ''){
                        $.dialog({
                             title: 'Thank You!',
                            content: 'Your request is already sent! we will get back to you soon',
                        });
                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    }else if(response.status == true){
                        $(this).empty();
                        window.location.href=base_url+"moulim_nagar_success";
                    }
                } 
            });
        });

        $(".helpBtn").click(function(){
            $('html, body').animate({
                scrollTop: $("#myCarousel").offset().top
            }, 2000);
        });

    </script>

    <script>
        $('.carousel-inner').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery:{
                enabled:true
            }
        });

        $('.aboutImageSec').magnificPopup({
            delegate: 'a',
            type: 'image'
        });
    </script>

    <script>

        $window.scroll(function ()  {
            if($window.scrollTop() > 620){
                document.getElementById("moulimNagar").play();
            }
        })
        
    </script>

   </body>

</html>