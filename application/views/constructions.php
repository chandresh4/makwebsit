<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="<?php echo SITE_URL?>assets/constructions/images/favicon.png" />
        <title>Smak Constructions</title>
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo SITE_URL?>assets/constructions/style/vendor.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">          
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>

        <link rel="stylesheet" href="<?php echo SITE_URL?>assets/constructions/style/constructions.css">

        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167529661-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-167529661-2');
        </script>


      <style>
        .error{
            color:#ff6262;
        }

        .slider .item{
            padding:20px
        }
      </style>

      <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160640646-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-160640646-1');
        </script>

        <!-- Global site tag (gtag.js) - Google Ads: 609114811 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-609114811"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-609114811');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '3897310283613857');
            fbq('track', 'PageView');
        </script>
        
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=3897310283613857&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

        

   </head> 
   <body> 
    <section>
        
        <header class="webView">

            <nav class="navbar">
            <div class="container">
            <div class="navbar-header">

                <a class="navbar-brand" href="<?php echo SITE_URL?>">
                    <img src="<?php echo SITE_URL?>assets/constructions/images/logo.svg" alt="smack-logo">
                </a>
            </div>
            
            
            </div>
        </nav>

        </header>

        <!-- banner -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">

                        <div class="webView">
                            <div class="estimateArea">
                                <h1>Homes Built with  <br> Exceptional Experience </h1>

                                <p>We manage everything from approval to handing <br>  Join hands with us for smooth construction experience <br> Come explore us today</p>

                                <button onClick="window.location.href='<?php echo SITE_URL?>contact'" class="btn btn-primary bannerButton">Contact Us Now <span class="bannerArrow"><i class="fa fa-arrow-right" aria-hidden="true"></i></span> </button>
                            
                            </div>
                            <img src="<?php echo SITE_URL?>assets/constructions/images/banner.png"  style="width:100%;" class="img-responsive" >
                        </div>

                        <div class="mobView">
                            <img src="<?php echo SITE_URL?>assets/constructions/images/banner_mobile.png"  style="width:100%;" class="img-responsive">
                        </div>

                    <div class="staticForm webView">

                        <form role="form" id="feedbackForm3" class="feedbackForm form_3" method="POST">
                            <h4>Fill the form! We'll call you back </h4>
                            <div class="group">
                                <div class="form-group">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <input type="text" class="form-control" id="name3" name="name" placeholder="Name" data-attr="Please enter correct name">
                                </div>


                                <div class="form-group">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                                </div>

                            </div>  

                            <div class="group">

                                <div class="form-group">
                                    <i class="fa fa-phone" aria-hidden="true"></i>  
                                    <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="countryCode3">
                                    <input type="text" class="form-control only_numeric phone" id="phone3" name="phone"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                                </div>

                            </div>  
                            
                            <div class="group">

                                <div class="form-group">
                                    <input type="text" class="form-control city" id="city3" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                                </div>
            
                            </div>  

                            <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_GET['utm_source']) != "" ? renderGETParams($_GET['utm_source']) : ""); ?>">
							<input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_GET['utm_medium']) != "" ? renderGETParams($_GET['utm_medium']) : ""); ?>">
							<input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_GET['utm_sub']) != "" ? renderGETParams($_GET['utm_sub']) : ""); ?>">
							<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_GET['utm_campaign']) != "" ? renderGETParams($_GET['utm_campaign']) : ""); ?>">
							<input type="hidden" id="utm_term" name="utm_term" value="<?php echo (isset($_GET['utm_term']) != "" ? renderGETParams($_GET['utm_term']) : ""); ?>">
							<input type="hidden" id="utm_content" name="utm_content" value="<?php echo (isset($_GET['utm_content']) != "" ? renderGETParams($_GET['utm_content']) : ""); ?>">
							
							<input type="hidden" id="adpos" name="adpos" value="<?php echo (isset($_GET['adpos']) != "" ? renderGETParams($_GET['adpos']) : ""); ?>">
							<input type="hidden" id="ad" name="ad" value="<?php echo (isset($_GET['ad']) != "" ? renderGETParams($_GET['ad']) : ""); ?>">
							
							<input type="hidden" id="business_type" name="business_type" value="constructions">
							<input type="hidden" id="project" name="project" value="">
	
                            <div class="submitbtncontainer wrap">
                                <input type="submit" class="button" value="Submit" name="submit">
                            </div>
                        </form>

                        <form action="" id="otpFrom3" class="feedbackForm otp-form_3" style="display: none;" novalidate="novalidate">

                            <div class="group">
                                <div class="form-group">
                                    <i class="fa fa-phone" aria-hidden="true"></i>  
                                    <input type="text" class="form-control only_numeric otp phone" id="otp3" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                                    <label id="otp3-error" class="error" for="otp3"></label>
    		                        <span id="otp3-msg"></span>
                                </div>
                            </div> 

                            <div class="group">
                                <div class="form-group" style="margin-bottom: 0; text-align: right; cursor:pointer;">
                                    <span href="javascript:void(0);" data-form-type="3" class="form_3-resend-otp resend-otp">Resend OTP</span>
                                </div>
                            </div> 
                            
                            <div class="submitbtncontainer wrap">
                                <input type="submit" class="button form_3-otp-submit-btn" value="Submit OTP" name="submit">
                            </div>

                            <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

                        </form>

                    </div>

                    <div class="bannerFooter">
                        <div class="container">
                            <img src="<?php echo SITE_URL?>assets/constructions/images/down_arrow.png" alt="" height='60'> 
                        </div>
                    </div>
                
                </div>           
            </div>
            <!-- Left and right controls --> 
        </div>

        <div class="mobileFormDiv"> 
            <form role="form" id="feedbackForm2" class="feedbackForm form_2 mobView mobileForm" method="POST">
                <h4>Fill the form! We'll call you back </h4>
                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control" id="name2" name="name" placeholder="Name" data-attr="Please enter correct name">
                    </div>


                    <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                    </div>

                </div>  

                <div class="group">

                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                        <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="countryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone2" name="phone"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                    </div>

                </div>   

                <div class="group">

                    <div class="form-group">
                        <input type="text" class="form-control city" id="city2" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                    </div>

                </div>  

                <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_GET['utm_source']) != "" ? renderGETParams($_GET['utm_source']) : ""); ?>">
				<input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_GET['utm_medium']) != "" ? renderGETParams($_GET['utm_medium']) : ""); ?>">
				<input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_GET['utm_sub']) != "" ? renderGETParams($_GET['utm_sub']) : ""); ?>">
				<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_GET['utm_campaign']) != "" ? renderGETParams($_GET['utm_campaign']) : ""); ?>">
				<input type="hidden" id="utm_term" name="utm_term" value="<?php echo (isset($_GET['utm_term']) != "" ? renderGETParams($_GET['utm_term']) : ""); ?>">
				<input type="hidden" id="utm_content" name="utm_content" value="<?php echo (isset($_GET['utm_content']) != "" ? renderGETParams($_GET['utm_content']) : ""); ?>">
							
				<input type="hidden" id="adpos" name="adpos" value="<?php echo (isset($_GET['adpos']) != "" ? renderGETParams($_GET['adpos']) : ""); ?>">
				<input type="hidden" id="ad" name="ad" value="<?php echo (isset($_GET['ad']) != "" ? renderGETParams($_GET['ad']) : ""); ?>">
					
				<input type="hidden" id="business_type" name="business_type" value="constructions">
				<input type="hidden" id="project" name="project" value="">

                <div class="submitbtncontainer">
                    <input type="submit" value="Submit" name="submit">
                </div>
            </form>

            <form action="" id="otpFrom2" class="feedbackForm otp-form_2" style="display: none;" novalidate="novalidate">

            <div class="group">
                <div class="form-group">
                    <i class="fa fa-phone" aria-hidden="true"></i>  
                    <input type="text" class="form-control only_numeric otp phone" id="otp2" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                    <label id="otp2-error" class="error" for="otp2"></label>
    		        <span id="otp2-msg"></span>
                </div>
            </div> 

            <div class="group">
                <div class="form-group" style="margin-bottom: 0; text-align: right; cursor:pointer;">
                    <span href="javascript:void(0);" data-form-type="2" class="form_2-resend-otp resend-otp">Resend OTP</span>
                </div>
            </div> 
            
            <div class="submitbtncontainer wrap">
                <input type="submit" class="button form_2-otp-submit-btn" value="Submit OTP" name="submit">
            </div>

            <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

            </form>

        </div>

        <!-- //banner --> 
    </section>

    <div class="clearfix"></div>

    <section class="aboutSection">

        <div class="container">

        <!-- <h2 class="smakColor text-center"> Overview </h2> -->

            <div class="col-md-12 paddingZero">

                <div class="col-md-6 paddingZero leftAboutImage">

                    <div class="aboutImageSec">
                        <a href="<?php echo SITE_URL?>assets/constructions/images/about.png">
                            <img src="<?php echo SITE_URL?>assets/constructions/images/about.png" alt=""  class="img-responsive">
                        </a>
                    </div>
                    
                </div>

                <div class="col-md-6 paddingZero aboutParaContent">

                    <h5 class="boldFont marginBottom10">About Us</h5>

                    <h1 class="marginBottom10 smakColor"> We build your <br> Future </h1>

                    <p class="aboutPara marginBottom10">
                        Our assurance to you? To use high quality and branded steel, bricks concrete and 
                        cement for giving you strong structures that live on forever! Choose your package or contact us 
                        for more details on how your dream project should be. 
                        
                        <br>

                        Our competent project managers will keep an eye on the project till the end to 
                        ensure hassle-free construction experience. Our internationally qualified architects 
                        will design the structures for you or you can give us yours. 
                        We promise 100% customer satisfaction.
                    </p>

                </div>

                

            </div>

        </div>

    </section>

    <div class="clearfix"></div>

    <section class="nearBy"> 

        <div class="container">

            <div class="slider col-md-12">

                <div class="item col-md-3">
                    <img src="<?php echo SITE_URL?>assets/constructions/images/construction.png" alt="construction" calss="img-responsive"'>
                    <h2>CONSTRUCTION</h1>
                    <p>We have always been known for long-lasting buildings and the ability to thoroughly comprehend clients’ requirements. </p>
                </div>

                <div class="item col-md-3">
                    <img src="<?php echo SITE_URL?>assets/constructions/images/mansory.png" alt="construction" calss="img-responsive">
                    <h2>MASONRY</h1>
                    <p>The team of skilled masons for all types of buildings SMAK has, has always been the secret behind our success.</p>
                </div>

                <div class="item col-md-3">
                    <img src="<?php echo SITE_URL?>assets/constructions/images/painting.png" alt="construction" calss="img-responsive">
                    <h2>PAINTING</h1>
                    <p>We are incredibly good at this luminous language! Our highly professional painters bring colours to your life. </p>
                </div>

                <div class="item col-md-3">
                    <img src="<?php echo SITE_URL?>assets/constructions/images/plumbing.png" alt="construction" calss="img-responsive">
                    <h2>PLUMBING</h1>
                    <p> No plumbing job is too small or too big for our team of plumbers. We fix everything in a jiffy! </p>
                </div>

                <div class="item col-md-3">
                    <img src="<?php echo SITE_URL?>assets/constructions/images/roofing.png" alt="construction" calss="img-responsive">
                    <h2>ROOFING</h1>
                    <p>We offer comprehensive roofing solutions of every size & shape to our esteemed clients at the best price</p>
                </div>

                <div class="item col-md-3">
                    <img src="<?php echo SITE_URL?>assets/constructions/images/architecture.png" alt="construction" calss="img-responsive">
                    <h2>ARCHITECTURE</h1>
                    <p>Our fundamental architectural ideas are all aimed at building strong & beautiful structures for you.</p>
                </div>
                
            </div>

        </div>
        
    </section>

    <div class="clearfix"></div>

    <section id="services">

        <div class="container">

            <h1 class="sec_heading">OUR PROJECT</h1>
            <p class="sec_description">Have a glance at our gallery book and let us know your heart’s desire! We’ll help you reach your goal.</p>
        
            <div class="col-md-12 padTop25">
                <div id='interior_service_1' class="col-md-3 col-sm-3 col-xs-12 interior_service smallImg text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="<?php echo SITE_URL?>assets/constructions/images/01.png" alt="" class="img-responsive" > 
                    <h3 class="interior_service_1">Elevation View</h3>
                </div>

                <div id='interior_service_2' class="col-md-3 col-sm-3 col-xs-12 interior_service smallImg text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="<?php echo SITE_URL?>assets/constructions/images/02.png" alt="" class="img-responsive" >
                    <h3 class='interior_service_2'>Living Area</h3>
                </div>

                
                <div id='interior_service_3' class="col-md-3 col-xs-12 interior_service smallImg" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="<?php echo SITE_URL?>assets/constructions/images/03.png" alt="" class="img-responsive">
                    <h3 class='interior_service_3'>Dining Area</h3>
                </div>
                
                <div id='interior_service_4' class="col-md-3 col-xs-12 interior_service smallImg" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="<?php echo SITE_URL?>assets/constructions/images/04.png" alt="" class="img-responsive" >
                    <h3 class='interior_service_4'>Bedroom 1</h3>
                </div>
                
            </div>

            <div id='interior_service_5' class="col-md-5 col-sm-5 col-xs-12 interior_service bigImg text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="<?php echo SITE_URL?>assets/constructions/images/05.png" alt="" class="img-responsive">
                    <h3 class='interior_service_5'>Bedroom 2</h3>
            </div>

            <div id='interior_service_6' class="col-md-5 col-sm-5 col-xs-12 interior_service bigImg text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="<?php echo SITE_URL?>assets/constructions/images/06.png" alt="" class="img-responsive">
                    <h3 class='interior_service_6'>Washroom</h3>
            </div>

            <div id='interior_service_7' class="col-md-5 col-sm-5 col-xs-12 interior_service bigImg text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="<?php echo SITE_URL?>assets/constructions/images/09.png" alt="" class="img-responsive">
                    <h3 class='interior_service_7'>Dining Space</h3>
            </div>

            <div id='interior_service_8' class="col-md-5 col-sm-5 col-xs-12 interior_service bigImg text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="<?php echo SITE_URL?>assets/constructions/images/10.png" alt="" class="img-responsive">
                    <h3 class='interior_service_8'>Study Area</h3>
            </div>
        
        </div>
        
    </section>

    <div class="clearfix"></div>

    

    <footer>
        <div class="container "> 
            <hr>
            <p class="">
               
                <span class="copyrightSec"> © 2020 Smak. All Rights Reserved.</span>

                <span class="footerLinks">
                    <a href="https://www.smakconcepts.com/terms_condition" target="_blank"> Terms & Conditions </a>
                    <a href="https://www.smakconcepts.com/privacy" target="_blank"> Privacy Policy </a>
                </span>
            </p>
        </div>
    </footer> 
    
        <!-- contact form start -->
    <div class="floating-form" id="contact_form">
       <div class="contact-opener">Enquire Now</div>
            <form role="form" id="feedbackForm1" class="feedbackForm form_1" method="POST">
            <h4> Fill the form! We'll call you back </h4>

                <div class="group">
                    <div class="form-group">
                    <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" data-attr="Please enter correct name">
                    </div>                    
                </div>  
                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                         <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                    </div>

                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                         <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="CountryCode">
                        <input type="text" class="form-control only_numeric phone" id="phone" name="phone" pattern="\d*"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="CountryCode_err2"> </span>
                    </div>
                    
                </div>

                <div class="group">
                    <div class="form-group">
                        <input type="text" class="form-control city" id="city" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                    </div>
                </div>  

                <input type="hidden" id="utm_source3" name="utm_source" value="<?php echo (isset($_GET['utm_source']) != "" ? $_GET['utm_source'] : ""); ?>">
                <input type="hidden" id="utm_medium3" name="utm_medium" value="<?php echo (isset($_GET['utm_medium']) != "" ? $_GET['utm_medium'] : ""); ?>">
                <input type="hidden" id="utm_sub3" name="utm_sub" value="<?php echo (isset($_GET['utm_sub']) != "" ? $_GET['utm_sub'] : ""); ?>">
                <input type="hidden" id="utm_campaign3" name="utm_campaign" value="<?php echo (isset($_GET['utm_campaign']) != "" ? $_GET['utm_campaign'] : ""); ?>">
                <div class="submitbtncontainer">
                    <input type="submit" value="Submit" name="submit" class="button pulse">
                </div>

            </form>

            <form action="" method="POST" id="otpFrom1" class="feedbackForm otp-form_1" style="display: none;">

                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                        <input type="text" class="form-control otp only_numeric phone" id="otp" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                        <label id="otp1-error" class="error" for="otp1"></label>
    		            <span id="otp1-msg"></span>
                    </div>
                </div> 

                <div class="group">
                    <div class="form-group" style="margin-bottom: 0; text-align: right; cursor:pointer;">
                        <span href="javascript:void(0);" data-form-type="1" class="form_1-resend-otp resend-otp">Resend OTP</span>
                    </div>
                </div> 
                
                <div class="submitbtncontainer wrap">
                    <input type="submit" class="button form_1-otp-submit-btn" value="Submit OTP" name="submit">
                </div>

                <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

            </form>

            <div>


    <input type="hidden" name="siteurl" id="siteurl" value="https://www.smakconcepts.com/" />    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo SITE_URL?>assets/interiors/js/jquery.validate.js"></script>
    <script src="<?php echo SITE_URL?>assets/consructions/js/main.js"></script>  
    <script type="text/javascript" src="<?php echo SITE_URL?>assets/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
	<script>

        $(".bannerButton").click( function(){
            $("#name3").focus();
        })

        if(window.innerWidth>900){
            
       
        
        function hover(ele){
            var id= ele.id;
            document.querySelector(`.${id}`).style.visibility='visible';    
        }

        function mouseOut(ele){
            var id= ele.id;
            document.querySelector(`.${id}`).style.visibility='hidden';    
        }

        }

    </script>

    <script>
       $(document).ready(function($) {
            $('.slider').slick({
                dots: false,
                infinite: true,                
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,                
                arrows: true,
                
                responsive: [{
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
                },
                {
                breakpoint: 400,
                settings: {
                    arrows: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }]
            });
        });

    
    
		window.addEventListener( "pageshow", function ( event ) {
            var historyTraversal = event.persisted || ( typeof window.performance != "undefined" && window.performance.navigation.type === 2 );
            if ( historyTraversal ) {
                // Handle page restore.
                window.location.reload();
            }
        });
        $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
        );
        jQuery.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "letters only");

      
        $(".helpBtn").click(function(){
            $('html, body').animate({
                scrollTop: $("#myCarousel").offset().top
            }, 2000);
        });
    </script>
	<?php $this->load->view ("footer_scripts");?>
	
   </body>

</html>

<?php 

function renderGETParams( $field = '' ) {

    if ( isset($field) ) {

        $value = str_replace( '"', '',  $field);
        $value = str_replace( "'", "",  $value );

        return $value;
    }else {
        return "";
    }
}

?>