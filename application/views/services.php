<!DOCTYPE html>
<html lang="en"> 
    <?php 
        $this->load->view('head');
    ?>
<body class="rigfix" style="padding-right: 0px !improtant;">
    <?php 
        $this->load->view('header');
    ?>
    <div class="clearfix"></div>

    <!-- Banner -->
    <section id="banner"> 
        <div class="aboutus_banner">
                <img src="./images/services_banner.jpg" alt="Banner" class="banner_img img-responsive webView">
                <img src="./images/services_banner_mobile.jpg" alt="Banner" class="banner_img img-responsive mobView">           
            
            <div class=" container">
                <div class="banner-caption">
                    <div class="col-md-12 col-xs-12">
                        <h1>Our Services</h1>
                    </div>
                </div>
            </div>      
        </div> 
    </section>

    <div class="clearfix"></div>
    <!-- Description Section -->
   
        <style>
        .how-para h3{
            border: 2px solid #1488ED;
            display: block;
            color: #000000;
            background-color: #fff;
            
            text-align: center;
            margin:10px;

        }
        .how-para h3 a{
            display: block;
            color: #000;
            font-size: 15px;
            font-weight: 600;

            padding: 20px 12px;
            text-align: center;
        }

        .how-para h3 a:hover{
            color: #fff;
            background-color: #1488ED;
            text-decoration: none;
        }
.com-pro1 h2{
    text-align: center;
    font-size: 26px;
    color: #000;
    font-weight: 600;
    margin: 0px;
    padding: 30px 0;
    text-transform: uppercase;
}
.close1
{
    float: right;
    width: 20px;
}
.modal-header{
    border-bottom: 0px;
}
.modal-dialog{
    width: 540px;
    top: 23%;
    text-align: center;
}
.modal-body{
    padding: 0px 30px 30px 30px;
}
.modal-body p{
    font-size: 14px;
    font-weight: 500;
    line-height: 1.6;
    color: #000;
}
.modal-body h3{
    font-size: 30px;
    font-weight: 600;
    color: #1488ED;
}
.modal-content{
    border-radius: 0px;
    border: 1px solid #707070;
}
.why-us {
    background-color: #fff;
    padding: 15px 30px;
    margin: 10px;
    text-align: center;
    height: 134px;
}
.why-us p {
    line-height: 1.6;
    font-size: 16px;
    color: #000;
    font-weight: 500;
    margin: 0px;
    padding-top: 8px;
}
.why-sec{
    background-color: #EEF7FF;
    padding: 20px 0;
    padding-bottom: 50px;
}
.md1{
    padding-top: 0px;
}

.what-para {
    padding: 25px 30px;
    margin: 25px 0;
}
.what-para h3{
    margin: 25px 0;
}
.what-para ul {
    margin: 0px;
    padding: 0px;
}
.what-para ul li {
    list-style: disc;
    font-size: 16px;
    font-weight: 500;
    color: #000;
    line-height: 1.8;
    padding: 0;
    display: revert;
    margin-left: 16px;
}
.what-para h3 a{
    margin: 10px 0;
    padding: 15px 30px;
    background-color: #1488ED;
    font-size: 16px;
    color: #fff;
    text-transform: uppercase;
    text-align: center;
}
.what-para h4{
    font-size: 16px;
    font-weight: 600;
}
.what-para h2{
    font-size: 21px;
    color: #000;
    font-weight: 600;
    text-transform: capitalize;
}
.what-para p{
    line-height: 1.6;
    font-size: 18px;
    color: #000;
    font-weight: 500;
    margin-bottom: 10px;
}
.what-para h3 a img{
    width: 20px;
    margin-left: 10px;
    margin-top: -3px;
}
.what-para h3 a:hover{
    text-decoration: none;
}
.what-img{
    
    padding: 20px;
    margin: 25px 0;
}
.mb0 h2{
    padding-bottom: 0px;
}
  @media only screen and (max-width: 600px)  {
            .modal-dialog{
                width: auto;
            }
            .why-us{
                height: unset;
            }
            
            }

    </style>
    <section class="what-offer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="com-pro1 mb0">
                    <h2>WHAT WE OFFER</h2>
                    </div>
                </div>
                    <div class="col-md-6">
                        <div class="what-img">
                        <img src="./images/shutterstock_740754769.jpg" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="what-para">
                            <h2>Architectural plans</h2>
                            <p>display a picture which speaks for architectural plan and a explore arrow option provided at its bottom. the below lines which speaks about the architectural plan should be displayed below the picture.<br><br> Our architects have always been fantasized about creating and designing over a space. We have a highly experienced architectures who works on the floor plans and set the details, elevations and working plans which are completely Vaastu oriented.</p>
                            <h3><a href="#">Explore <img src="./images/arrow.svg"></a></h3>
                            <ul>
                                <li> We offer the best options of plans for your site dimensions</li>
                                <li>  Discussion of the finalized plan with client and make them understand every aspect in the plans</li>
                                <li>  Error free design and drawings by our team</li>
                                <li>  Smooth and accurate communication with our team.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                     
                    <div class="col-md-6">
                        <div class="what-para">
                            <h2>Structural design</h2>
                            <p>We ensure the floor plans are worked on are designed everything to IS standards. The finalized drawings are studied for its compatible results. We discuss your requirements and deliver the best results out of all.</p>
                            <h3><a href="#">Explore <img src="./images/arrow.svg"></a></h3>
                            <h4>Role</h4>
                            <ul>
                                <li>Design of compatible structures</li>
                                <li>Designing each element using STAAD-pro software</li>
                                <li>Preparing and checking of STAAD models</li>
                                <li> Checking designs and drawings</li>
                                <li>Discuss with architect and arrive at mutually acceptable solutions</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="what-img">
                        <img src="./images/shutterstock_735296518-[Converted].jpg" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="what-img">
                        <img src="./images/shutterstock_1406116871.jpg" class="img-responsive">
                        </div>
                    </div>
                     
                    <div class="col-md-6">
                        <div class="what-para">
                            <h2>construction</h2>
                            <p>Our site engineers look into every detail in the drawing, also examines the materials delivered to the site are of the best quality as ordered, and the site is built everything to perfection. Ensuring that our systems and processes are in line with the market demands are at our best interest.</p>
                            <h3><a href="#">Explore <img src="./images/arrow.svg"></a></h3>
                             <h4>Role</h4>
                            <ul>
                                <li> Our well experienced team guides the labors by giving a detailed description on the work
                                </li><li> The team ensures every work is carried out to the right precision.</li>
                                <li>We oversee every minute details in the drawing and recheck the same details on site.</li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                     
                    <div class="col-md-6">
                        <div class="what-para">
                            <h2>Renovation</h2>
                            <p>Old buildings come with surprises with its own style and design. We retain all the things which add up the style to our recreated design and ensure its purpose is served entirely. While the world says renovation’s stressful, we always take it as a challenge and give it a new start.</p>
                            <h3><a href="#">Explore <img src="./images/arrow.svg"></a></h3>
                             
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="what-img">
                        <img src="./images/shutterstock_222648043.jpg" class="img-responsive">
                        </div>
                    </div>
                </div>


                </div>
            </div>
        </div>
    </section>

    <section class="why-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="com-pro1">
                    <h2>Why us</h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="why-us">
                        <p>Simplicity – we simplify the design and use the space we create for effective design. Simple and clean lines with minimal walls ensure free flowing space without compromising privacy.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="why-us">
                        <p>Detail – every space we create will be given detailing to make you understand how well the space has been used. Every element will be designed as per IS standard. Every material brought to the site are rechecked.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="why-us md1">
                        <p>Connection – we strongly believe that the connection we share with our client will serve the same with the work. We involve you in every stage of your house, may it be designing, selecting materials, selecting finishes to make sure that the purpose is served entirely.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="why-us">
                        <p>Convenience – our foresight activity in work and innovation makes things uncomplicated and we make sure everything is fitting well with your needs, activities and plan.</p>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="why-us">
                        <p>Timely deliver - We ensure that the construction is completed within the agreed time frame and the key is handed over to the client along with a formal handover kit</p>
                    </div>
                </div>
                <div class="col-md-3"></div>
                </div>
            </div>
        </div>
        
    </section>
     <section>
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <img src="./images/close-filled.svg" class="img-responsive close1" type="button" class="close" data-dismiss="modal">
          
          
        </div>
        <div class="modal-body">
            <h3>Site Visit</h3>
          <p>Visiting site, knowing site perpendicularity, road level, checking if any electric pole obstructs the construction and all the other details are noted down before starting to plan.</p>
        </div>
        
      </div>
      
    </div>
  </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="com-pro1">
                    <h2>How it works</h2>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="how-para">
                        <h3>
                            <a href="#" data-toggle="modal" data-target="#myModal">Client Requirement</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="how-para">
                        <h3>
                            <a href="#" data-toggle="modal" data-target="#myModal">Site Visit</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="how-para">
                        <h3>
                           <a href="#" data-toggle="modal" data-target="#myModal">Client Meeting</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="how-para">
                        <h3>
                            <a href="#" data-toggle="modal" data-target="#myModal">Planning and designing</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="how-para">
                        <h3>
                           <a href="#" data-toggle="modal" data-target="#myModal">Agreement signing</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="how-para">
                        <h3>
                         <a href="#" data-toggle="modal" data-target="#myModal">Daily work updates to clients</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="how-para">
                        <h3>
                           <a href="#" data-toggle="modal" data-target="#myModal">Regular Site visit by technical team</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="how-para">
                        <h3>
                            <a href="#" data-toggle="modal" data-target="#myModal">Materials selection with Client</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <div class="how-para">
                        <h3>
                           <a href="#" data-toggle="modal" data-target="#myModal">Checking of work at Final stage</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="how-para">
                        <h3>
                           <a href="#" data-toggle="modal" data-target="#myModal">Handing over the key.</a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="hireUs">
        <div class="container">
                <h1 class="sec_heading">WHAT WE DO</h1>

                <div class="experties col-md-12">
                    
                        <div class="expert col-md-4 col-xs-12 goToArchitects mtop15 handpointer">
                            
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="./images/architects.png" alt="" class="invert">
                                </div>
                                <div class="col-md-8 text_left we_do">
                                    <h3>Architects</h3>
                                    <p>We customize the designs to adapt to your necessity. So choose your package for a happy home!</p>
                                </div>
                            </div>

                        </div>

                        <div class="expert col-md-4 col-xs-12 goToFlooring mtop15 handpointer">
                            
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="./images/flooring_experts.png" alt="" class="invert">
                                </div>
                                <div class="col-md-8 text_left we_do">
                                    <h3>Flooring  Experts</h3>
                                    <p>We train & certify our experts to make sure the best quality work is delivered to you</p>
                                </div>
                            </div>                           
                            
                        </div>

                        <div class="expert col-md-4 col-xs-12 goToCarpentry mtop15 handpointer">
                            
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="./images/carpentry.png" alt="" class="invert">
                                </div>
                                <div class="col-md-8 text_left we_do">
                                    <h3>Carpentry/ Woodworking</h3>
                                    <p>Our skilled carpenters are aware of the latest trends, & will satisfy your specifications.</p>
                                </div>
                            </div>                      
                            
                        </div>

                        <div class="expert col-md-4 col-xs-12 goToBathroomRenovation mtop15 handpointer">
                            
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="./images/Bathroom Renovation.png" alt="" class=" invert">
                                </div>
                                <div class="col-md-8 text_left we_do">
                                    <h3>Bathroom  Renovation</h3>
                                    <p>We Have a team of skilled Civil Engineers, Plumbing & Electrical professionals for the top quality renovation</p>
                                </div>
                            </div>             
                            
                        </div>

                        <div class="expert col-md-4 col-xs-12 goToKitchen mtop15 handpointer">
                            
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="./images/kitchen.png" alt="" class="invert"> 
                                </div>
                                <div class="col-md-8 text_left we_do">
                                    <h3>Kitchen  Renovation</h3>
                                    <p>Upgrade your kitchen with the help of our certified team of skilled Civil Engineers, Interior Designers</p>
                                </div>
                            </div>             
                            
                        </div>

                        <div class="expert col-md-4 col-xs-12 goToFabrication mtop15 handpointer">
                            
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="./images/Grillwork.png" alt="" class=" invert">
                                </div>
                                <div class="col-md-8 text_left we_do">
                                    <h3>Metal Fabrication/ 
                                        Grillwork</h3>
                                    <p>Fabricate your dream home according to your design specifications with the help of our highly qualified fabricators.</p>
                                </div>
                            </div>             
                            
                        </div>

                        <!-- <div class="expert col-md-4 col-xs-6 goToCompound mtop15">
                            
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="./images/Compound Wall.png" alt="" class="invert">
                                </div>
                                <div class="col-md-8 text_left we_do">
                                    <h3>Compound Wall 
                                        Construction</h3>
                                    <p>Whatever type, we are ready with our professionals. Just mention what is that you need</p>
                                </div>
                            </div>             
                            
                        </div>    -->
                </div>
        </div>

    </section>

    <div class="clearfix"></div>

    <br>

    <section id="enquiry" class="bg_blue">
        <div class="container">
            <div class="col-md-12 text_Centre">
                <div class="col-md-8">
                    <p>We will help you make your house look awesome & professional</p>
                </div>
                <div class="col-md-4">
                    <button class="goToContact">ENQUIRE</button>
                </div>
            </div>
        </div>

    </section>

    <div class="clearfix"></div>
    <!-- Description Section -->

    <section class="description_section">
            <div class="container">
                <div class="col-md-12">
                    <div class="col-md-6 col-xs-12 col-sm-6 desc_img">
                        <img src="./images/services-desc.jpg" alt="descrition image" class="img-responsive">

                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6 text_desc">
                        <h1>Construct, Renovate & Remodel
                            your house</h1>
                        
                        <p>
                            Partial or complete renovation, we are your best choice! We not only make your dream project a reality; we make it affordable for you
                        </p>
                        <ul class="D_block">
                            <li><span><img src="./images/tick.png" alt=""></span> Highly experienced builders</li>
                            <li><span><img src="./images/tick.png" alt=""></span> Best interior designs & flooring</li>
                            <li><span><img src="./images/tick.png" alt=""></span> Worth every penny you pay</li>
                            <li><span><img src="./images/tick.png" alt=""></span> No hidden & extra costs</li>
                            <li><span><img src="./images/tick.png" alt=""></span> Your happiness is our priority</li>
                            
                        </ul>


                    </div>
                </div>
            </div>

    </section>

    <div class="clearfix"></div>

    <section id="features">
        <div class="col-md-12">
            <div class="col-md-4" id="design">
                <div class="box">
                    <img src="./images/architecture design.png" alt="">
                    <h3 class="clr_primary">Construction</h3>
                    <p>
                        For all your home-building experience, we are your best solution. We build for you,
                        customized homes to fit both your lifestyle and pocket.
                        Partner with us for exceptional services.  
                    </p>
                    
                </div>
            </div>

            <div class="col-md-4" id="construction">
                <div class="box">
                    <img src="./images/building construction.png" alt="">
                    <h3 class="clr_primary">Real Estate</h3>
                    <p>
                        Our experience in this area is unmatched and we aid you to elevate the quality of your life.
                         Explore our services to know more.
                    </p>
                    
                </div>
            </div>

            <div class="col-md-4" id="renovation">
                <div class="box">
                    <img src="./images/building renovation2.png" alt="">
                    <h3 class="clr_primary">Interiors</h3>
                    <p>
                        We deliver clever & sensual interior designs for all your needs and taste. 
                        Because, interiors are an insight into the brains!
                    </p>
                    
                </div>
            </div>
        </div>
    </section>
    
    <div class="clearfix"></div> 
    <?php 
        $this->load->view('footer');
        $this->load->view('script_links');
    ?>
    <script>

        $(document).ready(function(){

            var $window = $(window);
            
            $window.scroll(function () {
                if ($window.scrollTop() > 20) {          

                $(".navbar").addClass('nav_blue');

                }else{
                $(".navbar").removeClass('nav_blue');
                }
            });

        });
    </script>

</body>

</html>