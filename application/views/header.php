<header> 
    <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>

            <a class="navbar-brand" href="<?php echo SITE_URL?>">
                <img src="./images/smack-logo.svg" alt="smack-logo">
            </a>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav float_right topBotomBordersOut">
                <li class="homeLink active"><a href="home" class="headerLinks">Home</a></li>
                <li class="aboutLink"><a href="about_us" class="headerLinks">About Us</a></li>
                <!-- <li class="interiorLink"><a href="interior" class="headerLinks">Interior</a></li> -->
                <li class="serviceLink"><a href="service" class="headerLinks">Our Services</a></li>                    
                <li class="contactLink"><a href="contact" class="headerLinks">Contact Us</a></li>
              </ul>
          </div>
          
        </div>
      </nav>
</header>