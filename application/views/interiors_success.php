<!DOCTYPE html>

<html lang="en">
    <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" href="<?php echo SITE_URL?>assets/interiors/images/favicon.jpg" />
      <title>Smak Concepts | Moulim Nagar</title>
    
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/interiors/style/vendor.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">          
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/interiors/style/interiors.css">

      <style>
        .error{
            color:#ff6262;
        }
      </style>

      <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160640646-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-160640646-1');
        </script>


        <style>
        
        header{
            position: relative;
            background-color: #000000d9;
        }

        video{
            width: 90%;
            height: 88vh;
        }

        .videoParentSec{
            background-color: #000;
            text-align: center;
        }

        </style>

   </head> 
   <body> 

   <header>

        <nav class="navbar">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo SITE_URL?>">
                        <img src="<?php echo SITE_URL?>assets/interiors/images/logo.png" alt="smack-logo">
                    </a>
                </div>
            </div>
        </nav>

    </header>

   
    <section class="videoParentSec">

        <video id="moulimNagarSuccess" controls="controls" autoplay muted>
            <source src="https://adcancdn.s3.ap-southeast-1.amazonaws.com/smak-concepts/video/thank_you.mp4" type="video/mp4" >
            Your browser does not support the video tag.
        </video>

    </section>

    <!-- <footer>
        <div class="container "> 
            <p class="">
               
                <span class="copyrightSec"> © 2020 Smak. All Rights Reserved.</span>

                <span class="footerLinks">
                    <a href="https://www.smakconcepts.com/terms_condition" target="_blank"> Terms & Condition </a>
                    <a href="https://www.smakconcepts.com/privacy" target="_blank"> Privacy Policy </a>
                </span>
            </p>
        </div>
    </footer>  -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script>

        $( document ).ready(function() {
            document.getElementById("moulimNagarSuccess").play();
        });

    </script>

   </body>

</html>