<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="<?php echo SITE_URL?>assets/constructions/images/favicon.png" />
        <title>Smak Constructions</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo SITE_URL?>assets/constructions/style/vendor.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">          
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        
        <link rel="stylesheet" href="<?php echo SITE_URL?>assets/constructions/style/constructions.css">

        <!-- Global site tag (gtag.js) - Google Ads: 609114811 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-609114811"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-609114811');
        </script>
        <!-- Event snippet for Lead conversion page -->
        <script>
            gtag('event', 'conversion', {'send_to': 'AW-609114811/nWyVCITvmNkBELu1uaIC'});
        </script>

      <style>
        .error{
            color:#ff6262;
        }
      </style>

      <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160640646-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-160640646-1');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '3897310283613857');
            fbq('track', 'PageView');
        </script>
        
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=3897310283613857&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->


        <style>
        
        header{
            position: relative;
            background-color: #000000d9;
        }

        video{
            width: 90%;
            height: 88vh;
        }

        .videoParentSec{
            background-color: #000;
            text-align: center;
        }

        </style>

   </head> 
   <body> 

   <header>

        <nav class="navbar">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo SITE_URL?>">
                        <img src="<?php echo SITE_URL?>assets/constructions/images/logo.svg" alt="smack-logo">
                    </a>
                </div>
            </div>
        </nav>

    </header>

   
    <section class="thankYouImage">
        <!-- <img src="<?php echo SITE_URL?>assets/interiors/images/thank_you.jpg" alt="" class="img-responsive"> -->
    </section>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


   </body>

</html>