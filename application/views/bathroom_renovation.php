<!DOCTYPE html>
<html lang="en"> 
    <?php 
        $this->load->view('head');
    ?>
<body>
    <?php 
        $this->load->view('header');
    ?>
    <div class="clearfix"></div>

    <!-- Banner -->
    <section id="banner"> 
        <div class="aboutus_banner">
            <img src="./images/bathroom_banner.jpg" alt="Banner" class="banner_img img-responsive webView">
            <img src="./images/bathroom_banner_mobile.jpg" alt="Banner" class="banner_img img-responsive mobView">           
            
            <div class=" container">
                <div class="banner-caption">
                    <div class="col-md-12 col-xs-12">
                        <h1>Bathroom
                            Renovation</h1>
                    </div>
                </div>
            </div>      
        </div> 
    </section>

    <div class="clearfix"></div>
    <!-- Description Section -->

    <section class="description_section">
        <div class="container">
            <div class="col-md-12">
                <div class="col-md-6 col-xs-12 col-sm-6 desc_img">
                    <img src="./images/bathroom_desc.jpg" alt="descrition image" class="img-responsive">
                </div>
                <div class="col-md-6 col-xs-12 col-sm-6 desc_text">
                    <p>
                        A team of skilled Civil Engineers, Interior Designers, Plumbing & Electrical professionals
                        for the top quality renovation that fits your budget as well as your house needs.
                        We offer tiles demolition/ replacement, plumbing and electrical repairs and all 
                        types of civil works to give your house a complete transformation.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>
    <!-- Services Offered Section -->
    <section id="services">
        <div class="container">
            <h1 class="sec_heading">SERVICES OFFERED</h1>
            <p class="sec_description">Select a service based on your requirement. </p>

            <div class="col-md-12 padTop25">
                <div class="col-md-4 col-sm-4 col-xs-12 service text_Centre">
                    <img src="./images/tiles.jpg" alt="" >
                    <h3>Tiles demolition / Replacement</h3>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 service text_Centre">
                    <img src="./images/plumber.jpg" alt="">
                    <h3>Plumbing / Electrical Repairs</h3>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 service text_Centre">
                    <img src="./images/civil.jpg" alt="" >
                    <h3>Civil works</h3>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>
    
    <!-- How to go about it? Section -->
    <section id="howItWorks" class="secondary_blue">
        <div class="container">
            <h1 class="sec_heading">How to go about it?</h1>
            <p class="sec_description">3 easy steps to get your flooring job done.</p>

            <div class="steps text_Centre">
                <div class="col-md-12 col-xs-12 col-sm-12 ">
                    
                    <div class="col-md-4 col-xs-12 step">
                        <img src="./images/design.png" alt="design" >
                        <p>Fill & send the enquiry form.</p>
                    </div>

                    <div class="col-md-3 col-xs-12 step">
                        <img src="./images/call.png" alt="call" >
                        <p>Receive a call regarding confirmation & other details.</p>
                    </div>

                    <div class="col-md-4 col-xs-12 step">
                        <img src="./images/confirm.png" alt="confirm" >
                        <p>Work starts within 1-2 days after approval.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>  

    <div class="clearfix"></div>
    <!-- Questionary Section -->

    <section class="Questionary">       

        <div class="container">
            <div class="col-md-12">
                <h1 class="sec_heading">QUESTIONNAIRE / DESIGN ENQUIRY</h1>
                <p class="sec_description">Let us know your requirements below and book your FREE consultation with a SMAK Design Expert:</p>
            </div>

            <form method="POST" id="architect_form">
                <input type="hidden" name="profession_type" id="profession_type" value="architect">
                <div class="col-md-12  col-sm-12 col-xs-12">
                    <div class="col-md-6 Question col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="sel1">*Looking for services</label>
                            <select class="form-control" id="service" name="service">
                              <option value="" disabled selected>Select Service</option>
                              <option value="architects">Architects</option>
                              <option value="flooring">Flooring  Experts</option>
                              <option value="carpentry">Carpentry / Woodworking</option>
                              <option value="bathroomrenovation">Bathroom  Renovation</option>
                              <option value="kitchen">Kitchen  Renovation</option>
                              <option value="grillwork">Metal Fabrication / Grillwork</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6 Question col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="sel2">*Location</label>
                            <input type="text" class="form-control" id="location" name="location" placeholder="Enter Location">
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 Question col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="contact_no">*Contact No</label>
                            <input type="text" name="contact_no" id="contact_no" class="form-control  only_numeric phone"  placeholder="Enter Contact No.">
                          </div>
                    </div>

                    <div class="col-md-6 Question col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="email">*Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter Mail Id">
                          </div>
                    </div>

                     <input type="hidden" id="message" name="message" value="i have interested in smakconcepts Bathroom Renovation.Please call me ASAP.">
                    <div class="clearfix"></div>
                    
                    <div class="col md-12 col-xs-12 text_Centre">
                        <button type="submit" class="btn btn-enquiry">Send Enqiury <span class="arrow"><img src="./images/arrow.png" alt="" class=""></span></button>
                    </div>
                </div>
            </form>

        </div>
    </section>

    <div class="clearfix"></div>

    <!-- Advantages Section -->

    <section id="advantages">
        <div class="container">
            <div class="col-md-12">
                <h1 class="sec_heading">SMAK ADVANTAGES</h1>
                <p class="sec_description">For all your construction needs</p>

                <div class="col-md-12 advants">
                    <div class="col-md-3 advant text_Centre">
                        <div class="top">
                            <img src="./images/certfied.png" alt="">
                            <p>Certified Construction Professionals</p>
                        </div>                        

                    </div>
                    
                    <div class="col-md-3 advant text_Centre">
                        <div class="top">
                            <img src="./images/reliable.png" alt="">
                            <p>Reliable Service Delivery</p>
                        </div>
                        

                    </div>

                    <div class="col-md-3 advant text_Centre">
                        <div class="top">
                            <img src="./images/payment.png" alt="">
                            <p>Standard Rates</p>
                        </div>                        

                    </div>

                    <div class="col-md-3 advant text_Centre">
                        <div class="top">
                            <img src="./images/discount.png" alt="">
                            <p>Discounts on Labor + Materials Packages</p>
                        </div>                        

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

     <!-- Testimonial Section -->

    <section id="testimonials">
        <div class="container">

            <h1 class="sec_heading">TESTIMONIALS</h1>

            <div class="testimonial_slider">
                <div class="center text_Centre .autoplay">
                    <div class="col-md-4">
                        <img src="./images/customer_2.jpg" alt="">
                        <p>Interiors they designed turned out to look just as I expected it to. I am satisfied with the work.</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_1.jpg" alt="">
                        <p>I got my kitchen upgraded and I have a modular kitchen I always dreamt of. Thank you SMAK</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_3.jpg" alt="">
                        <p>I cannot recognize my old house of almost 20+ years! Quality work at reasonable price</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_5.jpg" alt="">
                        <p>One of my friends recommended Smak for fabrication work & I don’t regret my decision</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_4.jpg" alt="">
                        <p>Plumbers who worked for my bathroom renovation were truly skilled! Hats off to them.</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_6.jpg" alt="">
                        <p>They not only delivered the work on time, they also helped me with selecting superior quality materials. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>
    
    <?php 
        $this->load->view('footer');
        $this->load->view('script_links');
    ?>
     <script>

        $(document).ready(function(){

            $('.only_numeric').on('keypress', function(e) {
                var $this = $(this);
                var regex = new RegExp("^[0-9\b]+$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                // for 10 digit number only
                if ($this.val().length > 9) {
                    e.preventDefault();
                    return false;
                }

                if (e.charCode > 57 && e.charCode < 48) {
                    if ($this.val().length == 0) {
                        e.preventDefault();
                        return false;
                    } else {
                        return true;
                    }
                }

                if (regex.test(str)) {
                    return true;
                }

                e.preventDefault();

                return false;
            });

            $.validator.addMethod(
                "regex",
                function(value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Please check your input."
            );

            $("#architect_form").validate({
                rules: {
                  service: {
                    required: true
                  },
                  location: {
                    required: true
                  },
                  contact_no: {
                    required: true,
                    regex:/^(0|91)?[6789]\d{9}$/,
                    number: true,
                  },
                  email: {
                    required: true,
                    email:true
                  }
                },
                messages : {
                    service: {
                        required: "Please select service"
                    },
                    location: {
                        required: "The location field is required"
                    },
                    contact_no: {
                        required: "The Contact Number field is required.",
                        number: "Only digits allowed",
                        regex: "Please enter the valid Contact number",
                        minlength: "Minimum 10 digit number required",
                        maxlength: "Maximum 10 digit number can enter"
                    },
                    email: {
                        required: "The Email field is required.",
                        email: "email should be in the format: abc@domain.tld"
                    },
                },
                onsubmit: true,
                submitHandler: function(form) {
                    saveFormViaAjax();
                }
            });


            var $window = $(window);
            
                $window.scroll(function () {
                    if ($window.scrollTop() > 20) {          

                    $(".navbar").addClass('nav_blue');

                    }else{
                    $(".navbar").removeClass('nav_blue');
                    }
                });


                $('.center ').slick({
                dots: true,
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 3,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                    }
                ]
            });

        });

    function saveFormViaAjax() {

        var base_url = '<?php echo base_url(); ?>';//empty the error messages

        var inputData = {
            'name':'',
            'service':$('#service').val(),
            'location':$('#location').val(),
            'mobile_no':$('#contact_no').val(),
            'email':$('#email').val(),
            'business_type':'bathroom_renovation',
            'project':'',
            utm_source:'bathroom_renovation',
            utm_term:'bathroom_renovation',
            utm_campaign:'bathroom_renovation',
            utm_content:'bathroom_renovation',
            utm_medium:'bathroom_renovation',
            city:'',
            ad:'bathroom_renovation',
            adpos: 'bathroom_renovation',
            message: $("#message").val()

        };

        $("#architect_form").find("input[type='submit']").attr("disabled", "disabled");
        $("#ajax_load").show();

        //submit
        $.ajax({
            type: 'POST',
            url: base_url+"/submit/submitArchitects",
            data: inputData,
            success : function(response){ 

                console.log( response )
                var parsedData = JSON.parse(response);
                $("#ajax_load").hide();

                if(parsedData.status == "success"){
                   
                    setTimeout(function(){ 
                        location.reload();
                    }, 3000);

                   
                    $.dialog({
                         title: 'Success!',
                        content: 'Request form submitted successfully!',
                    });                       
                }else if(parsedData.status == false && parsedData.message == 'failed'){
                    $.dialog({
                         title: 'Sorry!',
                        content: 'Something went wrong. Try again',
                    });
                }else if(parsedData.status == true && parsedData.message != ''){
                    $.dialog({
                         title: 'Thank You!',
                        content: 'Your request is already sent! we will get back to you soon',
                    });
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }

               
            } 
        });
   }
        
 </script>

</body>


<style>
#ajax_load {
    height: 100vh;
    left: 0!important;
    opacity: .6;
    position: fixed!important;
    top: 0!important;
    width: 100%;
    z-index: 10060;
    background: #fff;
}

#ajax_load img {
    height: 32px;
    left: 50%;
    margin-left: -16px;
    margin-top: -16px;
    position: absolute;
    top: 50%;
    width: 32px;
    z-index: 999;
}

</style>

<div id="ajax_load" style="display:none;">
    <img src="<?php echo SITE_URL; ?>images/ajax-loader.gif" alt="">
</div>
