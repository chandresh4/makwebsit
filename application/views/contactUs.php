<!DOCTYPE html>
<html lang="en"> 
    <?php 
        $this->load->view('head');
    ?>
<body>
    <?php 
        $this->load->view('header');
    ?>
    <div class="clearfix"></div>

    <!-- Banner -->
    <section id="banner">   
        <div class="aboutus_banner">
            <img src="./images/contactus.jpg" alt="Banner" class="banner_img img-responsive webView">
            <img src="./images/contactus_mobile.jpg" alt="Banner" class="banner_img img-responsive mobView">
            <div class=" container">
                <div class="banner-caption">
                    <div class="col-md-12 col-xs-12">
                        <h1>Contact Us</h1>
                    </div>
                </div>
            </div>       
        </div>
    </section>

    <div class="clearfix"></div>

    <!--Contact Form Section -->

    <section class="contact">
        <div class="container">
            <div class="col-md-6 col-xs-12 col-sm-6 contactbox form">
                <form class="mui-form" id="contactus" method="POST">
                    <legend>Get In Touch</legend>
                    <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" name="user_name" id="user_name" autofocus>
                        <label for="name">Name</label>
                    </div>
                    <div class="mui-textfield mui-textfield--float-label">
                        <input type="email" name="email" id="email" >
                        <label>Mail ID</label>
                    </div>
                    <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" name="contact_no" id="contact_no" class="only_numeric">
                        <label>Contact No.</label>
                    </div>
                    <div class="mui-textfield mui-textfield--float-label">
                        <textarea type="text" id="message" name="message"></textarea>
                        <label>Your Message</label>
                    </div>
                            
                    <input type="hidden" id="business_type" name="business_type" value="">
                    <input type="hidden" id="project" name="project" value="">

                    <button type="submit" class="mui-btn mui-btn--raised">Submit</button>
                </form>
            </div>

            <div class="col-md-6 col-xs-12 col-sm-6 contactbox address">
                <h1>Contact Info</h1>
                <div class="col-md-12 block col-xs-12">
                    <div class="col-md-1 col-xs-2">
                        <img src="./images/location.png" alt="" class="img-responsive">
                    </div>

                    <div class="col-md-11 col-xs-10">
                        <p>
                            No.12, 11th Block,Dr Raj Kumar road, <br> J.S.S. Layout, Mysore-19
                        </p>
                    </div>
                </div>

                <div class="col-md-12 col-xs-12 block">
                    <div class="col-md-1 col-xs-2">
                        <img src="./images/tel.png" alt="" class="img-responsive">
                    </div>

                    <div class="col-md-11 col-xs-10">
                        <p class="mailSec"> <a href="tel:9886644767">+91 9886644767</a> </p>
                    </div>

                </div>

                <div class="col-md-12 col-xs-12 block">
                    <div class="col-md-1 col-xs-2">
                        <img src="./images/mail.png" alt="" class="img-responsive">
                    </div>

                    <div class="col-md-11 col-xs-10">
                        <p class="mailSec"> <a href="mailto:info@smakconcepts.com" target="_top"> info@smakconcepts.com </a></p>
                    </div>

                </div>
            </div>
        </div>
    </section> 


    <div class="clearfix"></div>

    <section id="map">
        <div class="col-md-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3897.8354054039405!2d76.68873131484523!3d12.32686303203194!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3baf71b2783ec3f5%3A0xbb6a8086cd5be473!2s19%2C%20Dr%20Rajkumar%20Main%20Rd%2C%20Kalyanagirinagara%2C%20Azeez%20Sait%20Nagar%2C%20Sathagalli%20Layout%2C%20Mysuru%2C%20Karnataka%20570019!5e0!3m2!1sen!2sin!4v1581588737257!5m2!1sen!2sin" width="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </section>
    <div class="clearfix"></div>
   
    <?php 
        $this->load->view('footer');
        $this->load->view('script_links');
    ?>
     <script>
        $(document).ready(function() {


            $('.only_numeric').on('keypress', function(e) {
                var $this = $(this);
                var regex = new RegExp("^[0-9\b]+$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                // for 10 digit number only
                if ($this.val().length > 9) {
                    e.preventDefault();
                    return false;
                }

                if (e.charCode > 57 && e.charCode < 48) {
                    if ($this.val().length == 0) {
                        e.preventDefault();
                        return false;
                    } else {
                        return true;
                    }
                }

                if (regex.test(str)) {
                    return true;
                }

                e.preventDefault();

                return false;
            });

            $.validator.addMethod(
                "regex",
                function(value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Please check your input."
            );
            
            jQuery.validator.addMethod("letterswithspace", function(value, element) {
                return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
            }, "letters only");

            $("#contactus").validate({
                rules: {
                  user_name : {
                    required: true,
                    minlength: 3,
                    maxlength: 30,
                    letterswithspace: /^[a-z][a-z\s]*$/
                  },
                  email: {
                    required: true,
                    email: true
                  },
                  contact_no: {
                    required: true,
                    regex:/^(0|91)?[6789]\d{9}$/, 
                    number: true,
                    minlength: 10,
                    maxlength: 10
                  },
                  city: {
                    required: true,
                    letterswithspace: /^[a-z][a-z\s]*$/
                  }
                },
                messages : {
                  user_name: {
                    required: "Please enter your name",
                    minlength: "Name should be at least 3 characters",
                    maxlength: "Name should be Maximum 30 characters",
                    letterswithspace: "Only alphabetic characters allowed"
                  },
                  email: {
                    required: "Please enter your Email Id",
                    email: "email should be in the format: abc@domain.tld"
                  },
                  contact_no: {
                    required: "Please enter your contact number",
                    number: "Contact number only digits",
                     regex: "Please enter the valid contact number",
                    minlength: "Minimum 10 digit number required",
                    maxlength: "Maximum 13 digit number can enter"
                  },
                  city: {
                    required: "Please enter your city",
                    letterswithspace: "Only alphabetic characters allowed"
                  },
                },

                onsubmit: true,
                submitHandler: function(form) {

                    event.preventDefault();
                    var baseURL = "<?php echo base_url(); ?>";

                    var inputData = {
                        name: $("#user_name").val(),
                        mobile_no: $("#contact_no").val(),
                        email: $("#email").val(),
                        city: $("#city").val(),
                        utm_source:'contactus',
                        utm_term:'contactus',
                        utm_campaign:'contactus',
                        utm_content:'contactus',
                        utm_medium:'contactus',
                        business_type:'',
                        project:'',
                        city:'',
                        ad:'contactus',
                        adpos: 'contactus',
                        message: $("#message").val()
                    };
                    
                    $("#ajax_load").show();

                    $.ajax({
                        type: 'POST',
                        url: baseURL+"submit/submitContactUs",
                        data: inputData,
                        success : function(response){ 
                            var parsedData = JSON.parse(response);
                            $("#ajax_load").hide();
                            if(parsedData.status == "success"){
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                                document.getElementById("contactus").reset();
                                    $.dialog({
                                         title: 'Success!',
                                        content: 'Request form submitted successfully!',
                                    });                       
                               
                            }else if ( parsedData.status == "true" || parsedData.status == true ) {

                                setTimeout(function(){ 
                                    location.reload();
                                }, 10000);

                                $.dialog({
                                        title: 'Thank You!',
                                        content: 'Your request is already sent! we will get back to you soon',
                                    });  
                            }else {
                                location.reload();
                                
                            }
                        } 
                    });


                }
            });


    });

    $(document).ready(function(){

        var $window = $(window);
        
        $window.scroll(function () {
            if ($window.scrollTop() > 20) {          

            $(".navbar").addClass('nav_blue');

            }else{
            $(".navbar").removeClass('nav_blue');
            }
        });

    });
    </script>
</body>

<div id="ajax_load" style="display:none;">
    <img src="<?php echo SITE_URL; ?>images/ajax-loader.gif" alt="">
</div>

<style>
#ajax_load {
    height: 100vh;
    left: 0!important;
    opacity: .6;
    position: fixed!important;
    top: 0!important;
    width: 100%;
    z-index: 10060;
    background: #fff;
}

#ajax_load img {
    height: 32px;
    left: 50%;
    margin-left: -16px;
    margin-top: -16px;
    position: absolute;
    top: 50%;
    width: 32px;
    z-index: 999;
}

</style>