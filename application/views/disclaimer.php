<!DOCTYPE html>
<html lang="en"> 
    <?php 
        $this->load->view('head');
    ?>
<body>
    <?php 
        $this->load->view('header');
    ?>
    <div class="clearfix"></div>

    <!-- Terms and Condition Banner -->

    <section id="banner">   
        <div class="aboutus_banner">
            <img src="./images/disclaimer.jpg" alt="Banner" class="banner_img img-responsive webView">
            <img src="./images/disclaimer_mobile.jpg" alt="Banner" class="banner_img img-responsive mobView">
            <div class=" container">
                <div class="banner-caption">
                    <div class="col-md-12 col-xs-12">
                        <h1> Disclaimer </h1>
                    </div>
                </div>
            </div>       
        </div>
    </section>

    <div class="clearfix"></div>

    <!--terms and Condition Section -->

    <section class="terms">

        <div class="container">

            <h4>Thank you for visiting The SMAK Concepts website.</h4>

            <div class="clearfix"></div>

            <p>
                No information given on this Website creates a warranty or expands the scope of any warranty
                that cannot be disclaimed under applicable law.
                Your use of the Website is solely at your own risk
            </p>

            <div class="clearfix"></div>

            <p>
                The Website is only promotional, none of the images, material, stock photography, projections, details,
                descriptions and other information that are currently available and/or displayed on the
                website including details of the projects/developments undertaken by the company including depictions by
                banners/posters of the project, should be deemed to be or constitute advertisements, solicitations, marketing,
                offer for sale, invitation to offer, invitation to acquire,
                including within the purview of the RERA.
            </p>

            <div class="clearfix"></div>

            <p>
                You are therefore requested to directly verify all details and aspects of any proposed booking/acquisition of units/premises,
                directly with our authorized sales team. Please do not rely on the information contained on this website,
                until our revision and update is complete.
            </p>

            <div class="clearfix"></div>

            <p>
                Please note, that we will not be accepting any bookings or allotments based on the images,
                material, stock photography, projections, details, descriptions that are currently available and/or displayed on the Website.
                We advise you to contact our Sales Team for further information.
                To send periodic emails.
            </p>

            <div class="clearfix"></div>

            <p>We thank you for your patience and understanding.</p>

        </div>

    </section> 


    <div class="clearfix"></div>
   
    <?php 
        $this->load->view('footer');
        $this->load->view('script_links');
    ?>
    <script>

        $(document).ready(function(){

            var $window = $(window);
            
            $window.scroll(function () {
                if ($window.scrollTop() > 20) {          

                $(".navbar").addClass('nav_blue');

                }else{
                $(".navbar").removeClass('nav_blue');
                }
            });

        });

    </script>
</body>