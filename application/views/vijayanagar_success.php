<!DOCTYPE html>

<html lang="en">
    <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" href="<?php echo SITE_URL?>assets/vijay_nagar/images/favicon.png" />
      <title>Smak Concepts | Vijayanagar</title>
    
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/vijay_nagar/style/vendor.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">          
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/vijay_nagar/style/main.css">

      <style>
        .error{
            color:#ff6262;
        }
      </style>

      <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160640646-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-160640646-1');
        </script>


        <style>
            header{
                position: relative;
                background-color: #000000d9;
            }
        </style>

        <!-- Global site tag (gtag.js) - Google Ads: 609045498 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-609045498"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-609045498');
        </script>

        <!-- Event snippet for Lead conversion page -->
        <script>
            gtag('event', 'conversion', {'send_to': 'AW-609045498/yAJLCOaQ-9gBEPqXtaIC'});
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '3897310283613857');
            fbq('track', 'PageView');
        </script>
        
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=3897310283613857&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

   </head> 
   <body> 

    <header>

        <div class="logoSec">
            <a href="<?php echo SITE_URL."vijayanagar"; ?>"><img src="<?php echo SITE_URL?>assets/vijay_nagar/images/white_logo.svg" alt="" class="handPointer"></a>
        </div>

        <div class="contactSec webView">
            <a href="tel:+91 9886644767">Call Us: +91 9886644767</a>
        </div>

    </header>

   
    <section class="thankYouImage">
        <!-- <img src="<?php echo SITE_URL?>assets/interiors/images/thank_you.jpg" alt="" class="img-responsive"> -->
    </section>

    <footer>
        <div class="container "> 
            <p class="">
                <span class="copyrightSec"> © 2020 Smak. All Rights Reserved.</span>

                <span class="footerLinks">
                    <a href="https://www.smakconcepts.com/terms_condition" target="_blank"> Terms & Condition </a>
                    <a href="https://www.smakconcepts.com/privacy" target="_blank"> Privacy Policy </a>
                </span>
            </p>
        </div>
    </footer> 

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


   </body>

</html>