<!DOCTYPE html>
<html lang="en"> 
    <?php 
        $this->load->view('head');
    ?>
<body>

    <header> 
        <nav class="navbar navbar-default" style="background-color: #001F3A ;">
            <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo SITE_URL?>">
                    <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/white_logo.svg" alt="smack-logo">
                </a>
            </div>
            
            </div>
        </nav>
    </header>

    <div class="clearfix"></div>

    <section class="errorPageContainer">  
        
    </section>
    
    <?php 
        $this->load->view('script_links');
    ?>
     <script>

        $(document).ready(function(){

            var $window = $(window);
            
            $window.scroll(function () {
                if ($window.scrollTop() > 20) {          

                $(".navbar").addClass('nav_blue');

                }else{
                $(".navbar").removeClass('nav_blue');
                }
            });

        });
    </script>

</body>

</html>