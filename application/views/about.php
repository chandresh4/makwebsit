<!DOCTYPE html>
<html lang="en"> 
    <?php 
        $this->load->view('head');
    ?>
<body>
    <?php 
        $this->load->view('header');
    ?>
    <div class="clearfix"></div>

    <!-- Banner -->

    <section id="banner">  
        <div class="aboutus_banner">
                <img src="./images/aboutus_banner.jpg" alt="Banner" class="banner_img img-responsive webView">
                <img src="./images/aboutus_banner_mobile.jpg" alt="Banner" class="banner_img img-responsive mobView">           
            
            <div class=" container">
                <div class="banner-caption">
                    <div class="col-md-12 col-xs-12">
                        <h1>About Us </h1>
                    </div>
                </div>   
            </div>  
        </div> 
    </section>

    <div class="clearfix"></div>

    <!-- Description Section -->


    <section class="description_section">

        <div class="container">
            <div class="col-md-12">
                
                <div class="col-md-12 col-xs-12 col-sm-12  aboutdesc">
                    <h3><b>GET TO KNOW US</b></h3>
                    <h1>We are the Brand you can Trust</h1>
                    <p>
                    We are highly experienced construction company in Bangalore possessing skilled teams of contractors, engineers,
                    architects and other professionals who ensure to exceed your expectations.
                    We guarantee hassle-free construction and renovation experience and top quality architect designs.
                    We offer a complete package for all your construction needs. Our punctuality in delivering the services is unparalleled. 
                    Contact us today and let us know what is it that you are looking for! 
                    </p>

                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 desc_img">
                    <img src="./images/about.png" alt="descrition image" class="img-responsive">

                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12  strengths">
                <h2>Our strength lies in</h2>
                <ul class="flexCol strength">
                    <li><i>Blending feasibilty and innovation</i></li>
                    <li><i>Providing customer centric services</i></li>
                    <li><i>Retaining quality of international standard</i></li>
                    <li><i>Maintaining our integrity & responsibility</i></li>
                </ul>

            </div>

        </div>

    </section>

    <!-- VAlues Section -->

    <section id="values">
        <div class="container">
            <div class="col-md-12">
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <div class="core-values">
                        <h1>Core Values</h1>
                        <p><span><img src="./images/bar.png" alt="bar"></span> Timely Delivery </p>
                        <p><span><img src="./images/bar.png" alt="bar"></span> Certified Quality </p>
                        <p><span><img src="./images/bar.png" alt="bar"></span> Transparency </p>
                        <p><span><img src="./images/bar.png" alt="bar"></span> Best Prices </p>

                    </div>
                    <div class="core-values">
                        <h1>Core Purpose</h1>
                        <p><span><img src="./images/bar.png" alt="bar"></span> To make your dream project a reality</p>
                        <h1 class="padTop25">Brand Promise</h1>
                        <p><span><img src="./images/bar.png" alt="bar"></span> We assure 100% Customer satisfaction</p> 
                    </div>

                </div>
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <div class="core-values right">
                        <h1>Our Mission</h1>
                        <p><i>Our motto is to deliver great workmanship with commitment and dedication to our customers.</i></p>
                        <h1 class="padTop25">Our Vision</h1>
                        <p><i>We equip ourselves and empower the whole team to meet the demands and expectations of our clients.</i></p> 
                    </div>
                </div>
            </div>
        </div>

    </section>

    <div class="clearfix"></div>
    
    <?php 
        $this->load->view('footer');
        $this->load->view('script_links');
    ?>
     <script>

        $(document).ready(function(){

            var $window = $(window);
            
            $window.scroll(function () {
                if ($window.scrollTop() > 20) {          

                $(".navbar").addClass('nav_blue');

                }else{
                $(".navbar").removeClass('nav_blue');
                }
            });

        });
    </script>

</body>

</html>