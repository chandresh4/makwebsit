    <script>
        $(document).ready(function(){
            $('#otp3').on('keypress', function(e) {
                $("#otp3-err").text('');
            });
        });

        $(".estimateArea").click( function(){
            $("#name3").focus();
        })
    
        var baseURL = "<?php echo base_url(); ?>";
        
        window.addEventListener( "pageshow", function ( event ) {
            var historyTraversal = event.persisted || ( typeof window.performance != "undefined" && window.performance.navigation.type === 2 );
            if ( historyTraversal ) {
                // Handle page restore.
                window.location.reload();
            }
        });
        
        $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
        );
        
        jQuery.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "letters only");

        $("#feedbackForm1").validate({
            rules: {
              name : {
                required: true,
                minlength: 3,
                maxlength: 30,
                letterswithspace: /^[a-z][a-z\s]*$/
              },
              phone: {
                required: true,
                number: true,
                regex:/^(0|91)?[6789]\d{9}$/, 
                minlength: 10,
                maxlength: 10
              },
              email: {
                required: true,
                email: true
              },
              city: {
                required: true,
                letterswithspace: /^[a-z][a-z\s]*$/
              }
            },
            messages : {
                name: {
                    required: "The Name field is required.",
                    minlength: "Name should be at least 3 characters",
                    maxlength: "Name should be Maximum 30 characters",
                    letterswithspace: "Only alphabetic characters allowed"
                },
                phone: {
                    required: "The Mobile Number field is required.",
                    number: "Only digits allowed",
                    regex: "Please enter the valid Phone number",
                    minlength: "Minimum 10 digit number required",
                    maxlength: "Maximum 10 digit number can enter"
                },
                email: {
                    required: "The Email field is required.",
                    email: "email should be in the format: abc@domain.tld"
                },
                city: {
                    required: "The City name field is required",
                    letterswithspace: "Only alphabetic characters allowed"
                }
            },
            
            onsubmit: true,
            submitHandler: function(form) {
                saveFormViaAjax( "form_1" );
            }
        });
        
        $("#otpFrom3").validate({
            rules: {
              otp: {
                required: true,
                number: true,
                minlength: 4,
                maxlength: 4,
              }
            },
            messages : {
                otp: {
                    required: "The OTP field is required.",
                    number: "Only digits allowed",
                    minlength: "Minimum 4 digit number required",
                    maxlength: "Maximum 4 digit number can enter",
                     regex: "Please enter the valid Phone number",
                }
            },
            
            submitHandler: function() {
                
                submitOtpForm( "form_3" );
            }
        });
        
        $("#otpFrom1").validate({
            rules: {
              otp: {
                required: true,
                number: true,
                minlength: 4,
                maxlength: 4,
              }
            },
            messages : {
                otp: {
                    required: "The OTP field is required.",
                    number: "Only digits allowed",
                    minlength: "Minimum 4 digit number required",
                    maxlength: "Maximum 4 digit number can enter",
                    regex: "Please enter the valid Phone number",
                }
            },
            
            submitHandler: function() {
                
                submitOtpForm( "form_1" );
            }
        });
        
        $("#otpFrom2").validate({
            rules: {
              otp: {
                required: true,
                number: true,
                minlength: 4,
                maxlength: 4,
              }
            },
            messages : {
                otp: {
                    required: "The OTP field is required.",
                    number: "Only digits allowed",
                    minlength: "Minimum 4 digit number required",
                    maxlength: "Maximum 4 digit number can enter",
                    regex: "Please enter the valid Phone number",
                }
            },
            
            submitHandler: function() {
                
                submitOtpForm( "form_2" );
            }
        });
        
        $("#feedbackForm3").validate({
            rules: {
              name : {
                required: true,
                minlength: 3,
                maxlength: 30,
                letterswithspace: /^[a-z][a-z\s]*$/
              },
              phone: {
                required: true,
                number: true,
                regex:/^(0|91)?[6789]\d{9}$/, 
                minlength: 9,
                maxlength: 11
              },
              email: {
                required: true,
                email: true
              },
              city: {
                required: true,
                letterswithspace: /^[a-z][a-z\s]*$/
              }
            },
            messages : {
                name: {
                    required: "The Name field is required.",
                    minlength: "Name should be at least 3 characters",
                    maxlength: "Name should be Maximum 30 characters",
                    letterswithspace: "Only alphabetic characters allowed"
                },
                phone: {
                    required: "The Mobile Number field is required.",
                    number: "Only digits allowed",
                    regex: "Please enter the valid Phone number",
                    minlength: "Minimum 10 digit number required",
                    maxlength: "Maximum 10 digit number can enter"
                },
                email: {
                    required: "The Email field is required.",
                    email: "email should be in the format: abc@domain.tld"
                },
                city: {
                    required: "The City name field is required",
                    letterswithspace: "Only alphabetic characters allowed"
                }
            },
            
            onsubmit: true,
            submitHandler: function() {
                
                saveFormViaAjax( "form_3" );
            }
        });
        
        $("#feedbackForm2").validate({
            rules: {
              name : {
                required: true,
                minlength: 3,
                maxlength: 30,
                letterswithspace: /^[a-z][a-z\s]*$/
              },
              phone: {
                required: true,
                number: true,
                regex:/^(0|91)?[6789]\d{9}$/, 
                minlength: 9,
                maxlength: 11
              },
              email: {
                required: true,
                email: true
              },
              city: {
                required: true,
                letterswithspace: /^[a-z][a-z\s]*$/
              }
            },
            messages : {
                name: {
                    required: "The Name field is required.",
                    minlength: "Name should be at least 3 characters",
                    maxlength: "Name should be Maximum 30 characters",
                    letterswithspace: "Only alphabetic characters allowed"
                },
                phone: {
                    required: "The Mobile Number field is required.",
                    number: "Only digits allowed",
                    regex: "Please enter the valid Phone number",
                    minlength: "Minimum 10 digit number required",
                    maxlength: "Maximum 10 digit number can enter"
                },
                email: {
                    required: "The Email field is required.",
                    email: "email should be in the format: abc@domain.tld"
                },
                city: {
                    required: "The City name field is required",
                    letterswithspace: "Only alphabetic characters allowed"
                }
            },
            
            onsubmit: true,
            submitHandler: function() {
                
                saveFormViaAjax( "form_2" );
            }
        });
        
        $(".resend-otp").on("click", function(){ 
        
            var formType = $(this).attr("data-form-type");
            var id = $(this).attr("data-id");
            
            var inputData = {
                id: id
            };
            
            $.ajax({
                type: 'POST',
                url: baseURL+"submit/resendOtp",
                data: inputData,
                success : function(response){ 

                    var parsedData = JSON.parse(response);
                    if (parsedData.status == "success") {
                       
                        $("#otp"+formType+"-msg").show();
                        $("#otp"+formType+"-error").hide();
                        $("#otp"+formType+"-msg").html( parsedData.msg );
                        
                        setTimeout(function(){
                            $("#otp"+formType+"-msg").fadeOut(500);
                        }, 1500);
                    }
                } 
            });
        });
        
        function submitOtpForm ( formType ) {
            
            var inputData = {
                id: $("."+formType+"-otp-submit-btn").attr("data-id"),
                otp: $(".otp-"+formType).find(".otp").val()
            };
            
            $.ajax({
                type: 'POST',
                url: baseURL+"submit/matchOtp",
                data: inputData,
                success : function(response){ 

                    var parsedData = JSON.parse(response);

                    if(parsedData.status == "success"){

                        $.dialog({
                            title: 'Success!',
                            content: 'OTP successfully verified',
                        }); 
                        
                        setTimeout(function(){
                            var project = $("#project").val();

                            if ( project == 'Vijayanagar' ) {

                                window.location.replace( baseURL + "vijayanagar_success");
                            }else if ( project == 'smak-meadows' ) {
                                window.location.replace( baseURL + "smak_meadows/smak_meadows_success");
                            }else if ( project == 'building-planner' ) {
                                window.location.replace( baseURL + "building_planner_success");
                            }else {
                                window.location.replace( baseURL + "constructions/constructions_success");
                            }
							
                        }, 4000);
                        
                    }else {


                        if(parsedData.status == "fail" || response.status=="fail")
                        {

                            if ( formType == 'form_3' ) {
                                $("span#otp3-error").html("");
                                $("label#otp3-error").html("");
                                $("#otp3-error").show();
                                $("#otp3-error").html( "Given OTP is not correct" );                     
                            }else if (formType == 'form_1' ) {
                                $("span#otp1-error").html("");
                                $("label#otp1-error").html("");
                                $("#otp1-error").show();
                                $("#otp1-error").html( "Given OTP is not correct" );
                            }else if (  formType == 'form_2' ) {
                                $("span#otp2-error").html("");
                                $("label#otp2-error").html("");
                                $("#otp2-error").show();
                                $("#otp2-error").html( "Given OTP is not correct" );
                            }                            
                        }


                    }
                } 
            });
        }
        
        function saveFormViaAjax ( formType ) {
            console.log(formType);
            var inputData = {
                name: $("."+formType).find("input[name='name']").val(),
                mobile_no: $("."+formType).find("input[name='phone']").val(),
                email: $("."+formType).find("input[name='email']").val(),
                city: $("."+formType).find("input[name='city']").val(),
                utm_source: $("#utm_source").val(),
                utm_term: $("#utm_term").val(),
                utm_campaign: $("#utm_campaign").val(),
                utm_content: $("#utm_content").val(),
                utm_medium: $("#utm_medium").val(),
                ad: $("#ad").val(),
                adpos: $("#adpos").val(),
                business_type: $("#business_type").val(),
                project: $("#project").val(),
            };
            
            $("."+formType).find("input[type='submit']").attr("disabled", "disabled");
            $("#ajax_load").show();

            $.ajax({
                type: 'POST',
                url: baseURL+"submit/submitEnquiryFrom",
                data: inputData,
                success : function(response){ 
                    console.log( response )
                    var parsedData = JSON.parse(response);
                    $("#ajax_load").hide();

                    if(parsedData.status == "success"){
                       
                       $("."+formType).hide();
                       $(".otp-"+formType).show();
                       $("."+formType+"-resend-otp").attr("data-id", parsedData.enquiry);
                       $("."+formType+"-otp-submit-btn").attr("data-id", parsedData.enquiry);
                       
                    }else if(parsedData.status == false && parsedData.message == 'failed'){
                        $.dialog({
                             title: 'Sorry!',
                            content: 'Something went wrong. Try again',
                        });
                    }else if(parsedData.status == true && parsedData.message != ''){
                        $.dialog({
                             title: 'Thank You!',
                            content: 'Your request is already sent! we will get back to you soon',
                        });
                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    }
                } 
            });
        }

        $(".helpBtn").click(function(){
            $('html, body').animate({
                scrollTop: $("#myCarousel").offset().top
            }, 2000);
        });

        $(".otp").on("change", function(){ 

            $(this).parent().find("label").html("");;
        });

        $('.mobile_no').on('keypress', function(e) {
            var $this = $(this);
            var regex = new RegExp("^[0-9\b]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            // for 10 digit number only
            if ($this.val().length > 9) {
                e.preventDefault();
                return false;
            }

            if (e.charCode > 57 && e.charCode < 48) {
                if ($this.val().length == 0) {
                    e.preventDefault();
                    return false;
                } else {
                    return true;
                }
            }

            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();

            return false;
        });


        $('.otp').on('keypress', function(e) {
           
            var $this = $(this);
            var regex = new RegExp("^[0-9\b]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            // for 10 digit number only
            if ($this.val().length > 3) {
                e.preventDefault();
                return false;
            }

            if (e.charCode > 57 && e.charCode < 48) {
                if ($this.val().length == 0) {
                    e.preventDefault();
                    return false;
                } else {
                    return true;
                }
            }

            if (regex.test(str)) {
                return true;
            }

            e.preventDefault();

            return false;
        });

        $('.phone').on('blur input', function() {

            var formattedPhone = $(this).val().replace(/\D/g, '');

            if ( formattedPhone.length > 10 ) {

                $(this).val( formattedPhone.substring(0, 10) );
            }else {
                $(this).val( formattedPhone );
            }
        });

        $('.otp').on('blur input', function() {

            var formattedOtp = $(this).val().replace(/\D/g, '');

            if ( formattedOtp.length > 4 ) {

                $(this).val( formattedOtp.substring(0, 4) );
            }else {
                $(this).val( formattedOtp );
            }
        });



    </script>

    <script>
        $('.carousel-inner').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery:{
                enabled:true
            }
        });

        $('.aboutImageSec').magnificPopup({
            delegate: 'a',
            type: 'image'
        });
    </script>

    <script>

        if(window.innerWidth>900){
        
            function hover(ele){
                var id= ele.id;
                document.querySelector(`.${id}`).style.visibility='visible';    
            }

            function mouseOut(ele){
                var id= ele.id;
                document.querySelector(`.${id}`).style.visibility='hidden';    
            }

        }

    </script>


<style>
#ajax_load {
    height: 100vh;
    left: 0!important;
    opacity: .6;
    position: fixed!important;
    top: 0!important;
    width: 100%;
    z-index: 10060;
    background: #fff;
}

#ajax_load img {
    height: 32px;
    left: 50%;
    margin-left: -16px;
    margin-top: -16px;
    position: absolute;
    top: 50%;
    width: 32px;
    z-index: 999;
}

</style>

<div id="ajax_load" style="display:none;">
    <img src="<?php echo SITE_URL; ?>images/ajax-loader.gif" alt="">
</div>

