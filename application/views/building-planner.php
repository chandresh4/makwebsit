<!DOCTYPE html>

<html lang="en">
    <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" href="<?php echo SITE_URL?>assets/vijay_nagar/images/favicon.png" />
      <title>Smak Concepts | Building Planner</title>
    
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/building-planner/style/vendor.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">          
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/building-planner/style/main.css">

        <!-- Global site tag (gtag.js) - Google Ads: 609114811 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-609114811"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-609114811');
        </script>
      

      <style>
        .error{
            color:#ff6262;
        }

      </style>

      <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160640646-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-160640646-1');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '3897310283613857');
            fbq('track', 'PageView');
        </script>
        
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=3897310283613857&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

   </head> 
   <body> 
    <section>
        
        <header>
        
        <div class="logoSec">
            <a href="<?php echo SITE_URL."anvit-meadows"; ?>"><img src="<?php echo SITE_URL?>assets/building-planner/images/smak-logo.svg" alt="" class="handPointer"></a>
        </div>


            <div class="contactSec webView">
                <a href="tel:+91 9886644767">Call Us: +91 9886644767</a>
            </div>

        </header>

        <!-- banner -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">

                    <div class="webView">
                        <div class="bannerContent">
                            <h1>A Personalized House Plan <br> is all you need!</h1>

                            <p>3D Floor plans & Structural Designs 

                            <br> Made Simple & Affordable for you!<br> </p>
 
                            <button class="btn btn-primary contactArea">Contact Us Now ➜ </button>
                            
                        </div>
                        <img src="<?php echo SITE_URL?>assets/building-planner/images/banner.jpg"  style="width:100%;" class="img-responsive" >
                    </div>

                    <div class="mobView">
                        <img src="<?php echo SITE_URL?>assets/building-planner/images/banner_mobile.jpg"  style="width:100%;" class="img-responsive">
                    </div>

                    <div class="staticForm webView">

                        <form role="form" id="feedbackForm3" class="feedbackForm form_3" method="POST">
                            
                            <h4>Fill the form! We'll call you back</h4>
                            
                            <div class="group">

                                <div class="form-group">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <input type="text" class="form-control" id="name3" name="name" placeholder="Name" data-attr="Please enter correct name">
                                </div>

                                <div class="form-group">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                                </div>

                            </div>  

                            <div class="group">

                                <div class="form-group">
                                    <i class="fa fa-phone" aria-hidden="true"></i>  
                                    <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="countryCode3">
                                    <input type="text" class="form-control only_numeric phone" id="phone3" name="phone"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                                </div>

                            </div>  
                            
                            <div class="group">

                                <div class="form-group">
                                    <input type="text" class="form-control city" id="city3" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                                </div>
            
                            </div>  

                            <input type="hidden" id="utm_source3" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_sr"); ?>">
                            <input type="hidden" id="utm_medium3" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "d_sr"); ?>">
                            <input type="hidden" id="utm_sub3" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : "d_sr"); ?>">
                            <input type="hidden" id="utm_campaign3" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "d_sr"); ?>">

                            <div class="submitbtncontainer wrap">
                                <input type="submit" class="button" value="Submit" name="submit">
                            </div>

                        </form>

                        
                        <form action="" id="otpFrom3" class="feedbackForm otp-form_3" style="display: none;" novalidate="novalidate">

                            <div class="group">
                                <div class="form-group">
                                    <i class="fa fa-phone" aria-hidden="true"></i>  
                                    <input type="text" class="form-control only_numeric otp phone" id="otp3" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                                    <label id="otp3-error" class="error" for="otp3"></label>
    		                        <span id="otp3-msg"></span>
                                </div>
                            </div> 

                            <div class="group">
                                <div class="form-group" style="margin-bottom: 0; text-align: right;  cursor:pointer;">
                                    <span href="javascript:void(0);" data-form-type="3" class="form_3-resend-otp resend-otp">Resend OTP</span>
                                </div>
                            </div> 
                            
                            <div class="submitbtncontainer wrap">
                                <input type="submit" class="button form_3-otp-submit-btn" value="Submit OTP" name="submit">
                            </div>

                            <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

                        </form>

                    </div>
                
                </div>           
            </div>
            <!-- Left and right controls --> 
        </div>

        <div class="mobileFormDiv"> 
            <form role="form" id="feedbackForm2" class="feedbackForm form_2 mobView mobileForm" method="POST">
                <h4>Fill the form! We'll call you back</h4>
                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control" id="name2" name="name" placeholder="Name" data-attr="Please enter correct name">
                    </div>


                    <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                    </div>

                </div>  

                <div class="group">

                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                        <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="countryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone2" name="phone"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                    </div>

                </div>   

                <div class="group">

                    <div class="form-group">
                        <input type="text" class="form-control city" id="city2" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                    </div>

                </div>  


                <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_GET['utm_source']) != "" ? renderGETParams($_GET['utm_source']) : ""); ?>">
				<input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_GET['utm_medium']) != "" ? renderGETParams($_GET['utm_medium']) : ""); ?>">
				<input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_GET['utm_sub']) != "" ? renderGETParams($_GET['utm_sub']) : ""); ?>">
				<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_GET['utm_campaign']) != "" ? renderGETParams($_GET['utm_campaign']) : ""); ?>">
				<input type="hidden" id="utm_term" name="utm_term" value="<?php echo (isset($_GET['utm_term']) != "" ? renderGETParams($_GET['utm_term']) : ""); ?>">
				<input type="hidden" id="utm_content" name="utm_content" value="<?php echo (isset($_GET['utm_content']) != "" ? renderGETParams($_GET['utm_content']) : ""); ?>">
							
				<input type="hidden" id="adpos" name="adpos" value="<?php echo (isset($_GET['adpos']) != "" ? renderGETParams($_GET['adpos']) : ""); ?>">
				<input type="hidden" id="ad" name="ad" value="<?php  echo (isset($_GET['ad']) != "" ? renderGETParams($_GET['ad']) : ""); ?>">
							
				<input type="hidden" id="business_type" name="business_type" value="building-planner">
				<input type="hidden" id="project" name="project" value="">

                <div class="submitbtncontainer">
                    <input type="submit" value="Submit" name="submit">
                </div>
            </form>

            <form action="" id="otpFrom2" class="feedbackForm otp-form_2" style="display: none;" novalidate="novalidate">

                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                        <input type="text" class="form-control only_numeric otp phone" id="otp2" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                        <label id="otp2-error" class="error" for="otp2"></label>
                        <span id="otp2-msg"></span>
                    </div>
                </div> 

                <div class="group">
                    <div class="form-group" style="margin-bottom: 0; text-align: right;  cursor:pointer;">
                        <span href="javascript:void(0);" data-form-type="2" class="form_2-resend-otp resend-otp">Resend OTP</span>
                    </div>
                </div> 

                <div class="submitbtncontainer wrap">
                    <input type="submit" class="button form_2-otp-submit-btn" value="Submit OTP" name="submit">
                </div>

                <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

            </form>

        </div>

        <!-- //banner --> 
    </section>

    <!-- About -->

    <div class="clearfix"></div>

    <section class="aboutSection">

        <div class="col-md-12 paddingZero">

            <div class="col-md-6 paddingZero aboutParaContent">

                <h4 class="marginBottom10"> About Us</h4>

                <h2 class="marginBottom10 smakColor"> SMAK IS ONE OF THE BEST <br> BUILDING PLANNER </h2>

                <p class="aboutPara marginBottom10">
                    SMAK possesses a team of skilled Home & Architectural designers that offers the 
                    clients customized House Plan Designs- Floor plan, 3D & Structural designs. 
                    And that’s what makes us one of the best in the industry offering a
                    superior quality service! 
                </p>

                <p>

                    <ul class="aboutListItems">
                        <li>Designs of International qualities </li>
                        <li>Zero hidden costs & easy on your pocket </li>
                        <li>Personalized designs to fit your needs </li>
                        <li>24/7 Customer Support to guide you throughout </li>
                    </ul>

                </p>
                
            </div>

            <div class="col-md-6 paddingZero leftAboutImage">

                <div class="aboutImageSec">
                    <a href="<?php echo SITE_URL?>assets/building-planner/images/about.jpg"><img src="<?php echo SITE_URL?>assets/building-planner/images/about.jpg" alt="" style="width:100%;" class="img-responsive"></a>
                </div>
                
            </div>

        </div>

    </section>

    <div class="clearfix"></div>

    <section class="nearBy">

        <section class="nearByUpper">

            <h2 class="smakColor text-center"> OUR SERVICES </h2>

            <div class="container">

                <div class="col-md-12 mtop30">

                    <p class="text-center">
                        3D Floor plans, Structural drawings, 
                        Architectural drawings and everything that your dream home demands- We are ever-ready.
                        <br>
                        Order all the services you require online & get them delivered on time!  
                    </p>

                </div>

            </div>

        </section>

        <div class="clearfix"></div>

        <section class="nearByLower">

            <div class="col-md-12 paddingZero nearByLowerImgSec">

                <div class="col-md-4 paddingZero interior_service leftMost">
                    <img src="<?php echo SITE_URL?>assets/building-planner/images/home-plan.jpg" alt="" class="img-responsive">
                    <h3 class="interior_service_2" style="visibility: visible;">Home Plan</h3>
                </div>

                <div class="col-md-8 paddingZero">

                    <div class="col-md-6 paddingZero interior_service RightSecleft">
                        <img src="<?php echo SITE_URL?>assets/building-planner/images/floor-plan.jpg" alt="" class="img-responsive">
                        <h3 class="interior_service_2" style="visibility: visible;">Floor Plan</h3>
                    </div>

                    <div class="col-md-6 paddingZero interior_service RightSecRight">
                        <img src="<?php echo SITE_URL?>assets/building-planner/images/architectural.jpg" alt="" class="img-responsive">
                        <h3 class="interior_service_2" style="visibility: visible;">Architectural</h3>
                    </div>

                    <div class="col-md-6 paddingZero interior_service RightSecleft">
                        <img src="<?php echo SITE_URL?>assets/building-planner/images/3d-elevation.jpg" alt="" class="img-responsive">
                        <h3 class="interior_service_2" style="visibility: visible;">3D Elevation</h3>
                    </div>

                    <div class="col-md-6 paddingZero interior_service RightSecRight">
                        <img src="<?php echo SITE_URL?>assets/building-planner/images/3d-floor-plan.jpg" alt="" class="img-responsive">
                        <h3 class="interior_service_2" style="visibility: visible;">3D Floor</h3>
                    </div>

                </div>

            </div>

            <div class="col-md-12 paddingZero nearByLowerBottomImgSec">

                <div class="col-md-6 paddingZero interior_service leftImageSec">
                    <img src="<?php echo SITE_URL?>assets/building-planner/images/3d-walkthrough.jpg" alt="" class="img-responsive">
                    <h3 class="interior_service_2" style="visibility: visible;">3D Walking</h3>
                </div>

                <div class="col-md-6 paddingZero interior_service rightImageSec">
                    <img src="<?php echo SITE_URL?>assets/building-planner/images/plan-sanction.jpg" alt="" class="img-responsive">
                    <h3 class="interior_service_2" style="visibility: visible;">Plan Section</h3>
                </div>

            </div>

        </section>
        
    </section>

    <div class="clearfix"></div>

    <footer>
        <div class="container "> 
            <p class="">
               
                <span class="copyrightSec"> © 2020 Smak. All Rights Reserved.</span>

                <span class="footerLinks">
                    <a href="https://www.smakconcepts.com/terms_condition" target="_blank"> Terms & Conditions </a>
                    <a href="https://www.smakconcepts.com/privacy" target="_blank"> Privacy Policy </a>
                </span>
            </p>
        </div>
    </footer> 
    
        <!-- contact form start -->
    <div class="floating-form" id="contact_form">
       <div class="contact-opener">Enquire Now</div>
            <form role="form" id="feedbackForm1" class="feedbackForm form_1" method="POST">
            <h4> Fill the form! We'll call you back </h4>

                <div class="group">
                    <div class="form-group">
                    <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" data-attr="Please enter correct name">
                    </div>                    
                </div>  
                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                         <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                    </div>

                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                         <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="CountryCode">
                        <input type="text" class="form-control only_numeric phone" id="phone" name="phone" pattern="\d*"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="CountryCode_err2"> </span>
                    </div>
                    
                </div>

                <div class="group">
                    <div class="form-group">
                        <input type="text" class="form-control city" id="city" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                    </div>
                </div>  

                <input type="hidden" id="utm_source3" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_sr"); ?>">
                <input type="hidden" id="utm_medium3" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "d_sr"); ?>">
                <input type="hidden" id="utm_sub3" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : "d_sr"); ?>">
                <input type="hidden" id="utm_campaign3" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "d_sr"); ?>">
                <div class="submitbtncontainer">
                    <input type="submit" value="Submit" name="submit" class="button pulse">
                </div>

            </form>

            <form action="" id="otpFrom1" class="feedbackForm otp-form_1" style="display: none;" novalidate="novalidate">

                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                        <input type="text" class="form-control only_numeric otp phone" id="otp1" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                        <label id="otp1-error" class="error" for="otp1"></label>
                        <span id="otp1-msg"></span>
                    </div>
                </div> 

                <div class="group">
                    <div class="form-group" style="margin-bottom: 0; text-align: right;  cursor:pointer;">
                        <span href="javascript:void(0);" data-form-type="1" class="form_1-resend-otp resend-otp">Resend OTP</span>
                    </div>
                </div> 

                <div class="submitbtncontainer wrap">
                    <input type="submit" class="button form_1-otp-submit-btn" value="Submit OTP" name="submit">
                </div>

                <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

            </form>

            <div>


    <input type="hidden" name="siteurl" id="siteurl" value="https://www.smakconcepts.com/" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo SITE_URL?>assets/building-planner/js/vendor.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>     
    <script src="<?php echo SITE_URL?>assets/building-planner/js/main.js"></script>  
    <script src="<?php echo SITE_URL?>assets/building-planner/js/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <script>
        window.addEventListener( "pageshow", function ( event ) {
            var historyTraversal = event.persisted || ( typeof window.performance != "undefined" && window.performance.navigation.type === 2 );
            if ( historyTraversal ) {
                // Handle page restore.
                window.location.reload();
            }
        });
        $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
        );
        jQuery.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "letters only");

        $(".helpBtn").click(function(){
            $('html, body').animate({
                scrollTop: $("#myCarousel").offset().top
            }, 2000);
        });

    </script>

    <script>
        $('.carousel-inner').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery:{
                enabled:true
            }
        });

        $('.aboutImageSec').magnificPopup({
            delegate: 'a',
            type: 'image'
        });
    </script>

    <?php $this->load->view ("footer_scripts");?>


    <script>
        $(".contactArea").click( function(){
            $("#name3").focus();
        })
    </script>

   </body>

</html>

<?php 

function renderGETParams( $field = '' ) {

    if ( isset($field) ) {

        $value = str_replace( '"', '',  $field);
        $value = str_replace( "'", "",  $value );

        return $value;
    }else {
        return "";
    }
}

?>