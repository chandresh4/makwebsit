<!DOCTYPE html>

<html lang="en">
    <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" href="<?php echo SITE_URL?>assets/vijay_nagar/images/favicon.png" />
      <title>Smak Concepts | Vijayanagar</title>
    
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/vijay_nagar/style/vendor.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">          
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/vijay_nagar/style/main.css">

      <style>
        .error{
            color:#ff6262;
        }
      </style>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167529661-3"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-167529661-3');
        </script>

        <!-- Global site tag (gtag.js) - Google Ads: 609045498 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-609045498"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-609045498');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '3897310283613857');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=3897310283613857&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

   </head> 
   <body> 
    <section>
        
        <header>

            <div class="logoSec">
                <a href="<?php echo SITE_URL."vijayanagar"; ?>"><img src="<?php echo SITE_URL?>assets/vijay_nagar/images/white_logo.svg" alt="" class="handPointer"></a>
            </div>

            <div class="contactSec webView">
                <a href="tel:+91 9886644767">Call Us: +91 9886644767</a>
            </div>

        </header>

        <!-- banner -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">

                        <div class="webView">
                            <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/banner.png"  style="width:100%;" class="img-responsive" >
                        </div>

                        <div class="mobView">
                            <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/banner_mobile.jpg"  style="width:100%;" class="img-responsive">
                        </div>

                    <div class="staticForm webView">

                        <form role="form" id="feedbackForm3" class="feedbackForm form_3" method="POST">
                            <h4>Fill the form! We'll call you back</h4>
                            <div class="group">
                                <div class="form-group">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <input type="text" class="form-control" id="name3" name="name" placeholder="Name" data-attr="Please enter correct name">
                                </div>


                                <div class="form-group">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                                </div>

                            </div>  

                            <div class="group">

                                <div class="form-group">
                                    <i class="fa fa-phone" aria-hidden="true"></i>  
                                    <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="countryCode3">
                                    <input type="text" class="form-control only_numeric phone" id="phone3" name="phone"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                                </div>

                            </div>  
                            
                            <div class="group">

                                <div class="form-group">
                                    <input type="text" class="form-control city" id="city3" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                                </div>
            
                            </div>  

                            <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_GET['utm_source']) != "" ? renderGETParams($_GET['utm_source']) : ""); ?>">
                        <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_GET['utm_medium']) != "" ? renderGETParams($_GET['utm_medium']) : ""); ?>">
                        <input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_GET['utm_sub']) != "" ? renderGETParams($_GET['utm_sub']) : ""); ?>">
                        <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_GET['utm_campaign']) != "" ? renderGETParams($_GET['utm_campaign']) : ""); ?>">
                        <input type="hidden" id="utm_term" name="utm_term" value="<?php echo (isset($_GET['utm_term']) != "" ? renderGETParams($_GET['utm_term']) : ""); ?>">
                        <input type="hidden" id="utm_content" name="utm_content" value="<?php echo (isset($_GET['utm_content']) != "" ? renderGETParams($_GET['utm_content']) : ""); ?>">
                                    
                        <input type="hidden" id="adpos" name="adpos" value="<?php echo (isset($_GET['adpos']) != "" ? renderGETParams($_GET['adpos']) : ""); ?>">
                        <input type="hidden" id="ad" name="ad" value="<?php echo (isset($_GET['ad']) != "" ? renderGETParams($_GET['ad']) : ""); ?>">
                            
							<input type="hidden" id="business_type" name="business_type" value="constructions">
							<input type="hidden" id="project" name="project" value="Vijayanagar">

                            <div class="submitbtncontainer wrap">
                                <input type="submit" class="button" value="Submit" name="submit">
                            </div>
                        </form>
						<form action="" id="otpFrom3" class="feedbackForm otp-form_3" style="display: none;" novalidate="novalidate">

                            <div class="group">
                                <div class="form-group">
                                    <i class="fa fa-phone" aria-hidden="true"></i>  
                                    <input type="text" class="form-control otp only_numeric phone" id="otp3" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                                    <label id="otp3-error" class="error" for="otp3"></label>
    		                        <span id="otp3-msg"></span>
                                </div>
                            </div> 

                            <div class="group">
                                <div class="form-group" style="margin-bottom: 0; text-align: right;  cursor:pointer;">
                                    <span data-form-type="3"  class="form_3-resend-otp resend-otp">Resend OTP</span>
                                </div>
                            </div> 
                            
                            <div class="submitbtncontainer wrap">
                                <input type="submit" class="button form_3-otp-submit-btn" value="Submit OTP" name="submit">
                            </div>

                            <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

                        </form>
                    </div>

                    <div class="bannerFooter">
                        <div class="container">
                            <p> Vijayanagar, Mysore - 2 BHK House </p> 
                        </div>
                    </div>
                
                </div>           
            </div>
            <!-- Left and right controls --> 
        </div>

        <div class="mobileFormDiv"> 
            <form role="form" id="feedbackForm2" class="feedbackForm form_2 mobView mobileForm" method="POST">
                <h4>Fill the form! We'll call you back </h4>
                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control" id="name2" name="name" placeholder="Name" data-attr="Please enter correct name">
                    </div>


                    <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                    </div>

                </div>  

                <div class="group">

                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                        <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="countryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone2" name="phone"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                    </div>

                </div>   

                <div class="group">

                    <div class="form-group">
                        <input type="text" class="form-control city" id="city2" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                    </div>

                </div>  

                <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_GET['utm_source']) != "" ? renderGETParams($_GET['utm_source']) : ""); ?>">
				<input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_GET['utm_medium']) != "" ? renderGETParams($_GET['utm_medium']) : ""); ?>">
				<input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_GET['utm_sub']) != "" ? renderGETParams($_GET['utm_sub']) : ""); ?>">
				<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_GET['utm_campaign']) != "" ? renderGETParams($_GET['utm_campaign']) : ""); ?>">
				<input type="hidden" id="utm_term" name="utm_term" value="<?php echo (isset($_GET['utm_term']) != "" ? renderGETParams($_GET['utm_term']) : ""); ?>">
				<input type="hidden" id="utm_content" name="utm_content" value="<?php echo (isset($_GET['utm_content']) != "" ? renderGETParams($_GET['utm_content']) : ""); ?>">
							
				<input type="hidden" id="adpos" name="adpos" value="<?php echo (isset($_GET['adpos']) != "" ? renderGETParams($_GET['adpos']) : ""); ?>">
				<input type="hidden" id="ad" name="ad" value="<?php echo (isset($_GET['ad']) != "" ? renderGETParams($_GET['ad']) : ""); ?>">
							
				<input type="hidden" id="business_type" name="business_type" value="constructions">
				<input type="hidden" id="project" name="project" value="vijayanager">

                <div class="submitbtncontainer">
                    <input type="submit" value="Submit" name="submit">
                </div>
            </form>
			<form action="" id="otpFrom2" class="feedbackForm otp-form_2" style="display: none;" novalidate="novalidate">

				<div class="group">
					<div class="form-group">
						<i class="fa fa-phone" aria-hidden="true"></i>  
						<input type="text" class="form-control otp only_numeric phone" id="otp2" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                        <label id="otp2-error" class="error" for="otp2"></label>
    		            <span id="otp2-msg"></span>
                    </div>
				</div> 

				<div class="group">
					<div class="form-group" style="margin-bottom: 0; text-align: right;  cursor:pointer;">
						<span href="javascript:void(0);" data-form-type="2"  class="form_2-resend-otp resend-otp">Resend OTP</span>
					</div>
				</div> 
				
				<div class="submitbtncontainer wrap">
					<input type="submit" class="button form_2-otp-submit-btn" value="Submit OTP" name="submit">
				</div>

				<p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

			</form>
        </div>

        <!-- //banner --> 
    </section>

    <!-- About -->

    <div class="clearfix"></div>

    <section class="videoSection">

        <div class="container">

            <div class="col-md-6 mtop30">

                <div id="myCarouselItems" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarouselItems" data-slide-to="0" class=""></li>
                        <li data-target="#myCarouselItems" data-slide-to="1" class=""></li>
                        <li data-target="#myCarouselItems" data-slide-to="2" class=""></li>
                        <li data-target="#myCarouselItems" data-slide-to="3" class="active"></li>
                        <li data-target="#myCarouselItems" data-slide-to="4" class=""></li>
                        <li data-target="#myCarouselItems" data-slide-to="5" class=""></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        
                        <div class="item">
                            <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/living_area_big.png">
                                <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/living_area.png" alt="" style="width:100%;" class="img-responsive">
                            </a>
                            
                        </div>

                        <div class="item">
                            <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/kitchen_big.png"> 
                                <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/kitchen.png" alt="" style="width:100%;" class="img-responsive">             
                            </a>
                        </div>

                        <div class="item active">
                            <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/bedroom_1_big.png">
                                <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/bedroom_1.png" alt="" style="width:100%;" class="img-responsive">
                            </a>
                        </div>

                        <div class="item">
                            <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/bedroom_2_big.png">
                                <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/bedroom_2.png" alt="" style="width:100%;" class="img-responsive">
                            </a>
                        </div>

                        <div class="item">
                            <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/common_big.png">                            
                                <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/common.png" alt="" style="width:100%;" class="img-responsive">
                            </a>
                        </div>

                        <div class="item">
                            <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/common_2_big.png">
                                <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/common_2.png" alt="" style="width:100%;" class="img-responsive">
                            </a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-5 mtop30">
                <video id="moulimNagar" poster="<?php echo SITE_URL?>assets/vijay_nagar/images/video_banner.jpg" controls="controls" preload="true" loop="loop" muted="muted" volume="0">
                    <source src="https://adcancdn.s3.ap-southeast-1.amazonaws.com/smak-concepts/video/Maulim%20Nagar-Smak.mp4" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>

            </div>
            
    </section>


    <div class="clearfix"></div>

    <section class="aboutSection">

        <div class="container-fluid">

        <!-- <h2 class="smakColor text-center"> Overview </h2> -->

            <div class="col-md-12 paddingZero">

                <div class="col-md-6 paddingZero leftAboutImage">

                    <div class="aboutImageSec">
                        <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/big_house.png"><img src="<?php echo SITE_URL?>assets/vijay_nagar/images/about_us.png" alt="" style="width:100%;" class="img-responsive aboutImgHeight"></a>
                    </div>
                    
                </div>

                <div class="col-md-6 paddingZero aboutParaContent">

                    <h4 class="marginBottom10"> About </h4>

                    <h2 class="marginBottom10"> Vijayanagar, Mysore</h2>

                    <p class="aboutPara marginBottom10">
                        This is an upcoming residential MUDA project by Smak concepts, 
                        in Vijayanagar 4th Stage 2nd Phase Mysore. This 2 BHK property is 1200 sqft in area & the layout
                        is intelligently planned and is in proximity to supermarkets,
                        schools, hospitals and parks.
                    </p>

                    <br>

                    <p>

                        <ul class="aboutListItems"> <b class="ulHeading">In Proximity</b>
                            <li>0.5 km from National Public School ( NPS)</li>
                            <li>1.4 km from Amritakripa Hospital Bhogadi Road</li>
                            <li>2.6 km from Hunsur Ring road junction</li>
                            <li>Less than 7 km to Railway Station</li>
                        </ul>

                    </p>

                    <br>
                    <br>

                    <div class="pricingSec">

                        <p class="font14">Project pricing </p>

                        <hr>

                        <p> <span class="leftPriceSec"> Area &nbsp; &nbsp; : &nbsp; </span> <span class="rightPriceSec"> 1200 sq.ft </span> </p>

                        <p> <span class="leftPriceSec"> Price &nbsp;&nbsp; : &nbsp;&nbsp; </span> <span class="rightPriceSec"> 80 L onwards </span> </p>

                    </div>
                    
                </div>

            </div>

        </div>

    </section>

    <div class="clearfix"></div>

    <section class="nearBy">

        <div class="container">

            <h2 class="smakColor text-center"> Amenities in proximity </h2>

            <div class="col-md-12 mtop30">

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/hospitals.png" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                        Hospitals within  <br> 1.5 km distance
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/schools.png" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                        Schools within <br>  0.5 km distance 
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/college.png" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                        Colleges within  <br> 2.8 km distance
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec centerBlock">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/supermarkets.png" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                        Supermarkets within <br> 0.6 km distance
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/parks.png" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                        Park with  <br> 0.6km distance
                        </p>
                    </div>

                </div>

            </div>

        </div>
        
    </section>

    <div class="clearfix"></div>

    <section class="locationMap">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3897.995002318264!2d76.57998751481524!3d12.31612299128829!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTLCsDE4JzU4LjAiTiA3NsKwMzQnNTUuOCJF!5e0!3m2!1sen!2sin!4v1587987234852!5m2!1sen!2sin" width="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </section>

    <footer>
        <div class="container "> 
            <p class="">
               
                <span class="copyrightSec"> © 2020 Smak. All Rights Reserved.</span>

                <span class="footerLinks">
                    <a href="https://www.smakconcepts.com/terms_condition" target="_blank"> Terms & Conditions </a>
                    <a href="https://www.smakconcepts.com/privacy" target="_blank"> Privacy Policy </a>
                </span>
            </p>
        </div>
    </footer> 
    
        <!-- contact form start -->
    <div class="floating-form" id="contact_form">
       <div class="contact-opener">Enquire Now</div>
            <form role="form" id="feedbackForm1" class="feedbackForm form_1" method="POST">
            <h4> Fill the form! We'll call you back </h4>

                <div class="group">
                    <div class="form-group">
                    <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" data-attr="Please enter correct name">
                    </div>                    
                </div>  
                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                         <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                    </div>

                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                         <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="CountryCode">
                        <input type="text" class="form-control only_numeric phone" id="phone" name="phone" pattern="\d*"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="CountryCode_err2"> </span>
                    </div>
                    
                </div>

                <div class="group">
                    <div class="form-group">
                        <input type="text" class="form-control city" id="city" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                    </div>
                </div>  

                <input type="hidden" id="utm_source3" name="utm_source" value="<?php echo (isset($_GET['utm_source']) != "" ? $_GET['utm_source'] : ""); ?>">
                <input type="hidden" id="utm_medium3" name="utm_medium" value="<?php echo (isset($_GET['utm_medium']) != "" ? $_GET['utm_medium'] : ""); ?>">
                <input type="hidden" id="utm_sub3" name="utm_sub" value="<?php echo (isset($_GET['utm_sub']) != "" ? $_GET['utm_sub'] : ""); ?>">
                <input type="hidden" id="utm_campaign3" name="utm_campaign" value="<?php echo (isset($_GET['utm_campaign']) != "" ? $_GET['utm_campaign'] : ""); ?>">
                <div class="submitbtncontainer">
                    <input type="submit" value="Submit" name="submit" class="button pulse">
                </div>

            </form>

            <form action="" id="otpFrom1" class="feedbackForm otp-form_1" style="display: none;" novalidate="novalidate">

                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                        <input type="text" class="form-control otp only_numeric phone" id="otp2" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                        <label id="otp1-error" class="error" for="otp1"></label>
    		            <span id="otp1-msg"></span>
                    </div>
                </div> 

                <div class="group">
                    <div class="form-group" style="margin-bottom: 0; text-align: right;  cursor:pointer;">
                        <span href="javascript:void(0);" data-form-type="1" class="form_1-resend-otp resend-otp">Resend OTP</span>
                    </div>
                </div> 

                <div class="submitbtncontainer wrap">
                    <input type="submit" class="button form_1-otp-submit-btn" value="Submit OTP" name="submit">
                </div>

                <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

            </form>

            <div>


    <input type="hidden" name="siteurl" id="siteurl" value="https://www.smakconcepts.com/" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo SITE_URL?>assets/vijay_nagar/js/vendor.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>     
    <script src="<?php echo SITE_URL?>assets/vijay_nagar/js/main.js"></script>  
    <script src="<?php echo SITE_URL?>assets/vijay_nagar/js/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <script>
        window.addEventListener( "pageshow", function ( event ) {
            var historyTraversal = event.persisted || ( typeof window.performance != "undefined" && window.performance.navigation.type === 2 );
            if ( historyTraversal ) {
                // Handle page restore.
                window.location.reload();
            }
        });
        $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
        );
        jQuery.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "letters only");

        $(".helpBtn").click(function(){
            $('html, body').animate({
                scrollTop: $("#myCarousel").offset().top
            }, 2000);
        });

    </script>
	<?php $this->load->view ("footer_scripts");?>
    <script>
        $('.carousel-inner').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery:{
                enabled:true
            }
        });

        $('.aboutImageSec').magnificPopup({
            delegate: 'a',
            type: 'image'
        });
    </script>

    <script>

        $window.scroll(function ()  {
            if($window.scrollTop() > 620){
                setTimeout(function(){
                    document.getElementById("moulimNagar").play();
                },5000);
            }
        })
        
    </script>

   </body>

</html>


<?php 

function renderGETParams( $field = '' ) {

    if ( isset($field) ) {

        $value = str_replace( '"', '',  $field);
        $value = str_replace( "'", "",  $value );

        return $value;
    }else {
        return "";
    }
}

?>