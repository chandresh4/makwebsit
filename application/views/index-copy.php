<!DOCTYPE html>
<html lang="en"> 
    <?php 
        $this->load->view('head');
    ?>
<body>
    <?php 
        $this->load->view('header');
    ?>
    <div class="clearfix"></div>

    <!-- Banner -->

    <section id="banner"> 
        <div class="banner"> 
            <div class="banner-inner">
                <img src="./images/banner.jpg" alt="Banner" class="banner_img  webView">
                <img src="./images/banner_mobile.jpg" alt="Banner" class="banner_img img-responsive mobView">
                <div class="container">
                    <div class="banner-caption">                
                        <div class="col-md-12 col-xs-12">
                            <h1>A NEW WAY TO BUILD </h1>
                            <p>Build a home that's uniquely yours with our <br>
                                Branded Turnkey construction packages.</p>
                            <!-- <button class="btn knowAboutUs">Know More</button> -->
                            <div class="col-md-12 ">
                                <ul class="highLights">
                                    <li id="Jumper1" class="highLight bg_blue">Construction</li>
                                    <li id="Jumper2" class="highLight">Interiors</li>
                                    <li id="Jumper3" class="highLight">Projects</li>
                                </ul>
                            </div>
                        </div>  
                    </div>
                </div>    
            </div> 
        </div>  
    </section>

    <div class="clearfix"></div>

    <!-- Features  Section -->

    <section id="features">
        <div class="col-md-12">
            <div class="col-md-4" id="design">
                <div class="box">
                    <img src="./images/architecture design.png" alt="">
                    <h3 class="clr_primary">Architecture Design</h3>
                    <p> 
                        We offer a broad array of elegant commercial designs with a unique, holistic approach.
                    </p>
                    <!-- <button class=".btn readMore">Read More <span class="arrow"><img src="./images/arrow.png" alt=""></span></button> -->
                </div>
            </div>

            <div class="col-md-4" id="construction">
                <div class="box">
                    <img src="./images/building construction.png" alt="">
                    <h3 class="clr_primary">Building Construction</h3>
                    <p>
                        Build effortlessly!
                        We build your dreams along with your buildings!
                        Explore a wide range of packages we provide…
                    </p>
                    <!-- <button class=".btn readMore">Read More <span class="arrow"><img src="./images/arrow.png" alt=""></span></button> -->
                </div>
            </div>

            <div class="col-md-4" id="renovation">
                <div class="box">
                    <img src="./images/building renovation.png" alt="">
                    <h3 class="clr_primary">Building Renovation</h3>
                    <p>
                        Our skilled professionals renovate your building not just to restore it, but to let it tell a story! 
                    </p>
                    <!-- <button class=".btn readMore">Read More <span class="arrow"><img src="./images/arrow.png" alt=""></span></button> -->
                </div>
            </div>

            
        </div>
    </section>

    <div class="clearfix"></div>

    <!-- Hire US Section -->

    <section id="hireUs">
        <div class="container">
            <h1 class="sec_heading">HIRE OUR EXPERT CONSTRUCTION PROFESSIONALS</h1>

            <div class="experties col-md-12">
                
                <div class="expert col-md-2 col-xs-6 goToArchitects">
                    <img src="./images/architects.png" alt="" class="invert">
                    <p>Architects </p>
                </div>

                <div class="expert col-md-2 col-xs-6 goToFlooring">
                    <img src="./images/Flooring_Experts.png" alt="" class="invert">
                    <p>Flooring  Experts</p>
                </div>

                <div class="expert col-md-2 col-xs-6 goToCarpentry">
                    <img src="./images/carpentry.png" alt="" class="invert">
                    <p>Carpentry/ Woodworking</p>
                </div>

                <div class="expert col-md-2 col-xs-6 goToBathroomRenovation">
                    <img src="./images/Bathroom Renovation.png" alt="" class=" invert">
                    <p>Bathroom  Renovation</p>
                </div>

                <div class="expert col-md-2 col-xs-6 goToKitchen">
                    <img src="./images/kitchen.png" alt="" class="invert">                    
                    <p>Kitchen  Renovation</p>
                </div>

                <div class="expert col-md-2 col-xs-6 goToFabrication">
                    <img src="./images/Grillwork.png" alt="" class=" invert">
                    <p>Metal Fabrication/ 
                        Grillwork</p>
                </div>

                <!-- <div class="expert col-md-2 col-xs-6 goToCompound">
                    <img src="./images/Compound Wall.png" alt="" class="invert">
                    <p>Compound Wall 
                        Construction</p>
                </div> --> 
            </div>
        </div>
    </section><!--
    <section class="com-pro">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="complete-p">
                        <img src="img/IMG_0350.png" class="img-responsive">

                    </div>
                </div>
            </div>
        </div>
    </section>
-->

    <div class="clearfix"></div>

    <!-- On going Projects  -->

    <section id="onGoing">

        <div class="container">
            <h1 class="sec_heading">On going projects</h1>
          

            <div class="projectss">

                <div class="moulimNagarSection">

                    <div class="row">

                        <div class="col-md-6 videoSec">
                            <h3>Vijaya Nagar</h3>
                            <video id="moulimNagar" poster="<?php echo SITE_URL?>assets/vijay_nagar/images/video_banner.jpg" controls="controls" preload="true" loop="loop" muted="muted" volume="0">
                                <source src="https://adcancdn.s3.ap-southeast-1.amazonaws.com/smak-concepts/video/Maulim%20Nagar-Smak.mp4" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>

                        <div class="col-md-6 videoSec">
                            <h3>Smak Meadows</h3>
                            <video id="smakMeadows" poster="https://adcancdn.s3.ap-southeast-1.amazonaws.com/Anvit%20Concepts/thumbnail.jpg" controls="controls" preload="true" loop="loop" muted="muted" volume="0">
                                <source src="https://adcancdn.s3.ap-southeast-1.amazonaws.com/Anvit%20Concepts/anvitconcepts.mp4" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    
                    </div>
                    

                    <div class="col-md-12 sliderContainer">

                        <!-- <button class="btn btn-default redirectButton">Check it Out</button>

                        <div class="projectLocation">
                            <span> Moulim Nagar, Mysuru</span>
                        </div> -->

                        <div class=" col-md-12">

                            <h2 style="margin-left: 1em; font-weight: 600;">Vijaya Nagar</h2>

                            <div class="moulimSlider">

                                <div class="s-Slider col-md-12" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/living_area_big.png">
                                            <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/living_area.png" alt="" style="width:100%;" class="img-responsive">
                                        </a>
                                        
                                    </div>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/kitchen_big.png"> 
                                            <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/kitchen.png" alt="" style="width:100%;" class="img-responsive">             
                                        </a>
                                    </div>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/bedroom_1_big.png">
                                            <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/bedroom_1.png" alt="" style="width:100%;" class="img-responsive">
                                        </a>
                                    </div>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/bedroom_2_big.png">
                                            <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/bedroom_2.png" alt="" style="width:100%;" class="img-responsive">
                                        </a>
                                    </div>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/common_big.png">                            
                                            <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/common.png" alt="" style="width:100%;" class="img-responsive">
                                        </a>
                                    </div>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/vijay_nagar/images/common_2_big.png">
                                            <img src="<?php echo SITE_URL?>assets/vijay_nagar/images/common_2.png" alt="" style="width:100%;" class="img-responsive">
                                        </a>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 ">

                            <h2 style="margin-left: 1em; font-weight: 600;">Smak Meadows </h2>

                            <div class="moulimSlider">

                                <div class="s-Slider col-md-12" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows1.png">
                                            <img src="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows1.png" alt="" style="width:100%;" class="img-responsive">
                                        </a>
                                    </div>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows2.png"> 
                                            <img src="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows2.png" alt="" style="width:100%;" class="img-responsive">             
                                        </a>
                                    </div>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows3.png">
                                            <img src="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows3.png" alt="" style="width:100%;" class="img-responsive">
                                        </a>
                                    </div>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows4.png">
                                            <img src="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows4.png" alt="" style="width:100%;" class="img-responsive">
                                        </a>
                                    </div>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows1.png">                            
                                            <img src="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows1.png" alt="" style="width:100%;" class="img-responsive">
                                        </a>
                                    </div>

                                    <div class="slider_Item">
                                        <a href="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows3.png">
                                            <img src="<?php echo SITE_URL?>assets/anvit_meadows/images/meadows3.png" alt="" style="width:100%;" class="img-responsive">
                                        </a>
                                    </div>

                                </div>

                            </div>

                        </div>


                </div>
<!--
                <div class="col-md-12 projects">

                    <div class="col-md-4 col-sm-12 col-xs-12 project text_Centre">
                        <img src="./images/plot_2.png" alt="project name" class="img-responsive">
                        <h3 class="bg_blue">1200 sq.ft MUDA</h3>
                        <p class="location">Project Location</p>
                        <p>Moulim Nagar</p>

                    </div>

                    <div class="col-md-4 col-sm-12 col-xs-12 project text_Centre">
                        <img src="./images/resident_4.png" alt="project name" class="img-responsive">
                        <h3 class="bg_blue">Commercial & Residential</h3>
                        <p>( 3 shops , 3 houses, G+3 )</p>
                        <p class="location">Project Location</p>
                        <p>Shakti Nagar</p>

                    </div>

                    <div class="col-md-4 col-sm-12 col-xs-12 project text_Centre">
                        <img src="./images/plot_3.png" alt="project name" class="img-responsive">
                        <h3 class="bg_blue">1200 sq.ft MUDA</h3>
                        <p class="location">Project Location</p>
                        <p>Police Layout</p>

                    </div>

                    <!-- <div class="col-md-4 col-sm-12 col-xs-12 project text_Centre">
                        <img src="./images/plot_1.png" alt="project name" class="img-responsive">
                        <h3 class="bg_blue">1200 sq.ft MUDA</h3>
                        <p class="location">Project Location</p>
                        <p>Ring Road, Moulim Nagar</p>

                    </div> -->
                    
                    <!-- 
                    <div class="col-md-4 col-sm-12 col-xs-12 project text_Centre">
                        <img src="./images/land_1.jpg" alt="project name" class="img-responsive">
                        <h3 class="bg_blue">Project Name</h3>
                        <p class="location">Project Location</p>
                        <p>Apartments, 3rd Phase, J. P. Nagar, Bengaluru,
                            Karnataka 560078</p>

                    </div>

                    <div class="col-md-4 col-sm-12 col-xs-12 project text_Centre">
                        <img src="./images/land_1.jpg" alt="project name" class="img-responsive">
                        <h3 class="bg_blue">Project Name</h3>
                        <p class="location">Project Location</p>
                        <p>Apartments, 3rd Phase, J. P. Nagar, Bengaluru,
                            Karnataka 560078</p>

                    </div> -->

                </div>
            </div>
            <div class="clearfix"></div>
<!--
            <div class="hireExperts">
                <h1 class="sec_heading">HIRE OUR EXPERT</h1>  
            </div>
        -->
        </div>

    </section>

<!---
    <section id="hireExperts_slider">
        <div class="container">

            <div class="hireExperts_slider">

                <div class="s-Slider col-md-12" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>
                    
                    <div class="col-md-4 slider_Item">

                        <img src="./images/architecture.jpg" alt="" class="img-responsive">
                        <h3>Architects</h3>
                        <p>We customize the designs to adapt to your necessity. So choose your package for a happy home!</p>
                        <button class="btn btn-knowmore goToArchitects">Know More</button>

                    </div>

                    <div class="col-md-4 slider_Item">

                        <img src="./images/flooring.jpg" alt="" class="img-responsive">
                        <h3>Flooring Experts</h3>
                        <p>We train & certify our experts to make sure the best quality work is delivered to you</p>
                        <button class="btn btn-knowmore goToFlooring">Know More</button>

                    </div>

                    <div class="col-md-4 slider_Item">

                        <img src="./images/carpentry_works.jpg" alt="" class="img-responsive">
                        <h3>Carpentry</h3>
                        <p>Our skilled carpenters are aware of the latest trends, & will satisfy your specifications.</p>
                        <button class="btn btn-knowmore goToCarpentry">Know More</button>


                    </div>

                    <div class="col-md-4 slider_Item">

                        <img src="./images/bathroom_des.jpg" alt="" class="img-responsive">
                        <h3>Bathroom  Renovation</h3>
                        <p>
                            A team of skilled Civil Engineers, Interior Designers, Plumbing & Electrical professionals for the top quality renovation. 
                        </p>
                        <button class="btn btn-knowmore goToBathroomRenovation">Know More</button>


                    </div>

                    <div class="col-md-4 slider_Item">

                        <img src="./images/kitchen_renovation_works.jpg" alt="" class="img-responsive">
                        <h3>Kitchen Renovation</h3>
                        <p>Upgrade your kitchen with the help of our certified team while fitting it into your budget. </p>
                        <button class="btn btn-knowmore goToKitchen">Know More</button>

                    </div>

                    <div class="col-md-4 slider_Item">

                        <img src="./images/metal_fabrication_works.jpg" alt="" class="img-responsive">
                        <h3>Metal Fabrication</h3>
                        <p>Fabricate your dream home according to your specifications with the help of our qualified fabricators.</p>
                        <button class="btn btn-knowmore goToFabrication">Know More</button>

                    </div>
                -->

                    <!-- <div class="col-md-4 slider_Item">

                        <img src="./images/compound_wall.jpg" alt="" class="img-responsive">
                        <h3>Compound Wall </h3>
                        <p>Whatever type, we are ready with our professionals. Just mention what is that you need</p>
                        <button class="btn btn-knowmore goToCompound">Know More</button>

                    </div> -->
                    
                   <!--- 
                </div>

            </div>
        </div>
    </section>
    --->
   

    <section class="sliderTopMinusMargin main-futu" style="margin-top:150px">

        <div class="container">

            <h1 class="sec_heading  ">On Futuristic projects</h1>
            
    
            <div class="singleImg_slider col-md-12">
                <div class="single-item">
                    <div class="slider_Item"><img src="./images/project_1.png" alt="" class="img-responsive"></div>
                    <div class="slider_Item"><img src="./images/project_2.png" alt="" class="img-responsive"></div>
                    <div class="slider_Item"><img src="./images/project_3.png" alt="" class="img-responsive"></div>
                    <div class="slider_Item"><img src="./images/project_4.png" alt="" class="img-responsive"></div>
                    <div class="slider_Item"><img src="./images/project_5.png" alt="" class="img-responsive"></div>
                </div>
            </div>
        </div>

    </section>

    <div class="clearfix"></div>

    <!---
<style>
body {
  font-family: Verdana, sans-serif;
  margin: 0;
}

* {
  box-sizing: border-box;
}

.row > .column {
  padding: 0 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
}

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 1;
  padding-top: 100px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: transparent;
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
  top: 100px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
}

.caption-container {
  text-align: center;
  background-color: black;
  padding: 2px 16px;
  color: white;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
</style>
--->
<!--
<h2 style="text-align:center">Lightbox</h2>

<div class="container"> <div class="row">
  
      <div class="col-md-3">
    <img src="img_nature.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
  </div>
  <div class="col-md-3">
    <img src="img_snow.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
  </div>
  <div class="col-md-3">
    <img src="img_mountains.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
  </div>
  <div class="col-md-3">
    <img src="img_lights.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
  </div>
</div></div>
  

<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <div class="numbertext">1 / 4</div>
      <img src="img_nature_wide.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">2 / 4</div>
      <img src="img_snow_wide.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">3 / 4</div>
      <img src="img_mountains_wide.jpg" style="width:100%">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">4 / 4</div>
      <img src="img_lights_wide.jpg" style="width:100%">
    </div>
    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>


    <div class="column">
      <img class="demo cursor" src="img_nature_wide.jpg" style="width:100%" onclick="currentSlide(1)" alt="Nature and sunrise">
    </div>
    <div class="column">
      <img class="demo cursor" src="img_snow_wide.jpg" style="width:100%" onclick="currentSlide(2)" alt="Snow">
    </div>
    <div class="column">
      <img class="demo cursor" src="img_mountains_wide.jpg" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
    </div>
    <div class="column">
      <img class="demo cursor" src="img_lights_wide.jpg" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
    </div>
  </div>
</div>

<script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

--->

    <!-- Testimonial Section -->

    <section id="testimonials">
        <div class="container">

            <h1 class="sec_heading">TESTIMONIALS</h1>

            <div class="testimonial_slider">
                <div class="center text_Centre autoplay">
                    <div class="col-md-4">
                        <img src="./images/customer_2.jpg" alt="">
                        <p>Interiors they designed turned out to look just as I expected it to. I am satisfied with the work.</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_1.jpg" alt="">
                        <p>I got my kitchen upgraded and I have a modular kitchen I always dreamt of. Thank you SMAK</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_3.jpg" alt="">
                        <p>I cannot recognize my old house of almost 20+ years! Quality work at reasonable price</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_5.jpg" alt="">
                        <p>One of my friends recommended Smak for fabrication work & I don’t regret my decision</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_4.jpg" alt="">
                        <p>Plumbers who worked for my bathroom renovation were truly skilled! Hats off to them.</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_6.jpg" alt="">
                        <p>They not only delivered the work on time, they also helped me with selecting superior quality materials. </p>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <div class="clearfix"></div>


    <div id="form_top" class="col-md-2 contactbox webView form">
        <h2 class="legend" onclick="formChange()">Enquire Form</h2>
        <form class="mui-form home_enquiry" role="form" id="home_contact_form"  method="POST">                        
            <div class="mui-textfield mui-textfield--float-label">
                <input type="text" id="name" name="name">

                <label>Name</label>
            </div>
            <div class="mui-textfield mui-textfield--float-label">
                <input type="email" id="email" name="email">
                <label>Mail ID</label>
            </div>
            <div class="mui-textfield mui-textfield--float-label">
                <input type="text" class="only_numeric phone" id="mobile_no" name="mobile_no">
                <label>Contact No.</label>
            </div>


            <input type="hidden" id="message" name="message" value="i have interested in smakconcepts projects.Please call me ASAP.">
            <input type="hidden" id="utm_source" name="utm_source" value="enquire_now">
            <input type="hidden" id="utm_term" name="utm_term" value="enquire_now">
            <input type="hidden" id="utm_campaign" name="utm_campaign" value="enquire_now">
            <input type="hidden" id="utm_content" name="utm_content" value="enquire_now">
            <input type="hidden" id="utm_medium" name="utm_medium" value="enquire_now">
            <input type="hidden" id="business_type" name="business_type" value="constructions">
            <input type="hidden" id="project" name="project" value="">
            <input type="hidden" id="city" name="city" value="">
            <input type="hidden" id="ad" name="ad" value="enquire_now">
            <input type="hidden" id="adpos" name="adpos" value="enquire_now">

            
            <button type="submit" class="mui-btn mui-btn--raised">Submit</button>
        </form>
    </div>
    <?php 
        $this->load->view('footer');
        $this->load->view('script_links');
    ?>

    <script>

    $(document).ready(function(){

           $('.only_numeric').on('keypress', function(e) {
                var $this = $(this);
                var regex = new RegExp("^[0-9\b]+$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                // for 10 digit number only
                if ($this.val().length > 9) {
                    e.preventDefault();
                    return false;
                }

                if (e.charCode > 57 && e.charCode < 48) {
                    if ($this.val().length == 0) {
                        e.preventDefault();
                        return false;
                    } else {
                        return true;
                    }
                }

                if (regex.test(str)) {
                    return true;
                }

                e.preventDefault();

                return false;
            });

        $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
        );

        jQuery.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "letters only");


        $("#home_contact_form").validate({
            rules: {
              name : {
                required: true,
                minlength: 3,
                maxlength: 30,
                letterswithspace: /^[a-z][a-z\s]*$/
              },
              mobile_no: {
                required: true,
                regex:/^(0|91)?[6789]\d{9}$/,
                number: true,
              },
              email: {
                required: true,
                email: true
              }
            },
            messages : {
                name: {
                    required: "The Name field is required.",
                    minlength: "Name should be at least 3 characters",
                    maxlength: "Name should be Maximum 30 characters",
                    letterswithspace: "Only alphabetic characters allowed"
                },
                mobile_no: {
                    required: "The Mobile Number field is required.",
                    number: "Only digits allowed",
                    regex: "Please enter the valid Phone number",
                    minlength: "Minimum 10 digit number required",
                    maxlength: "Maximum 10 digit number can enter"
                },
                email: {
                    required: "The Email field is required.",
                    email: "email should be in the format: abc@domain.tld"
                }
            },
            
            onsubmit: true,
            submitHandler: function() {
                
                saveFormViaAjax( "home_enquiry" );
            }
        });

    });

    function saveFormViaAjax ( formType ) {

        var baseURL = "<?php echo base_url(); ?>";

        console.log(formType);

        var inputData = {
            name: $("."+formType).find("input[name='name']").val(),
            mobile_no: $("."+formType).find("input[name='mobile_no']").val(),
            email: $("."+formType).find("input[name='email']").val(),
            utm_source: $("#utm_source").val(),
            utm_term: $("#utm_term").val(),
            utm_campaign: $("#utm_campaign").val(),
            utm_content: $("#utm_content").val(),
            utm_medium: $("#utm_medium").val(),
            ad: $("#ad").val(),
            city:'',
            message:$("#message").val(),
            adpos: $("#adpos").val(),
            business_type: $("#business_type").val(),
            project: $("#project").val(),
        };
        
        $("."+formType).find("input[type='submit']").attr("disabled", "disabled");
        //get the input data
        $("#ajax_load").show();


        //submit
        $.ajax({
            type: 'POST',
            url: baseURL+"submit/submitEnquiryForm",
             data: inputData,
            success : function(response){ 
                console.log( response )
                var parsedData = JSON.parse(response);
                $("#ajax_load").hide();

                if(parsedData.status == "success"){
                   
                    setTimeout(function(){ 
                        location.reload();
                    }, 3000);

                   document.getElementById("home_contact_form").reset();
                    $.dialog({
                         title: 'Success!',
                        content: 'Request form submitted successfully!',
                    });                       
                }else if(parsedData.status == false && parsedData.message == 'failed'){
                    $.dialog({
                         title: 'Sorry!',
                        content: 'Something went wrong. Try again',
                    });
                }else if(parsedData.status == true && parsedData.message != ''){
                    $.dialog({
                         title: 'Thank You!',
                        content: 'Your request is already sent! we will get back to you soon',
                    });
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }


            } 
        });
   }
</script>
     <script>

        $(document).ready(function(){

            jumpBg()

            $('.s-Slider').slick({ 
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                    },
                    {
                    breakpoint: 700,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '20px',
                        autoplay:true,
                        autoplaySpeed: 1500,
                        slidesToShow: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: false,
                        centerPadding: '40px',
                        autoplay:true,
                        autoplaySpeed: 1500,
                        slidesToShow: 1
                    }
                    }
                ]
            });


            $('.single-item').slick({
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                    },
                    {
                    breakpoint: 700,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '20px',
                        autoplay:true,
                        autoplaySpeed: 3000,
                        slidesToShow: 1
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        dots:true,
                        arrows: false,
                        centerMode: false,
                        centerPadding: '40px',
                        autoplay:false,
                        autoplaySpeed: 3000,
                        slidesToShow: 1
                    }
                    }
                ]
            });

            $('.center ').slick({
                dots: true,
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 3,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                    }
                ]
            });

            var $window = $(window);

            if ($window.scrollTop() > 20) {          
                $(".navbar").addClass('nav_blue');
            }
        

            
        });

        var $window = $(window);

        $window.scroll(function () {
            if ($window.scrollTop() > 20) {          

            $(".navbar").addClass('nav_blue');

            }else{
                $(".navbar").removeClass('nav_blue');
            }

            if($window.scrollTop() > 1450){
                setTimeout(function(){
                    document.getElementById("moulimNagar").play();
                },5000);
                
            }


        });

</script>

<script>

    $('.moulimSlider').slick({
        autoplay:true,
        autoplaySpeed: 1500,
        arrows: false,
    });

    $('.moulimSlider').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery:{
            enabled:true
        }
    });
</script>
<div id="ajax_load" style="display:none;">
    <img src="<?php echo SITE_URL; ?>images/ajax-loader.gif" alt="">
</div>

<style>
#ajax_load {
    height: 100vh;
    left: 0!important;
    opacity: .6;
    position: fixed!important;
    top: 0!important;
    width: 100%;
    z-index: 10060;
    background: #fff;
}

#ajax_load img {
    height: 32px;
    left: 50%;
    margin-left: -16px;
    margin-top: -16px;
    position: absolute;
    top: 50%;
    width: 32px;
    z-index: 999;
}

</style>
</body>

</html>