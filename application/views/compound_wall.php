<!DOCTYPE html>
<html lang="en"> 
    <?php 
        $this->load->view('head');
    ?>
<body>
    <?php 
        $this->load->view('header');
    ?>
    <div class="clearfix"></div>

    <!-- Banner -->
    <section id="banner">   
        <div class="aboutus_banner">
            <img src="./images/wall_banner.jpg" alt="Banner" class="banner_img img-responsive webView">
            <img src="./images/wall_banner_mobile.jpg" alt="Banner" class="banner_img img-responsive mobView">           
            
            <div class=" container">
                <div class="banner-caption">
                    <div class="col-md-12 col-xs-12">
                        <h1>Compound Wall
                            Construction</h1>
                    </div>
                </div>
            </div> 
        </div> 
    </section>

    <div class="clearfix"></div>
    <!-- Description Section -->

    <section class="description_section">
        <div class="container">
            <div class="col-md-12">
                <div class="col-md-6 col-xs-12 col-sm-6 desc_img">
                    <img src="./images/wall_desc.jpg" alt="descrition image" class="img-responsive">

                </div>
                <div class="col-md-6 col-xs-12 col-sm-6 desc_text">
                    <p>
                        Whatever type, we are ready with our professionals. Secure your house while making it looks classy.
                        Block work compound wall, precast compound wall, or barbed wire fencing, we possess highly experienced team.
                        Just mention what is that you need,
                        and we will have your protected.
                    </p>

                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

    <!-- Services Offered Section -->

    <section id="services">
        <div class="container">
            <h1 class="sec_heading">SERVICES OFFERED</h1>
            <p class="sec_description">Select a service based on your requirement. </p>

            <div class="col-md-12 padTop25">
                <div class="col-md-4 col-sm-4 col-xs-12 service text_Centre">
                    <img src="./images/brick_wall.jpg" alt="" >
                    <h3>Block Work Compound Wall</h3>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 service text_Centre">
                    <img src="./images/grill_fence.jpg" alt="">
                    <h3>Barbed Wire Fencing</h3>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 service text_Centre">
                    <img src="./images/precast_wall.jpg" alt="" >
                    <h3>Precast Compound Wall</h3>
                </div>
                
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    
    <!-- How to go about it? Section -->
    <section id="howItWorks" class="secondary_blue">
        <div class="container">
            <h1 class="sec_heading">How To Go About it?</h1>
            <p class="sec_description">3 easy steps to get your job done.</p>

            <div class="steps text_Centre">
                <div class="col-md-12 col-xs-12 col-sm-12 ">
                    
                    <div class="col-md-4 col-xs-12 step">
                        <img src="./images/design.png" alt="design" >
                        <p>Fill & send the enquiry form.</p>
                    </div>

                    <div class="col-md-3 col-xs-12 step">
                        <img src="./images/call.png" alt="call" >
                        <p>Receive a call regarding confirmation & other details</p>
                    </div>

                    <div class="col-md-4 col-xs-12 step">
                        <img src="./images/confirm.png" alt="confirm" >
                        <p>Work starts within 1-2 days after approval</p>
                    </div>
                </div>
            </div>
        </div>
    </section>  

    <div class="clearfix"></div>


    <!-- Questionary Section -->

    <section class="Questionary">       

        <div class="container">

            <div class="col-md-12">
                <h1 class="sec_heading">QUESTIONNAIRE / DESIGN ENQUIRY</h1>
                <p class="sec_description">Let us know your requirements below and book your FREE consultation with a SMAK Design Expert:</p>
            </div>

            <form method="POST" id="compound_wall_form">
                <input type="hidden" name="profession_type" id="profession_type" value="compound_wall">
                <div class="col-md-12  col-sm-12 col-xs-12">
                    <div class="col-md-6 Question col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="sel1">*Looking for services</label>
                            <select class="form-control" id="sel1" name="sel1" required>
                              <option value="" disabled selected>Select </option>
                              <option value="architects">Architects</option>
                              <option value="flooring">Flooring  Experts</option>
                              <option value="carpentry">Carpentry / Woodworking</option>
                              <option value="bathroomrenovation">Bathroom  Renovation</option>
                              <option value="kitchen">Kitchen  Renovation</option>
                              <option value="grillwork">Metal Fabrication / Grillwork</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6 Question col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="sel2">*Location</label>
                            <input type="text" class="form-control" id="sel2" name="sel2" placeholder="Enter Location">
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 Question col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="contact_no">*Contact No</label>
                            <input type="tel" class="form-control" id="contact_no" name="contact_no" placeholder="Enter Contact No.">
                          </div>
                    </div>

                    <div class="col-md-6 Question col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="email">*Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter Mail Id">
                          </div>
                    </div>

                    <div class="clearfix"></div>
                    
                    <div class="col md-12 col-xs-12 text_Centre">
                        <button type="submit" class="btn btn-enquiry">Send Enqiury <span class="arrow"><img src="./images/arrow.png" alt="" class=""></span></button>
                    </div>
                </div>
            </form>
            
        </div>

    </section>

    <div class="clearfix"></div>



    <!-- Advantages Section -->

    <section id="advantages">
        <div class="container">
            <div class="col-md-12">
                <h1 class="sec_heading">SMAK ADVANTAGES</h1>
                <p class="sec_description">For all your construction needs</p>

                <div class="col-md-12 advants">
                    <div class="col-md-3 advant text_Centre">
                        <div class="top">
                            <img src="./images/certfied.png" alt="">
                            <p>Certified Construction Professionals</p>
                        </div>                        

                    </div>
                    
                    <div class="col-md-3 advant text_Centre">
                        <div class="top">
                            <img src="./images/reliable.png" alt="">
                            <p>Reliable Service Delivery</p>
                        </div>
                        

                    </div>

                    <div class="col-md-3 advant text_Centre">
                        <div class="top">
                            <img src="./images/payment.png" alt="">
                            <p>Standard Rates</p>
                        </div>                        

                    </div>

                    <div class="col-md-3 advant text_Centre">
                        <div class="top">
                            <img src="./images/discount.png" alt="">
                            <p>Discounts on Labor + Materials Packages</p>
                        </div>                        

                    </div>
                </div>
            

            </div>
        </div>


    </section>

    <div class="clearfix"></div>

     <!-- Testimonial Section -->

    <section id="testimonials">
        <div class="container">

            <h1 class="sec_heading">TESTIMONIALS</h1>

            <div class="testimonial_slider">
                <div class="center text_Centre .autoplay">
                    <div class="col-md-4">
                        <img src="./images/customer_2.jpg" alt="">
                        <p>Interiors they designed turned out to look just as I expected it to. I am satisfied with the work.</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_1.jpg" alt="">
                        <p>I got my kitchen upgraded and I have a modular kitchen I always dreamt of. Thank you SMAK</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_3.jpg" alt="">
                        <p>I cannot recognize my old house of almost 20+ years! Quality work at reasonable price</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_5.jpg" alt="">
                        <p>One of my friends recommended Smak for fabrication work & I don’t regret my decision</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_4.jpg" alt="">
                        <p>Plumbers who worked for my bathroom renovation were truly skilled! Hats off to them.</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_6.jpg" alt="">
                        <p>They not only delivered the work on time, they also helped me with selecting superior quality materials. </p>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <div class="clearfix"></div>
    
    <?php 
        $this->load->view('footer');
        $this->load->view('script_links');
    ?>
     <script>

        $(document).ready(function(){
            $("#compound_wall_form").validate({
                rules: {
                  sel1: {
                    required: true
                  },
                  sel2: {
                    required: true
                  },
                  contact_no: {
                    required: true,
                    regex:/^(0|91)?[6789]\d{9}$/
                  },
                  email: {
                    required: true,
                    email:true
                  }
                },
                messages : {
                    sel1: {
                        required: "Please select service"
                    },
                    sel2: {
                        required: "Please select location"
                    },
                    contact_no:{
                        required: "Contact number is requires",
                        regex: "Please enter the valid contact number"
                    },
                    email: {
                        required: "Email Id is requires",
                        email: "Email should be in the format: abc@domain.tld"
                    }
                }
              });
             $.validator.addMethod(
                 "regex",
                 function(value, element, regexp) {
                     var re = new RegExp(regexp);
                     return this.optional(element) || re.test(value);
                 },
                 "Please check your input."
               );

             //submit join form
            $('#compound_wall_form').submit(function(event){
                event.preventDefault();
                //get the input data
                input = $('#compound_wall_form').serialize();
                base_url = '<?php echo base_url(); ?>';//empty the error messages
                $('#compound_wall_form .errMsg').empty();
                //submit
                $.ajax({
                    type: 'POST',
                    url: base_url+"/Smak/submit_professional_form",
                    data: input,
                    success : function(response){ 
                        if(response.status == false){
                            $.each(response.data['error'], function(key, value){
                               $('#compound_wall_form #'+key+'-error').html(value);
                            });
                        }else if(response.status == true){
                            $.dialog({
                                 title: 'Success!',
                                content: 'Request form submitted successfully!',
                            });
                            location.reload();
                        }
                    } 
                });
            });

            var $window = $(window);
            
                $window.scroll(function () {
                    if ($window.scrollTop() > 20) {          

                    $(".navbar").addClass('nav_blue');

                    }else{
                    $(".navbar").removeClass('nav_blue');
                    }
                });


                $('.center ').slick({
                dots: true,
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 3,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                    }
                ]
            });

        });


 </script>

</body>