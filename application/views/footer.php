
<footer> 
    <div class="container">

        <div id="scroll">
            <div class="container">
                <a href="#"><img src="./images/scroll_top.png" alt="ScrollTop" class="scroll_top"></a>
            </div>
        </div>

        <div class="col-md-12 col-xs-12 sub_footer">

            <div class="col-md-4 col-xs-6 footer_Item">

                <ul class="footer_navs">
                    <li>
                        <span class="footer_nav goToAboutUs handpointer">About Us</span>
                    </li>       
                    

                    <li>
                        <span class="footer_nav goToService handpointer">Our Services</span>
                    </li>

                    <li>
                        <span class="footer_nav goToPrivacy handpointer">Privacy Policy</span>
                    </li>

                    <li>
                        <span class="footer_nav goToContact handpointer">Contact Us</span>  
                    </li>
                    
                </ul>      
                
                
                               
                    
            </div>

            <div class="col-md-4 col-xs-6 footer_Item">
                
                <ul class="footer_navs">
                    <li><span class="footer_nav goToVijayNagar handpointer">Vijayanagar</span></li>
                    <li><span class="footer_nav goToMeadows handpointer">Smak Meadows </span></li>
                    <li> <span class="footer_nav goToTerms handpointer">Terms & Conditions</span></li>
                </ul>

            </div>

            <div class="col-md-4 col-xs-12 flexrow footer_Item">
                <span class="handpointer"><img src="./images/logo_white.png" alt=""></span>
                <!-- <div class="follow">

                    <span class="mar0" handpointer>Follow Us</span> <br>
                    <span class="socai_connect handpointer">
                        <span class=" handpointer 404Error"><img src="./images/facebook.png" alt="facebook"></span>
                        <span class=" handpointer 404Error"><img src="./images/linkedin.png" alt="linkedin"></span>
                        <span class=" handpointer 404Error"><img src="./images/twitter.png" alt="twitter"></span>
                        <span class=" handpointer 404Error"><img src="./images/instagram.png" alt="instagram"></span>
                        
                    </span>
                </div>                              -->
                    
            </div>
            
            
            
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12">
            <hr>
        </div>

        <div class="col-md-12">
            <div class="col-md-12 copy_right">
                <p>© 2020 Smak. All rights reserved.</p>
            </div>
            
        </div>
    </div>
</footer>