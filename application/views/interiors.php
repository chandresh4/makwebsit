<!DOCTYPE html>

<html lang="en">
    <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" href="<?php echo SITE_URL?>assets/interiors/images/favicon.png" />
      <title>Smak Interiors</title>
    
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/interiors/style/vendor.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">          
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

      <link rel="stylesheet" href="<?php echo SITE_URL?>assets/interiors/style/interiors.css">

      <style>
        .error{
            color:#ff6262;
        }
      </style>

      <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160640646-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-160640646-1');
        </script>

   </head> 
   <body> 
    <section>
        
        <header>

            <nav class="navbar">
            <div class="container">
            <div class="navbar-header">

                <a class="navbar-brand" href="<?php echo SITE_URL?>">
                    <img src="<?php echo SITE_URL?>assets/interiors/images/logo.png" alt="smack-logo">
                </a>
            </div>
            
            
            </div>
        </nav>

        </header>

        <!-- banner -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">

                        <div class="webView">
                            <img src="<?php echo SITE_URL?>assets/interiors/images/banner.jpg"  style="width:100%;" class="img-responsive" >
                        </div>

                        <div class="mobView">
                            <img src="<?php echo SITE_URL?>assets/interiors/images/banner_mobile.jpg"  style="width:100%;" class="img-responsive">
                        </div>

                    <div class="staticForm webView">

                        <form role="form" id="feedbackForm3" class="feedbackForm form_3" method="POST" data-form-type="form-3" onSubmit="return false;">
                            <h4>Contact us for a FREE Quote </h4>
                            <div class="group">
                                <div class="form-group">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <input type="text" class="form-control" id="name3" name="name" placeholder="Name" data-attr="Please enter correct name">
                                </div>


                                <div class="form-group">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                                </div>

                            </div>  

                            <div class="group">

                                <div class="form-group">
                                    <i class="fa fa-phone" aria-hidden="true"></i>  
                                    <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="countryCode3">
                                    <input type="text" class="form-control only_numeric phone" id="phone3" name="phone"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                                </div>

                            </div>  
                            
                            <div class="group">

                                <div class="form-group">
                                    <input type="text" class="form-control city" id="city3" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                                </div>
            
                            </div>  

							
							<input type="hidden" id="business_type" name="business_type" value="Interiors">
							<input type="hidden" id="project" name="project" value="interiors">

                            <div class="submitbtncontainer wrap">
                                <input type="submit" class="button" value="Submit" name="submit">
                            </div>
                        </form>

                        <form action="" id="otpFrom3" class="feedbackForm otp-form_3" style="display: none;" novalidate="novalidate">

                            <div class="group">
                                <div class="form-group">
                                    <i class="fa fa-phone" aria-hidden="true"></i>  
                                    <input type="text" class="form-control only_numeric phone" id="otp3" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                                </div>
                            </div> 

                            <div class="group">
                                <div class="form-group" style="margin-bottom: 0; text-align: right;">
                                    <a href="">Resend OTP</a>
                                </div>
                            </div> 
                            
                            <div class="submitbtncontainer wrap">
                                <input type="submit" class="button form_3-otp-submit-btn" value="Submit OTP" name="submit">
                            </div>

                            <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

                        </form>

                    </div>

                    <div class="bannerFooter">
                        <div class="container">
                            <img src="<?php echo SITE_URL?>assets/interiors/images/down_arrow.png" alt="" height='60'> 
                        </div>
                    </div>
                
                </div>           
            </div>
            <!-- Left and right controls --> 
        </div>

        <div class="mobileFormDiv"> 
            <form role="form" id="feedbackForm3" class="feedbackForm form_1 mobView mobileForm" method="POST" data-form-type="form-3" onSubmit="return false;">
                <h4>Contact us for a FREE Quote </h4>
                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control" id="name2" name="name" placeholder="Name" data-attr="Please enter correct name">
                    </div>


                    <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                    </div>

                </div>  

                <div class="group">

                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                        <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="countryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone2" name="phone"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                    </div>

                </div>   

                <div class="group">

                    <div class="form-group">
                        <input type="text" class="form-control city" id="city2" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                    </div>

                </div>  

                <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : ""); ?>">
                <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : ""); ?>">
                <input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : ""); ?>">
                <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : ""); ?>">
				<input type="hidden" id="utm_term" name="utm_term" value="<?php echo (isset($_REQUEST['utm_term']) != "" ? $_REQUEST['utm_term'] : ""); ?>">
				<input type="hidden" id="utm_content" name="utm_content" value="<?php echo (isset($_REQUEST['utm_content']) != "" ? $_REQUEST['utm_term'] : ""); ?>">
				
				<input type="hidden" id="adpos" name="adpos" value="<?php echo (isset($_REQUEST['adpos']) != "" ? $_REQUEST['adpos'] : ""); ?>">
				<input type="hidden" id="ad" name="ad" value="<?php echo (isset($_REQUEST['ad']) != "" ? $_REQUEST['ad'] : ""); ?>">
				
				<input type="hidden" id="business_type" name="business_type" value="Interiors">
				<input type="hidden" id="project" name="project" value="interiors">
				
                <div class="submitbtncontainer">
                    <input type="submit" value="Submit" name="submit">
                </div>
            </form>

            <form action="" id="otpFrom3" class="feedbackForm otp-form_3" style="display: none;" novalidate="novalidate">

            <div class="group">
                <div class="form-group">
                    <i class="fa fa-phone" aria-hidden="true"></i>  
                    <input type="text" class="form-control only_numeric phone" id="otp2" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                </div>
            </div> 

            <div class="group">
                <div class="form-group" style="margin-bottom: 0; text-align: right;">
                    <a href="">Resend OTP</a>
                </div>
            </div> 
            
            <div class="submitbtncontainer wrap">
                <input type="submit" class="button form_3-otp-submit-btn" value="Submit OTP" name="submit">
            </div>

            <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

        </form>

        </div>

        <!-- //banner --> 
    </section>

    <!-- About -->

    <div class="clearfix"></div>

    


    <div class="clearfix"></div>

    <section class="aboutSection">

        <div class="container-fluid">

        <!-- <h2 class="smakColor text-center"> Overview </h2> -->

            <div class="col-md-12 paddingZero">

            <div class="col-md-6 paddingZero aboutParaContent">

            

            <h2 class="marginBottom10"> Why choose SMAK?</h2>

            <p class="aboutPara marginBottom10">
            We are obsessed with providing our clients with services that are of international standard. Our proficient teams aim for perfection in each and every step by giving you the best interior designs that are apt for your lifestyle. Personalized modular kitchen, brilliant bedroom furniture and everything that you will require for luxury and comfort. 
            </p>

            <br>

            <p>

                <ul class="aboutListItems">
                    <li>Passionate teams & trained professionals</li>
                    <li>Easy on your wallet</li>
                    <li>Quick, hassle-free services</li>
                    <li>Superior quality materials </li>
                </ul>

            </p>

            <br>

            <p> <b>Contact us for further queries & get FREE quotes. </b> </p>

            </div>

                <div class="col-md-6 paddingZero leftAboutImage">

                    <div class="aboutImageSec">
                        <a href="<?php echo SITE_URL?>assets/interiors/images/about_us.jpg">
                            <img src="<?php echo SITE_URL?>assets/interiors/images/about_us.jpg" alt="" style="width:100%;" class="img-responsive aboutImgHeight">
                        </a>
                    </div>
                    
                </div>

                

            </div>

        </div>

    </section>

    <div class="clearfix"></div>

    <section class="nearBy">

        <div class="container">

            

            <div class="col-md-12 mtop30">

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/interiors/images/turnkey.png" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p> Turnkey Projects
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/interiors/images/bespoke.png" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                        Bespoke Modular <br>Furniture
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/interiors/images/color.png" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                        Colour <br>Coordination
                        </p>
                    </div>

                </div>

                <div class="col-md-3 locationItemSec">

                    <div class="locationImgSec">
                        <img src="<?php echo SITE_URL?>assets/interiors/images/visualization.png" alt="" class="img-responsive"></a>
                    </div>

                    <div class="locationTextSec">
                        <p>
                           3D Visualization
                        </p>
                    </div>

                </div>

                

                

            </div>

        </div>
        
    </section>

    <div class="clearfix"></div>

    <section id="services">
        <div class="container">
            <h1 class="sec_heading">Let your home interiors convey your signature</h1>
            <p class="sec_description">We deliver realistic 3D visuals to make your home match the image you always had in your mind. Every aspect of your home will go through thorough detailing </p>
        </div>
            <div class="col-md-12 padTop25">
                <div id='interior_service_1' class="col-md-4 col-sm-4 col-xs-12 interior_service text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="./images/kids_room.png" alt="" class="img-responsive" height='100%'  width='100%' > 
                    <h3 class="interior_service_1">Kids Room</h3>
                </div>

                <div id='interior_service_2' class="col-md-4 col-sm-4 col-xs-12 interior_service text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="./images/bedroom.png" alt="" class="img-responsive"height='100%' width='100%' >
                    <h3 class='interior_service_2'>Bed Room</h3>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 interior_service text_Centre">
                    <div id='interior_service_3' class="col-md-12 col-xs-12 interior_service" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                        <img src="./images/hall.png" alt="" class="img-responsive"  height='100%' width='100%'>
                        <h3 class='interior_service_3'>Hall</h3>
                    </div>
                    <div id='interior_service_4' class="col-md-12 col-xs-12 interior_service" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                        <img src="./images/kitchen_2.png" alt="" class="img-responsive" height='100%' width='100%'>
                        <h3 class='interior_service_4'>Kitchen</h3>
                    </div>
                </div>
            </div>

            <div id='interior_service_5' class="col-md-6 col-sm-4 col-xs-12 interior_service text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="./images/bath.png" alt="" class="img-responsive"height='100%' width='100%'>
                    <h3 class='interior_service_5'>Bath Room</h3>
            </div>

            <div id='interior_service_6' class="col-md-6 col-sm-4 col-xs-12 interior_service text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="./images/gym.png" alt="" class="img-responsive"height='100%' width='100%'>
                    <h3 class='interior_service_6'>Gym</h3>
            </div>
        
    </section>
    <div class="clearfix"></div>

    

    <footer>
        <div class="container "> 
            <hr>
            <p class="">
               
                <span class="copyrightSec"> © 2020 Smak. All Rights Reserved.</span>

                <span class="footerLinks">
                    <a href="https://www.smakconcepts.com/terms_condition" target="_blank"> Terms & Conditions </a>
                    <a href="https://www.smakconcepts.com/privacy" target="_blank"> Privacy Policy </a>
                </span>
            </p>
        </div>
    </footer> 
    
        <!-- contact form start -->
    <div class="floating-form" id="contact_form">
       <div class="contact-opener">Enquire Now</div>
            <form role="form" id="feedbackForm1" class="feedbackForm" method="POST">
            <h4> Contact us for a FREE Quote </h4>

                <div class="group">
                    <div class="form-group">
                    <i class="fa fa-user" aria-hidden="true"></i>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" data-attr="Please enter correct name">
                    </div>                    
                </div>  
                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                         <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" data-attr="Please enter correct email">
                    </div>

                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                         <input type="hidden" class="hiddenCountry" name="countryCode" value="91" id="CountryCode">
                        <input type="text" class="form-control only_numeric phone" id="phone" name="phone" pattern="\d*"  placeholder="Mobile Number" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="CountryCode_err2"> </span>
                    </div>
                    
                </div>

                <div class="group">
                    <div class="form-group">
                        <input type="text" class="form-control city" id="city" name="city"  placeholder="Enter City" data-attr="Please enter correct City Name">
                    </div>
                </div>  

                <input type="hidden" id="utm_source3" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : ""); ?>">
                <input type="hidden" id="utm_medium3" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : ""); ?>">
                <input type="hidden" id="utm_sub3" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : ""); ?>">
                <input type="hidden" id="utm_campaign3" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : ""); ?>">
                <div class="submitbtncontainer">
                    <input type="submit" value="Submit" name="submit" class="button pulse">
                </div>

            </form>

            <form action="" class="feedbackForm" style="display: none;">

                <div class="group">
                    <div class="form-group">
                        <i class="fa fa-phone" aria-hidden="true"></i>  
                        <input type="text" class="form-control only_numeric phone" id="otp" name="otp"  placeholder="Enter OTP" data-attr="Please enter correct OTP">
                    </div>
                </div> 

                <div class="group">
                    <div class="form-group" style="margin-bottom: 0; text-align: right;">
                        <a href="">Resend OTP</a>
                    </div>
                </div> 
                
                <div class="submitbtncontainer wrap">
                    <input type="submit" class="button" value="Submit OTP" name="submit">
                </div>

                <p style="font-size: 12px; margin-top: 12px; color: #305c67;">OTP is sent to the registered mobile no, if not received click resend otp</p>

            </form>

            <div>


    <input type="hidden" name="siteurl" id="siteurl" value="https://www.smakconcepts.com/" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo SITE_URL?>assets/interiors/js/vendor.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>     
    <script src="<?php echo SITE_URL?>assets/interiors/js/main.js"></script>  
    <script src="<?php echo SITE_URL?>assets/interiors/js/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
	<?php $this->load->view ("footer_scripts");?>
   </body>

</html>