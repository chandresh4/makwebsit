<!DOCTYPE html>
<html lang="en"> 
    <?php 
        $this->load->view('head');
    ?>
<body>

    <?php 
        $this->load->view('header');
    ?>
    <div class="clearfix"></div>

    <!-- Banner -->
    
    <section id="banner">
        <div class="aboutus_banner">
            <img src="./images/interior_banner.png" alt="Banner" class="banner_img img-responsive webView">
            <img src="./images/interior_banner_mobile.jpg" alt="Banner" class="banner_img img-responsive mobView">           
            
            <div class="container">
                <div class="banner-caption">
                    <div class="col-md-12 col-xs-12">
                        <h1 id="interior">Interiors with a blend <br>of functionality & <br>sensuality</h1>
                    </div>
                </div>
            </div> 
        </div>
    </section>

    <div class="clearfix"></div>
    <!-- Features  Section -->

    <section id="features">
        <div class="col-md-12">
            <div class="col-md-3" id="design">
                <div class="box">
                    <img src="./images/turnkey.png" alt="">
                    <h3 class="clr_primary">Turnkey Projects</h3>
                    <p> 
                        We offer a broad array of elegant commercial designs with a unique, holistic approach.
                    </p>
                    <!-- <button class=".btn readMore">Read More <span class="arrow"><img src="./images/arrow.png" alt=""></span></button> -->
                </div>
            </div>

            <div class="col-md-3" id="construction">
                <div class="box">
                    <img src="./images/building renovation2.png" alt="">
                    <h3 class="clr_primary">Bespoke Modular Furniture</h3>
                    <p>
                        Build effortlessly!
                        We build your dreams along with your buildings!
                        Explore a wide range of packages we provide…
                    </p>
                    <!-- <button class=".btn readMore">Read More <span class="arrow"><img src="./images/arrow.png" alt=""></span></button> -->
                </div>
            </div>

            <div class="col-md-3" id="renovation">
                <div class="box">
                    <img src="./images/coordination.png" alt="">
                    <h3 class="clr_primary">Colour Coordination</h3>
                    <p>
                        Our skilled professionals renovate your building not just to restore it, but to let it tell a story! 
                    </p>
                    <!-- <button class=".btn readMore">Read More <span class="arrow"><img src="./images/arrow.png" alt=""></span></button> -->
                </div>
            </div>

            <div class="col-md-3" id="visualization">
                <div class="box">
                    <img src="./images/3d.png" alt="">
                    <h3 class="clr_primary">3D Visualization</h3>
                    <p>
                        Our skilled professionals renovate your building not just to restore it, but to let it tell a story! 
                    </p>
                    <!-- <button class=".btn readMore">Read More <span class="arrow"><img src="./images/arrow.png" alt=""></span></button> -->
                </div>
            </div>

            
        </div>
    </section>

    <div class="clearfix"></div>

    <!-- Services Offered Section -->

    <section id="services">
        <div class="container">
            <h1 class="sec_heading">Infusing Homes with Desire</h1>
            <p class="sec_description">lorem Ipsum available, but the majority have suffered alterati on in some form, by ious by accident.lorem Ipsum available, but the majority have suffered </p>
        </div>
            <div class="col-md-12 padTop25">
                <div id='interior_service_1' class="col-md-4 col-sm-4 col-xs-12 interior_service text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="./images/kids_room.png" alt="" class="img-responsive" >
                    <h3 class="interior_service_1">Kids Room</h3>
                </div>

                <div id='interior_service_2' class="col-md-4 col-sm-4 col-xs-12 interior_service text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="./images/bedroom.png" alt="" class="img-responsive">
                    <h3 class='interior_service_2'>Bed Room</h3>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 interior_service text_Centre">
                    <div id='interior_service_3' class="col-md-12 col-xs-12 interior_service" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                        <img src="./images/hall.png" alt="" class="img-responsive" >
                        <h3 class='interior_service_3'>Hall</h3>
                    </div>
                    <div id='interior_service_4' class="col-md-12 col-xs-12 interior_service" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                        <img src="./images/kitchen_2.png" alt="" class="img-responsive">
                        <h3 class='interior_service_4'>Kitchen</h3>
                    </div>
                </div>
            </div>

            <div id='interior_service_5' class="col-md-6 col-sm-4 col-xs-12 interior_service text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="./images/bath.png" alt="" class="img-responsive">
                    <h3 class='interior_service_5'>Bath Room</h3>
            </div>

            <div id='interior_service_6' class="col-md-6 col-sm-4 col-xs-12 interior_service text_Centre" onmouseover='hover(this)' onmouseout='mouseOut(this)'>
                    <img src="./images/gym.png" alt="" class="img-responsive">
                    <h3 class='interior_service_6'>Gym</h3>
            </div>
        
    </section>

    <div class="clearfix"></div>
    
    <!-- Our project Section -->

    <section id='our_Projects'>

        <div class="container">
            <h1 class="sec_heading">Our project</h1>
            <p class="sec_description">Choose from the following options based on your specific requirement.</p>

            <div class="projectslider">
            <div class="col-md-12 container">
                        
                        <div class=" col-md-12">

                            <div class="projectsSlider">

                            <div class="P_slider col-md-12" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>

                                <div class="slider_Item">
                                    <a href="<?php echo SITE_URL?>/images/ourproject1.png">
                                        <img src="./images/ourproject1.png" alt="" style="width:100%;" class="img-responsive">
                                    </a>
                                    <h2 class="center-text">Project Name</h2>
                                    
                                </div>

                                <div class="slider_Item">
                                    <a href="<?php echo SITE_URL?>/images/ourproject2.png"> 
                                        <img src="<?php echo SITE_URL?>/images/ourproject2.png" alt="" style="width:100%;" class="img-responsive">             
                                    </a>
                                    <h2 class="center-text">Project Name</h2>
                                </div>

                                <div class="slider_Item">
                                    <a href="<?php echo SITE_URL?>/images/ourproject3.png">
                                        <img src="<?php echo SITE_URL?>/images/ourproject3.png" alt="" style="width:100%;" class="img-responsive">
                                    </a>
                                    <h2 class="center-text ">Project Name</h2>
                                </div>

                                <div class="slider_Item">
                                    <a href="<?php echo SITE_URL?>/images/ourproject4.png">
                                        <img src="<?php echo SITE_URL?>/images/ourproject1.png" alt="" style="width:100%;" class="img-responsive">
                                    </a>
                                    <h2 class="center-text">Project Name</h2>
                                </div>

                                <div class="slider_Item">
                                    <a href="<?php echo SITE_URL?>/images/ourproject1.png">                            
                                        <img src="<?php echo SITE_URL?>/images/ourproject1.png" alt="" style="width:100%;" class="img-responsive">
                                    </a>
                                    <h2 class="center-text">Project Name</h2>
                                </div>                              

                            </div>
                        </div>
                        </div>



            </div>

        </div>   
    
    
    </section>



   

    <div class="clearfix"></div>
    
     <!-- Testimonial Section -->

    <section id="testimonials">
        <div class="container">

            <h1 class="sec_heading">TESTIMONIALS</h1>

            <div class="testimonial_slider">
                <div class="center text_Centre .autoplay">
                    <div class="col-md-4">
                        <img src="./images/customer_2.jpg" alt="">
                        <p>Interiors they designed turned out to look just as I expected it to. I am satisfied with the work.</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_1.jpg" alt="">
                        <p>I got my kitchen upgraded and I have a modular kitchen I always dreamt of. Thank you SMAK</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_3.jpg" alt="">
                        <p>I cannot recognize my old house of almost 20+ years! Quality work at reasonable price</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_5.jpg" alt="">
                        <p>One of my friends recommended Smak for fabrication work & I don’t regret my decision</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_4.jpg" alt="">
                        <p>Plumbers who worked for my bathroom renovation were truly skilled! Hats off to them.</p>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/customer_6.jpg" alt="">
                        <p>They not only delivered the work on time, they also helped me with selecting superior quality materials. </p>
                    </div>
                </div>
            </div>
        </div>

    </section>
    
    <div class="clearfix"></div>
   
    <?php 
        $this->load->view('footer');
        $this->load->view('script_links');
    ?>


     <script>

        $(document).ready(function(){

            $('.P_slider').slick({ 
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                    },
                    {
                    breakpoint: 700,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '20px',
                        autoplay:true,
                        autoplaySpeed: 1500,
                        slidesToShow: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: false,
                        centerPadding: '40px',
                        autoplay:true,
                        autoplaySpeed: 1500,
                        slidesToShow: 1
                    }
                    }
                ]
            });

            


             $("#carpentry_form").validate({
                rules: {
                  sel1: {
                    required: true
                  },
                  sel2: {
                    required: true
                  },
                  contact_no: {
                    required: true,
                    regex:/^(0|91)?[6789]\d{9}$/
                  },
                  email: {
                    required: true,
                    email:true
                  }
                },
                messages : {
                    sel1: {
                        required: "Please select service"
                    },
                    sel2: {
                        required: "Please select location"
                    },
                    contact_no:{
                        required: "Contact number is requires",
                        regex: "Please enter the valid contact number"
                    },
                    email: {
                        required: "Email Id is requires",
                        email: "Email should be in the format: abc@domain.tld"
                    }
                }
              });
             $.validator.addMethod(
                 "regex",
                 function(value, element, regexp) {
                     var re = new RegExp(regexp);
                     return this.optional(element) || re.test(value);
                 },
                 "Please check your input."
               );

             //submit join form
            $('#carpentry_form').submit(function(event){
                event.preventDefault();
                //get the input data
                input = $('#carpentry_form').serialize();
                base_url = '<?php echo base_url(); ?>';//empty the error messages
                $('#carpentry_form .errMsg').empty();
                //submit
                $.ajax({
                    type: 'POST',
                    url: base_url+"/Smak/submit_professional_form",
                    data: input,
                    success : function(response){ 
                        if(response.status == false){
                            $.each(response.data['error'], function(key, value){
                               $('#carpentry_form #'+key+'-error').html(value);
                            });
                        }else if(response.status == true){
                            $.dialog({
                                 title: 'Success!',
                                content: 'Request form submitted successfully!',
                            });
                            location.reload();
                        }
                    } 
                });
            });

            var $window = $(window);
            
                $window.scroll(function () {
                    if ($window.scrollTop() > 20) {          

                    $(".navbar").addClass('nav_blue');

                    }else{
                    $(".navbar").removeClass('nav_blue');
                    }
                });


                $('.center ').slick({
                dots: true,
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 3,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                    }
                ]
            });

        });



        
  </script>

</body>