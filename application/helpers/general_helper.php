<?php
if(! function_exists ('is_Admin_Login') ) {
	function is_Admin_Login($username, $uid, $admin_role, $admin_role_id, $admin_role_details, $cur_controller) {
		
		// CHECK IP ADDRESS IS ALLOWED FOR VIEW
		//is_ip_allowed();
		
		if( strlen (trim ($username)) > 0 && is_numeric($uid) != "" && 
			strlen (trim ($admin_role)) > 0 && is_numeric($admin_role_id) != "" &&  
			strlen (trim ($admin_role_details)) > 0 && trim($cur_controller) != ""){
			
			if(strtolower($cur_controller) == 'extra_call') {
				// IF ITS AJAX CALL THEN DONT CHECK FOR PERMISSION
				return true;
			} else {
				// OTHERWISE CHECK FOR PERMISSION
				if (check_role_permission ($admin_role_details, $cur_controller) != false) {
					// IF USER IS ALLOWED THEN KEEP HE/SHE TO THAT PAGE	
					return true;
				} else {
					return "404 not found";
					// ELSE REDIRECT TO 404 PAGE
					//redirect(FULL_CMS_URL."/".ADMIN_DEFAULT_CONTROLLER);		
				}
			}
		}else{
			// THEN REDIRECT
			redirect(FULL_CMS_URL."/".ADMIN_LOGOUT_CONTROLLER);
		}
	}
}
if(! function_exists('check_role_permission')) {
	function check_role_permission($role_details, $cur_controller) {
		
		// CONVERT PERMISSION IN SINGLE ARRAY AND MAKE IT CLEAN PER ELEMENT ONE CONTROLLER
		$arr_permission = create_permission_array($role_details);
		
			
		// CHECK USER REQUESTED PAGE IS PERMITTED TO THAT USER OR ROLE WHICH IS GRANTED TO USER
		if (in_array(strtolower($cur_controller), $arr_permission)) {
			return true;	
		} else {
			return false;
		}
	}
}
if(! function_exists('create_permission_array')) {
	function create_permission_array($role_details) {
		// UNSERIALIZE ARRAY AND STORE IN VARIABLE
		$arr_role_details = unserialize($role_details);
		
		// SEPERATE THE DOUBLE VARIALE
		foreach($arr_role_details as $k => $v) {
			
			// EXTRACT THE STRIG FROM COMMA
			$arr_cur_val = explode(",", $v);
			foreach ($arr_cur_val as $k1 => $v1) {
				$arr_permission[] = strtolower($v1);
			}
		}
		return $arr_permission;
	}	
}
if(! function_exists ('is_Admin_Login_Redirect') ) {
	function is_Admin_Login_Redirect ($username, $uid, $admin_role, $admin_role_id, $admin_role_details, $cur_controller) {	
		
		// CHECK IP ADDRESS IS ALLOWED FOR VIEW
		is_ip_allowed();
		
		if( strlen (trim ($username)) > 0 && is_numeric($uid) != "" && 
			strlen (trim ($admin_role)) > 0 && is_numeric($admin_role_id) != "" &&  
			strlen (trim ($admin_role_details)) > 0 && trim($cur_controller) != ""){
		
			// THEN REFIRECT 
			redirect(FULL_CMS_URL."/dashboard");
		} else {
			// DO NOTHING
			return true;
		}
	}	
}




function get_cuurent_city()
{
	$temp_res = get_location_details();
      $res=json_decode($temp_res,true);
		  $ip_city="";
	      
		  if($res['status'] == 'success')
		  {
			  if( isset($res['city']) )
			  {
				  $ip_city=strtolower(trim($res['city']));
			   }
		  }
		  return $ip_city;
}

function get_current_state()
{
	$temp_res = get_location_details();
      $res=json_decode($temp_res,true);
		  $ip_state="";
	      
		  if($res['status'] == 'success')
		  {
			  if( isset($res['regionName']) )
			  {
				  $ip_state=strtolower(trim($res['regionName']));
			  }
		  }
		  return $ip_state;
}



if( ! function_exists("all_arrays")) {
	  function all_arrays() {
		
		$city          = array(
							   "1" => "Bangalore",
							   "2" => "Delhi",
							   "3" => "Mumbai",
							   "4" => "Kolkata"
							);					   
		
		
		$caller_status    = array(
		                    	"1" => "Qualified for Appointment",
								"2" => "Appointment rejected",
								"3" => "Irrelevant documents",
								"4" => "Irrelevant leads" 
							);
		
		$date_cat          = array(
		                         "1" => "Today",
								 "2" => "Yesterday",
								 "3" => "Last 7 Days",
								 "4" => "This Month -".date('F'),
								 "5" => "Last Month -".date('F', mktime(0, 0, 0, (date('m')-1 ), 10)),
								 "6" => "Last six Month",
								 "7" => "All Time",
								 "8" => "Customize date"
								 );	

								 
		$clients_arr    = array(
		                    	"1" => "Dr Batra",
		                    	"2" => "srlworld",
								"3" => "Jubination"
							);																														 
		$srl_time_slot = array(
								"1" => "6:00 AM",
								"2" => "6:30 AM",
								"3" => "7:00 AM",
								"4" => "7:30 AM",
								"5" => "8:00 AM",
								"6" => "8:30 AM",
								"7" => "9:00 AM",
								"8" => "9:30 AM",
								"9" => "10:00 AM",
							    "10" => "10:30 AM",
								"11" => "11:00 AM",
								"12" => "11:30 AM",
								"13" => "12:00 PM",
								"14" => "12:30 PM",
								"15" => "01:00 PM",
								"16" => "01:30 PM",
								"17" => "02:00 PM",
								"18" => "02:30 PM",
								"19" => "03:00 PM",
							);
							
		$srl_test_new 	 =  array(	
									"COMPLETE CARE" =>array( "1" => "Basic (27 TESTS)",
															 "2" => "Essential (46 TESTS)",
															 "3" => "Advance (63 TESTS)",
															 "4" => "Total (72 TESTS)",
															 "5" => "Premium (78 TESTS)"
															),
									"FITNESS CARE" =>array( "1" => "Basic (46 TESTS)",
															"2" => "Advance (59 TESTS)",
															"3" => "Performance (48 TESTS)"
															),
									"EVERGREEN CARE" =>array("1" => "60+ Male (56 TESTS)",
															 "2" => "60+ Female (57 TESTS)",
															 "3" => "50+ Male (54 TESTS)",
															 "4" => "50+ Female (68 TESTS)",
															 "5" => "40+ Male (62 TESTS)",
															 "6" => "40+ Female (60 TESTS)",
															),
									"ACTIVE CARE" =>array(  "1" => "Sedentary Lifestyle (46 TESTS)",
															"2" => "Professionals on the go (47 TESTS)",
															"3" => "Homemakers (48 TESTS)",
															"4" => "Socializers (38 TESTS)"
															),
									"VITAL CARE" =>array(   "1" => "Heart",
															"1" => "Liver",
															"1" => "Kidney",
															"4" => "Thyroid",
															"5" => "Bone",
															"6" => "Menopause"
															)
															
                               
         						);
		
		
		$srl_test 	 =  array( 	"1" =>		array("Health Champion Package Advanced", "4,650", "(HCP Advance)","(84 Tests)"),
                                "2" =>  	array("Health Champion Package Plus", "2,950", "(HCP +)","(60 Tests)"),
								"3" =>    	array("Senior citizen Male", "1,950", " ","(52 Tests)"),
                                "4" =>    	array("Senior Citizen  Female", "1,850", " ","(51 Tests)"),
                                "5" =>    	array("Healthy Lady Package", "1,350", " ","(44 Tests)"),
                                "6" =>    	array("Healthy Gentlemen Package", "1,450", " ","(41 Tests)"),
         						"7" =>		array("Youthful He Package",  "999", " ","(36 Tests)"),
         						"8" =>		array("Youthful She Package",  "999", " ","(36 Tests)"),
         						"9" =>		array("Diabetes Check",  "1,050", " ","(30 Tests)"),
         						"10" =>		array("Healthy Heart Package Advanced",  "2,450", " ","(51 Tests)"),
         						"11" =>		array("Extensive Diabetes Check",  "1,250", " ","(35 Tests)"),
         						"12" =>		array("Healthy Liver Package",  "750", " ","(8 Tests)"),
         						"13" =>		array("Thyroid Profile ",  "400", " ","(3 Tests)"),
         						"14" =>		array("Hypertension Profile ",  "1,450", " ","(29 Tests)"),
         						"15" =>		array("Thyroid Panel II",  "900", " ","(3 Tests)"),
         						"16" =>		array("Obesity Package",  "1,350", " ","(28 Tests)"),
         						"17" =>		array("Healthy Heart Package",  "1,450", " ","(45 Tests)"),
         						"18" =>		array("Arthritis Screen ",  "1,250", "","(37 Tests)" )
         						);
		 $itokyo_test 	 =  array( 	"1" =>	array("EDELWEISS TOKIO LIFE", "Know More", "","MYLIFE+"),
                                "2" =>  	array("EDELWEISS TOKIO LIFE", "Know More", "(HCP +)","WEALTH PLUS"),
								"3" =>    	array("EDELWEISS TOKIO LIFE", "Know More", " ","GCAP"),
                                "4" =>    	array("EDELWEISS TOKIO LIFE", "Know More", " ","POS SARAL NIVESH"),
                                "5" =>    	array("EDELWEISS TOKIO LIFE", "Know More", " ","Wealth Accumulation"),
                                "6" =>    	array("EDELWEISS TOKIO LIFE", "Know More", " ","CRITICARE+"),
         						"7" =>		array("EDELWEISS TOKIO LIFE", "Know More", " ","TOTALSECURE+"),
         						
         						);
		
		  

		$jubination_pkg =       array(  "1" => "FULL BODY CHECKUP PACKAGE FOR COUPLE",
										"2" => "COMPREHENSIVE PREVENTIVE HEALTH PROGRAM",
										"3" => "DIABETES COUPLE PACKAGE",
										"4" => "WELLNESS PLATINUM MALE",
										"5" => "WELLNESS PLATINUM FEMALE");

		$jubi_time_slot = 		array(
									"1" => "07.00 - 07.30",
									"2" => "07.30 - 08.00",
									"3" => "08.00 - 08.30",
									"4" => "08.30 - 09.00",
								);		
		$medlife_test =         array(   "1" => "Aarogyam B (60 Tests)",
										 "2" => "Healthscreen A Profile*(71 Tests)",
										 "3" => "Healthscreen B Profile*(74 Tests)",
										 "4" => "Healthscreen C Profile*(79 Tests)",
										 "5" => "Medlife Kushal Basic 42",
										 "6" => "Medlife Kushal Comprehensive 73",
										 "7" => "Medlife Kushal Comprehensive 76",
										 "8" => "Medlife Kushal Comprehensive 3-76",
										 "9" => "Medlife Kushal Pro 76",
										 "10"=> "Medlife Kushal Basic 41",								 
										 "11" => "Medlife Kushal Advanced 68 Parameters",
										 "12"=> "Medlife Kushal Comprehensive 3(77 Parameters)",
										 "13" => "Medlife Kushal Basic 41 Parameters",
										 "14" => "Medlife Kushal Basic 1-41 Parameters",
										 "15" => "Medlife Kushal Basic 2-42 Parameters",
										 "16" => "Alcohol Impact Checkup - 49 Parameters",
										 "17" => "Medlife Kushal Pro-70 Parameters",
										 "18" => "Medlife Obesity Advanced Package - 70 Parameters",
										 "19" => "Medlife Kushal Advanced-68 Parameters",
										 "20" => "Medlife Diabetic Screening-55 Parameters",
										 "21" => "Medlife Nutrition Deficiency Screening - 76 Parameters",
										 "22" => "Medlife Kushal Pro 1- 76 Parameters",
										 "23" => "Medlife Fever Screening-61 Parameters",
										 "24" => "Medlife Kushal Comprehensive 1-73 Parameters",
										 "25" => "Smoker's Screening Package - 51 Parameters",
										 "26" => "Medlife Kushal Comprehensive 2-76 Parameters",
										 "27" => "Medlife Comprehensive Heart",
										 "28" => "Medlife Kushal Comprehensive 3 Male",
										 "29" => "Medlife Kushal Comprehensive 3 Female",
										 "30" => "Basic Health Checkup (48 Tests)",
										 "31" => "Advanced Full Body Checkup (72 Tests)",
										 "32" => "Comprehensive Full Body Checkup - (77 Tests)",
										 "33" => "Comprehensive Full Body Checkup with Vitamin D - (78 Tests)",
										 "34" => "Comprehensive Full Body Checkup with Vitamin D & B12 (79 Tests)",
										 "35" => "Master full body checkup with Cancer & Arthritis Screening [Male] 85 (Tests)",
										 "36" => "Master full body checkup with Cancer & Arthritis Screening {Female} (85 Tests)",
										 "37" => "Complete Blood Count Test (20 Tests)",
										 "38" => "Thyroid Profile(3 Tests)",
										 "39" => "Lipid Profile(6 Tests)",
										 "40" => "Comprehensive PCOD profile(9 Tests)",
										 "41" => "Medlife Ayushman Comprehensive Plus(70 Tests)",
										 "42" => "Alcohol Impact Checkup(49 Tests)",
										 "43" => "Medlife Diabetic Screening(55 Tests)",
										 "44" => "Medlife Arthritis Basic Screening(26 Tests)",
 									);
		
		$etokio_plan   = 		array("1" => "Retirement Plan",
                                      "2" => "Pension Plan",
                                      "3" => "Child Plan",
                                      "4" => "ULIP", 
									  "5"=> "Savings Plan");					
								
								
		$etokio_age    = 		array( "1" => "0 - 5",
                                       "2" => "5 - 10",
                                       "3" => "10 - 15",
                                       "4" => "15 - 20",
                                       "5" => "20 - 25",
                                       "6" => "25 - 30",
                                       "7" => "30 - 35",
                                       "8" => "35 - 40",
                                       "9" => "40 - 45",
                                       "10" => "45 - 50",
                                       "11" => "50 - 55",
                                       "12" => "55 - 60",
                                       "13" => "60+"
									   );

	
		$puravankara_freedom_time = array(	 "01:00 AM" => "01:00 AM", "01:30 AM" => "01:30 AM", "02:00 AM" => "02:00 AM", "02:30 AM" => "02:30 AM", "03:00 AM" => "03:00 AM", "03:30 AM" => "03:30 AM", "04:00 AM" => "04:00 AM", "04:30 AM" => "04:30 AM", "05:00 AM" => "05:00 AM", "05:30 AM" => "05:30 AM", "06:00 AM" => "06:00 AM", "06:30 AM" => "06:30 AM", "07:00 AM" => "07:00 AM", "07:30 AM" => "07:30 AM", "08:00 AM" => "08:00 AM", "08:30 AM" => "08:30 AM", "09:00 AM" => "09:00 AM", "09:30 AM" => "09:30 AM", "10:00 AM" => "10:00 AM", "10:30 AM" => "10:30 AM", "11:00 AM" => "11:00 AM", "11:30 AM" => "11:30 AM", "12:00 PM" => "12:00 PM", "12:30 PM" => "12:30 PM", "01:00 PM" => "01:00 PM", "01:30 PM" => "01:30 PM", "02:00 PM" => "02:00 PM", "02:30 PM" => "02:30 PM", "03:00 PM" => "03:00 PM", "03:30 PM" => "03:30 PM", "04:00 PM" => "04:00 PM", "04:30 PM" => "04:30 PM", "05:00 PM" => "05:00 PM", "05:30 PM" => "05:30 PM", "06:00 PM" => "06:00 PM", "06:30 PM" => "06:30 PM", "07:00 PM" => "07:00 PM", "07:30 PM" => "07:30 PM", "08:00 PM" => "08:00 PM", "08:30 PM" => "08:30 PM", "09:00 PM" => "09:00 PM", "09:30 PM" => "09:30 PM", "10:00 PM" => "10:00 PM", "10:30 PM" => "10:30 PM", "11:00 PM" => "11:00 PM", "11:30 PM" => "11:30 PM", "12:00 AM" => "12:00 AM", "12:30 AM" => "12:30 AM"
									);
		$puravankara_bluemont_country = array(						
											"91"	=> "India (+91)",
											"1"	=> "USA (+1)",
											"44"	=> "UK (+44)",
											"61"	=> "Australia (+61)",
											"60"	=> "Malaysia (+60)",
											"65"	=> "Singapore (+65)",
											"852"	=> "Hong Kong (+852)", 
											"971"	=> "Dubai ()", 
											"974"	=> "Qatar (+974)",
											"971"=> "UAE (+971)",
											"966"=> "Saudi Arabia (+966)",
											"965"=> "Kuwait (+965)",
											"973"=> "Bahrain (+973)",
											"968"=> "Oman (+968)"
									);	
		$puravankara_bluemont_country_others = array(
						  	  "1" => "Algeria (+213)", "2" => "Andorra (+376)", "3" => "Angola (+244)", "4" => "Anguilla (+1264)", "5" => "Antigua &amp; Barbuda (+1268)", "6" => "Argentina (+54)", "7" => "Armenia (+374)", "8" => "Aruba (+297)", "9" => "Australia (+61)", "10" => "Austria (+43)", "11" => "Azerbaijan (+994)", "12" => "Bahamas (+1242)", "13" => "Bangladesh (+880)", "14" => "Barbados (+1246)", "15" => "Belarus (+375)", "16" => "Belgium (+32)", "17" => "Belize (+501)", "18" => "Benin (+229)", "19" => "Bermuda (+1441)", "20" => "Bhutan (+975)", "21" => "Bolivia (+591)", "22" => "Bosnia Herzegovina (+387)", "23" => "Botswana (+267)", "24" => "Brazil (+55)", "25" => "Brunei (+673)", "26" => "Bulgaria (+359)", "27" => "Burkina Faso (+226)", "28" => "Burundi (+257)", "29" => "Cambodia (+855)", "30" => "Cameroon (+237)", "31" => "Canada (+1)", "32" => "Cape Verde Islands (+238)", "33" => "Cayman Islands (+1345)", "34" => "Central African Republic (+236)", "35" => "Chile (+56)", "36" => "China (+86)", "37" => "Colombia (+57)", "38" => "Comoros (+269)", "39" => "Congo (+242)", "40" => "Cook Islands (+682)", "41" => "Costa Rica (+506)", "42" => "Croatia (+385)", "43" => "Cuba (+53)", "44" => "Cyprus North (+90392)", "45" => "Cyprus South (+357)", "46" => "Czech Republic (+42)", "47" => "Denmark (+45)", "48" => "Djibouti (+253)", "49" => "Dominica (+1809)", "50" => "Dominican Republic (+1809)", "51" => "Ecuador (+593)", "52" => "Egypt (+20)", "53" => "El Salvador (+503)", "54" => "Equatorial Guinea (+240)", "55" => "Eritrea (+291)", "56" => "Estonia (+372)", "57" => "Ethiopia (+251)", "58" => "Falkland Islands (+500)", "59" => "Faroe Islands (+298)", "60" => "Fiji (+679)", "61" => "Finland (+358)", "62" => "France (+33)", "63" => "French Guiana (+594)", "64" => "French Polynesia (+689)", "65" => "Gabon (+241)", "66" => "Gambia (+220)", "67" => "Georgia (+7880)", "68" => "Germany (+49)", "69" => "Ghana (+233)", "70" => "Gibraltar (+350)", "71" => "Greece (+30)", "72" => "Greenland (+299)", "73" => "Grenada (+1473)", "74" => "Guadeloupe (+590)", "75" => "Guam (+671)", "76" => "Guatemala (+502)", "77" => "Guinea (+224)", "78" => "Guinea - Bissau (+245)", "78" => "Guyana (+592)", "80" => "Haiti (+509)", "81" => "Honduras (+504)", "82" => "Hong Kong (+852)", "83" => "Hungary (+36)", "84" => "Iceland (+354)", "85" => "Indonesia (+62)", "86" => "Iran (+98)", "87" => "Iraq (+964)", "88" => "Ireland (+353)", "89" => "Israel (+972)", "90" => "Italy (+39)", "91" => "Jamaica (+1876)", "92" => "Japan (+81)", "93" => "Jordan (+962)", "94" => "Kazakhstan (+7)", "95" => "Kenya (+254)", "96" => "Kiribati (+686)", "97" => "Korea North (+850)", "98" => "Korea South (+82)", "99" => "Kyrgyzstan (+996)", "100" => "Laos (+856)", "101" => "Latvia (+371)", "102" => "Lebanon (+961)", "103" => "Lesotho (+266)", "104" => "Liberia (+231)", "105" => "Libya (+218)", "106" => "Liechtenstein (+417)", "107" => "Lithuania (+370)", "108" => "Luxembourg (+352)", "109" => "Macao (+853)", "110" => "Macedonia (+389)", "111" => "Madagascar (+261)", "112" => "Malawi (+265)", "113" => "Malaysia (+60)", "114" => "Maldives (+960)", "115" => "Mali (+223)", "116" => "Malta (+356)", "117" => "Marshall Islands (+692)", "118" => "Martinique (+596)", "119" => "Mauritania (+222)", "120" => "Mayotte (+269)", "121" => "Mexico (+52)", "122" => "Micronesia (+691)", "123" => "Moldova (+373)", "124" => "Monaco (+377)", "125" => "Mongolia (+976)", "126" => "Montserrat (+1664)", "127" => "Morocco (+212)", "128" => "Mozambique (+258)", "129" => "Myanmar (+95)", "130" => "Namibia (+264)", "131" => "Nauru (+674)", "132" => "Nepal (+977)", "133" => "Netherlands (+31)", "134" => "New Caledonia (+687)", "135" => "New Zealand (+64)", "136" => "Nicaragua (+505)", "137" => "Niger (+227)", "138" => "Nigeria (+234)", "139" => "Niue (+683)", "140" => "Norfolk Islands (+672)", "141" => "Northern Marianas (+670)", "142" => "Norway (+47)", "143" => "Palau (+680)", "144" => "Panama (+507)", "145" => "Papua New Guinea (+675)", "146" => "Paraguay (+595)", "147" => "Peru (+51)", "148" => "Philippines (+63)", "149" => "Poland (+48)", "150" => "Portugal (+351)", "151" => "Puerto Rico (+1787)", "152" => "Reunion (+262)", "153" => "Romania (+40)", "154" => "Russia (+7)", "155" => "Rwanda (+250)", "156" => "San Marino (+378)", "157" => "Sao Tome & Principe (+239)", "158" => "Senegal (+221)", "159" => "Serbia (+381)", "160" => "Seychelles (+248)", "161" => "Sierra Leone (+232)", "162" => "Singapore (+65)", "163" => "Slovak Republic (+421)", "164" => "Slovenia (+386)", "165" => "Solomon Islands (+677)", "166" => "Somalia (+252)", "167" => "South Africa (+27)", "168" => "Spain (+34)", "169" => "Sri Lanka (+94)", "170" => "St. Helena (+290)", "171" => "St. Kitts (+1869)", "172" => "St. Lucia (+1758)", "173" => "Sudan (+249)", "174" => "Suriname (+597)", "175" => "Swaziland (+268)", "176" => "Sweden (+46)", "177" => "Switzerland (+41)", "178" => "Syria (+963)", "179" => "Taiwan (+886)", "180" => "Tajikstan (+7)", "181" => "Thailand (+66)", "182" => "Togo (+228)", "183" => "Tonga (+676)", "184" => "Trinidad & Tobago (+1868)", "185" => "Tunisia (+216)", "186" => "Turkey (+90)", "187" => "Turkmenistan (+7)", "188" => "Turkmenistan (+993)", "189" => "Turks &amp; Caicos Islands (+1649)", "190" => "Tuvalu (+688)", "191" => "Uganda (+256)", "192" => "Uruguay (+598)", "193" => "Uzbekistan (+7)", "194" => "Vanuatu (+678)", "195" => "Vatican City (+379)", "196" => "Venezuela (+58)", "197" => "Vietnam (+84)", "198" => "Virgin Islands - British (+1284)", "199" => "Virgin Islands - US (+1340)", "200" => "Wallis &amp; Futuna (+681)", "201" => "Yemen (North)(+969)", "202" => "Yemen (South)(+967)", "203" => "Zambia (+260)", "204" => "Zimbabwe (+263)",	
                            		
						);
			
		$religare_city = array("Agra", "Ahmedabad", "Aligarh", "Allahabad", "Ambala", "Amritsar", "Bhatinda", "Bengaluru", "Bhopal", "Bhubaneswar", "Chandigarh", "Chennai", "Coimbatore", "Cuttack", "Dehradun", "Faridabad", "Ghaziabad", "Gorakhpur", "GreaterNoida", "Gurgaon", "Hyderabad", "Indore", "Jaipur", "Kanpur", "Kochi", "Kolkata", "Lucknow", "Ludhiana", "Madurai", "Mangalore", "Margao", "Meerut", "Mohali", "Mumbai", "Nashik", "Navi Mumbai", "New Delhi", "Noida", "Panaji", "Panchkula", "Pune", "Ranchi", "Surat", "Thane", "Vadodara", "Vapi", "Varanasi", "Virar");
		
		$lp_name = array(   
				   "1" => "purvaamaiti",
				   "2" => "adoradegoa",
				   "3" => "freedom",
				   "4" => "skydale",
				   "5" => "providentsunworth",
				   "6" => "purvabluemont",
				   "7" => "welworthcity",
				   "8" => "raysofdawn",
				   "9" => "palmbeach",
				   "10" => "silversands",
				   "11" => "parksquare",
				   "12" => "westend",
				   "13" => "sunflower",
				   "14" => "swanlake",
				   "15" => "purvawindermere",
				   "16" => "grandbay",
				   "17" => "oceana",
				   "18" =>"northerndestiny",
				   "19" =>"vriksha",
				   "20" =>"neora",
				   "21" =>"watabid",
				   "22" =>"highland",
				   "23" =>"balinese",
				   "24" =>"toogoodhomes",				   
				   "25" =>"greenpark",
				   "26" =>"zenium",
				   "27" =>"somerset",
				   "28"=>"smilingwillows",
				   "29"=> "Parkwoods",
				   "30"=> "equinox",
				   "31"=> "coronation",
				   "32"=> "capella",
				  ); 
		
		$lsbf_cource = Array
				(
				    "1" => "Diploma in Data Analytics - Full Time",
				    "2" => "Diploma in Data Analytics - Part Time",
				    "3" => "Foundation Diploma in Business Studies - Full time",
				    "4" => "Foundation Diploma in Business Studies - Part time",
				    "5" => "Diploma in Business Studies - Full time",
				    "6" => "Diploma in Business Studies - Part time",
				    "7" => "Advanced Diploma in Business Studies - Full time",
				    "9" => "Higher Diploma in Business Studies",
				    "10" => "Diploma in Banking & Finance - Full time",
				    "11" => "Diploma in Banking & Finance - Part time",
				    "12" => "Diploma in Law - Full time",
				    "13" => "Diploma in Law - Part time",
				    "14" =>" Diploma in Logistics & Supply Chain Management - Full time",
				    "15" => "Diploma in Logistics & Supply Chain Management - Part time",
				    "16" => "Advanced Diploma in Logistics & Supply Chain Management - Full time",
				    "17" => "Advanced Diploma in Logistics & Supply Chain Management - Part time",
				    "18"=> "Higher Diploma in Logistics & Supply Chain Management",
				    "19"=> "Diploma in Accounting & Finance - Full time",
				    "20" => "Diploma in Accounting & Finance - Part time",
				    "21" => "Advanced Diploma in Accounting & Finance - Full time",
				    "22"=> "Advanced Diploma in Accounting & Finance - Part time",
				    "23"=> "Higher Diploma in Accounting & Finance",
				    "24"=> "Diploma in International Hospitality Management - Full time",
				    "25" => "Diploma in International Hospitality Management - Part time",
				    "26"=> "Diploma in Applied Hospitality Skills - Full time",
				    "27" => "Diploma in Applied Hospitality Skills - Part time",
				    "28"=> "Advanced Diploma in Hospitality and Tourism Management - Full time",
				    "29" => "Advanced Diploma in Hospitality and Tourism Management - Part time",
				    "30"=> "Higher Diploma in Hospitality and Tourism Management - Full time",
				    "31"=> "Bachelor of Science (Honours) Banking Practice and Management",
				    "32"=> "Bachelor of Business (Management & Innovation) (Top-Up)",
				    "33" => "Bachelor of Business (Accounting) (Top-Up)",
				    "34"=> "Bachelor of Business (Financial Risk Management) (Top-Up)",
				    "35"=> "Higher Diploma in Logistics & Supply Chain Management - Part time",
				    "36"=> "Bachelor of Business - Supply Chain & Logistics Management (Top-Up) - Full time",
				    "37" => "Bachelor of Business - Supply Chain &Logistics Management (Top-Up) - Part time",
				    "38" => "Bachelor of Arts (Hons) Accounting and Finance - Full-Time",
				    "39" => "Bachelor of Arts (Hons) Accounting and Finance - Part-Time",
				    "40"=> "Advanced Diploma in Data Analytics - Full Time",
				    "41" =>" Advanced Diploma in Data Analytics - Part Time"
				   );


		$nmims_city = Array
			        (
					  "1"=>"Goa",
					  "2"=>"Ahmedabad",
					  "3"=>"Anand",
					  "4"=>"Bangalore",
					  "5"=>"Bhopal",
					  "6"=>"Bhubaneswar",
					  "7"=>"Chandigarh",
					  "8"=>"Delhi-NCR",
					  "9"=>"Hyderabad",
					  "10"=>"Indore",
					  "11"=>"Jaipur",
					  "12"=>"Jodhpur",
					  "13"=>"Kolkata",
					  "14"=>"Lucknow",
					  "15"=>"Mohali",
					  "16"=>"Mumbai",
					  "17"=>"Nagpur",
					  "18"=>"Nashik",
					  "19"=>"Panchkula",
					  "20"=>"Pune",
					  "21"=>"Surat",
					  "22"=>"Udaipur",
					  "23"=>"Vadodara",
					 
					);	

			$medlife_time_slot = array(
								"1" => "06 am-07 am",
								"2" => "07 am-08 am",
								"3" => "09 am-10 am",
								"4" => "10 am-11 am",
								"5" => "11 am-12 pm",
								"6" => "12 pm-01 pm",
								"7" => "01 pm-02 pm",
								"8" => "02 pm-03 pm",
								"9" => "03 pm-04 pm",
							    "10" => "04 pm-05 pm",
								"11" => "05 pm-06 pm",
								
							);
			
			$winmore_grade = array (	
								'1' => "LKG",
								'2' => "UKG",
								'3' => "Grade-1",
				   			    '4' => "Grade-2",
								'5' => "Grade-3",
								'6' => "Grade-4",
								'7' => "Grade-5",
								'8' => "Grade-6",
				   			    '9' => "Grade-7",
								/* '10' => "8 std",
								'11' => "9 std",
								'12' => "10 std", */
								'13' =>"playhome",
								);
		
		$fire_limit_arr = array (	
								'1' => "10%",
								'2' => "20%",
								'3' => "30%",
				   			    '4' => "40%",
								'5' => "50%",
								'6' => "60%",
								'7' => "70%",
								'8' => "80%",
				   			    '9' => "90%",
								'10' => "100%",
								);	


		$nutratimes_state_arr = array(	
							"1"	=>	"Maharashtra",
							"2"	=>	"Karnataka",
							"3"	=>	"Tamil Nadu",
							"4"	=>	"Delhi",
							"5"	=>	"Andra Pradesh",
							"6"	=>	"Arunachal Pradesh",
							"7"	=>	"Assam",
							"8"	=>	"Andaman and Nicobar Islands",
							"9"	=>	"Bihar",
							"10"=>	"Chandigarh",
							"11"=>	"Chhattisgarh",
							"12"=>	"Dadar and Nagar Haveli",
							"13"=>	"Daman and Diu",
							"14"=>	"Goa",
							"15"=>	"Gujarat",
							"16"=>	"Haryana",
							"17"=>	"Himachal Pradesh",
							"18"=>	"Jammu and Kashmir",
							"19"=>	"Jharkhand",
							"20"=>	"Kerala",
							"21"=>	"Lakshadeep",
							"22"=>	"Madya Pradesh",
							"23"=>	"Manipur",
							"24"=>	"Meghalaya",
							"25"=>	"Mizoram",
							"26"=>	"Nagaland",
							"27"=>	"Orissa",
							"28"=>	"Punjab",
							"29"=>	"Pondicherry",
							"30"=>	"Rajasthan",
							"31"=>	"Sikkim",
							"32"=>	"Tripura",
							"33"=>	"Uttaranchal",
							"34"=>	"Uttar Pradesh",
							"35"=>	"West Bengal",
							"36"=>	"Telangana");

		$nutatimes_product 	 =  array(	
									"1" =>array("quantity" => "1",
									 "discount" => "20%",
									 "amount" => "2499",
									 "discount_amount" => "500",
									 "total_amount" => "1999"
									),
									"2" =>array("quantity" => "2",
									 "discount" => "30%",
									 "amount" => "4998",
									 "discount_amount" => "1499",
									 "total_amount" => "3499"
									),
									"3" =>array("quantity" => "6",
									 "discount" => "60%",
									 "amount" => "14994",
									 "discount_amount" => "8995",
									 "total_amount" => "5999"
									),
									"4" =>array("quantity" => "4",
									 "discount" => "50%",
									 "amount" => "9996",
									 "discount_amount" => "4997",
									 "total_amount" => "4999"
									),
								    
															
                               
         						);


		$realestate_clients = array( 
			                    "1" =>"sterlingascentia",
								"2" =>"gcorptheicon",
								"3" =>"galleriaresidences",
								"4" => "mittalelanza",
								"5" => "lgcl_newlife",
								"6" => "lgcl_highstreet",
								"7" => "lgcl_pueblo",
								"8" => "maya_indradhanush",
								"9" => "tata",
								"10"=>"tatapromont",
								"11"=> "brigade",
							  );

		$policymagnifier_cities= array(
                                 "1"=> "Allahabad",
                                 "2"=> "Aurangabad",
                                 "3"=> "Bangalore",
                                 "4"=> "Bareilly",
                                 "5"=> "Bhopal",
                                 "6"=> "Calicut",
                                 "7"=> "Chandigarh",
                                 "8"=> "Chennai",
                                 "9"=> "Cochin",
                                 "10"=> "Cochin/Kochi/Ernakulam",
                                 "11"=> "Coimbatore",
                                 "12"=> "Cuttack",
                                 "13"=> "Delhi",
                                 "14"=> "Dhanbad",
                                 "15"=> "Gurgaon",
                                 "16"=> "Guwahati",
                                 "17"=> "Hyderabad",
                                 "18"=> "Indore",
                                 "19"=> "Jabalpur",
                                 "20"=> "Jalandhar",
                                 "21"=> "Kanpur",
                                 "22"=> "Kolkata",
                                 "23"=> "Lucknow","24"=> "Ludhiana",
                                 "25"=> "Madurai",
                                 "26"=> "Mangalore",
                                 "27"=> "Mumbai",
                                 "28"=> "Nagpur",
                                 "29"=> "Nashik",
                                 "30"=> "Pune",
                                 "31"=> "Raipur",
                                 "32"=> "Ranchi",
                                 "33"=> "Surat",
                                 "34"=> "Thiruvananthapuram",
                                 "35"=> "Thrissur",
                                 "36"=> "Tiruchirappalli",
                                 "37"=> "Vadodara",
                                 "38"=> "Vapi",
                                 "39"=> "Vijaywada",
                                 "40"=> "Visakhapatnam",

		                         );


		$insurance_clients = array( 
			                    "1" =>"policy_magnifier",
			                    "2" =>"maxlife",
			                    "3" =>"maxbupa",
			                    "4" =>"getinsured",
			                );

		$universalprime_projects = array( 
			                    "1" =>"creek_edge_dubai",
			                    "2" =>"lg_20_upr",
			                    "3" =>"lg_18_emaar",
			                    "4" =>"mohammed_bin_rashid_city",
			                    "5" =>"reginatowers",
			                );

		$alliance_city = Array
			        (
					  "Goa"=>"Goa",
					  "Ahmedabad"=>"Ahmedabad",
					  "Anand"=>"Anand",
					  "Bangalore"=>"Bangalore",
					  "Bhopal"=>"Bhopal",
					  "Bhubaneswar"=>"Bhubaneswar",
					  "Chandigarh"=>"Chandigarh",
					  "Delhi-NCR"=>"Delhi-NCR",
					  "Hyderabad"=>"Hyderabad",
					  "Indore"=>"Indore",
					  "Jaipur"=>"Jaipur",
					  "Jodhpur"=>"Jodhpur",
					  "Kolkata"=>"Kolkata",
					  "Lucknow"=>"Lucknow",
					  "Mohali"=>"Mohali",
					  "Mumbai"=>"Mumbai",
					  "Nagpur"=>"Nagpur",
					  "Nashik"=>"Nashik",
					  "Panchkula"=>"Panchkula",
					  "Pune"=>"Pune",
					  "Surat"=>"Surat",
					  "Udaipur"=>"Udaipur",
					  "Vadodara"=>"Vadodara",
					);	
$alliance_state_arr = array(	
							"Maharashtra"	=>	"Maharashtra",
							"Karnataka"	=>	"Karnataka",
							"Tamil Nadu"	=>	"Tamil Nadu",
							"Delhi"	=>	"Delhi",
							"Andra Pradesh"	=>	"Andra Pradesh",
							"Arunachal Pradesh"	=>	"Arunachal Pradesh",
							"Assam"	=>	"Assam",
							"Andaman and Nicobar Islands"	=>	"Andaman and Nicobar Islands",
							"Bihar"	=>	"Bihar",
							"Chandigarh"=>	"Chandigarh",
							"Chhattisgarh"=>	"Chhattisgarh",
							"Dadar and Nagar Haveli"=>	"Dadar and Nagar Haveli",
							"Daman and Diu"=>	"Daman and Diu",
							"Goa"=>	"Goa",
							"Gujarat"=>	"Gujarat",
							"Haryana"=>	"Haryana",
							"Himachal Pradesh"=>	"Himachal Pradesh",
							"Jammu and Kashmir"=>	"Jammu and Kashmir",
							"Jharkhand"=>	"Jharkhand",
							"Kerala"=>	"Kerala",
							"Lakshadeep"=>	"Lakshadeep",
							"Madya Pradesh"=>	"Madya Pradesh",
							"Manipur"=>	"Manipur",
							"Meghalaya"=>	"Meghalaya",
							"Mizoram"=>	"Mizoram",
							"Nagaland"=>	"Nagaland",
							"Orissa"=>	"Orissa",
							"Punjab"=>	"Punjab",
							"Pondicherry"=>	"Pondicherry",
							"Rajasthan"=>	"Rajasthan",
							"Sikkim"=>	"Sikkim",
							"Tripura"=>	"Tripura",
							"Uttaranchal"=>	"Uttaranchal",
							"Uttar Pradesh"=>	"Uttar Pradesh",
							"West Bengal"=>	"West Bengal",
							"Telangana"=>	"Telangana");

		$main_arr['CITY_ARR']         = $city;						
		$main_arr['CALLER_STATUS']    = $caller_status;	
		$main_arr['DATE_TYPE']        = $date_cat;
		$main_arr['CLIENTS_NAME']     = $clients_arr;
		$main_arr['SRL_TIME']         = $srl_time_slot;
		$main_arr['SRL_TEST']         = $srl_test;
		$main_arr['SRL_TEST_NEW']     = $srl_test_new;		
		//$main_arr['SRL_TEST_DESC']    = $srl_test_details;
		$main_arr['ITOKYO_TEST_DESC'] = "";  
		$main_arr['JUBINATION_PKG']   = $jubination_pkg;
		$main_arr['JUBINATION_SLOT']  =	$jubi_time_slot;
		$main_arr['ITOKYO_TEST']      =	"";
		$main_arr['MEDLIFE_TEST']     =	$medlife_test;
		$main_arr['PURAVANKARA_TIME'] = $puravankara_freedom_time;
		$main_arr['PURAVA_BLUE_COUNTRY'] =$puravankara_bluemont_country;
		$main_arr['PURAVA_BLUE_COUNTRY_OTHERS'] =$puravankara_bluemont_country_others;
		$main_arr['RELIGARE_CITY']    =	$religare_city;
		$main_arr['ETOKIO_PLAN']      =	$etokio_plan;
		$main_arr['ETOKIO_AGE']       =	$etokio_age;
		$main_arr['LP_NAME']          = $lp_name;
		$main_arr['LSBF_COURCE']      = $lsbf_cource;
		$main_arr['NMIMS_CITY']       = $nmims_city;
		$main_arr['MEDLIFE_TIME']     = $medlife_time_slot;		
		$main_arr['WINMORE_GRADE']    = $winmore_grade;
		$main_arr['FIRE_LIMIT']       = $fire_limit_arr;
		$main_arr['NUTRATIMES_STATE_ARR']    = $nutratimes_state_arr;
		$main_arr['NUTRTATIMES_PRODUCT']     = $nutatimes_product;
		$main_arr['REALESTATE_CLIENTS']      = $realestate_clients;
		$main_arr['POLICYMAFNIFER_CITIES']   = $policymagnifier_cities;
		$main_arr['INSURANCE_CLIENTS']       = $insurance_clients;
		$main_arr['UNIVERSALPRIME_CLIENTS']  = $universalprime_projects; 
		$main_arr['ALLIANCE_CITY']       = $alliance_city;
		$main_arr['ALLIANCE_STATE_ARR']    = $alliance_state_arr;
		
		return $main_arr;					
	}
	
function get_random_chracter($chars_min=6, $chars_max=8, $use_upper_case=false, $include_numbers=false, $include_special_chars=false) {
	$length = rand($chars_min, $chars_max);
	$selection = 'aeuoyibcdfghjklmnpqrstvwxz';
	if($include_numbers) {
		$selection .= "1234567890";
	}
	if($include_special_chars) {
		$selection .= "!@04f7c318ad0360bd7b04c980f950833f11c0b1d1quot;#$%&[]{}?|";
	}
							
	$password = "";
	for($i=0; $i<$length; $i++) {
		$current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];            
		$password .=  $current_letter;
	}
	return $password;
}	
  }
  
  
function __serialize__($str) {
 return json_encode($str);
}

function __unserialize__($str) {
 return json_decode($str);
}

function object_to_array($arr) {
    if (is_object($arr)) {
        $arr = get_object_vars($arr);
    }

    if (is_array($arr)) {
        return array_map(__FUNCTION__, $arr);
    } else {
        return $arr;
    }
}

function array_to_object($arr) {
 json_decode (json_encode ($arr), FALSE);
}  

function inMultiArray($needle,$heystack) {
    if (array_key_exists($needle,$heystack) or in_array($needle,$heystack)) {
             return true;
        } else {
            $return = false;
            foreach (array_values($heystack) as $value) {
                if (is_array($value) and !$return) {
                    $return = inMultiArray($needle,$value);
                }
            }
            return $return;
        }
    }

if (! function_exists("array_lower")) {
	function array_lower($arr) {
		if(is_array($arr)) {
			foreach ($arr as $k => $v) {
				if(is_array($v)) {
					$new_arr[$k] = array_lower($v);
				} else {
					$new_arr[$k] = strtolower($v);
				}
			}
		}
		return $new_arr; 
	}
}	


if(! function_exists('date_dropdown')){
   function date_dropdown($date_type="", $from_date="", $to_date = ""){
	   
	   $date_val = "";
	   $sql = "";
	   if($date_type == 1){
			$date_val =  date('Y-m-d');
			$sql = "  DATE_FORMAT(date_created, '%Y-%m-%d') = '".$date_val."'";				 

		}else if($date_type == 2){
			$date_val =  "CURDATE() - INTERVAL 1 DAY" ;
			$sql = "  DATE_FORMAT(date_created, '%Y-%m-%d') = ".$date_val;				 

		}else if($date_type == 3){
			$date_val =  "CURDATE() - INTERVAL 7 DAY";
			$sql = "  DATE_FORMAT(date_created, '%Y-%m-%d') >= ".$date_val;				 

		}else if($date_type == 4){
			$date_val =  date('Y-m');
			$sql = "  DATE_FORMAT(date_created, '%Y-%m') = '".$date_val."'";

		}else if($date_type == 5){
			//$sql =  "  MONTH(date_created) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)" ;
			
			$sql =  " YEAR(date_created) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(date_created) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ";
		
		}else if($date_type == 6){
			$date_val =  "date_format(now() - interval 6 month,'%Y-%m')";
			$sql = "  DATE_FORMAT(date_created, '%Y-%m') >= ".$date_val;

		}else if($date_type == 7){
			$date_val =  "";
			$sql = "1 = 1";
		}else if($date_type == 8){
			$sql .= " DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."'";
				
		}
						 
		return $sql;					
   }
}

if(! function_exists('all_param')){
	function all_param($url = ""){
		$pq['query'] = NULL;	
		$pq = parse_url($url);
		
		if( is_array($pq) && count($pq) > 1 && isset($pq['query']) ){
			$headr_param = $pq['query'];
			
			return $headr_param;
			
		}else{
			return false;
		}
	}
}

function _sendsms($mobile = '', $message = '', $sender = '') {
  // CHECK MOBILE NO IS VALID OR NOT
  if (isset($mobile) && trim($mobile) > 0 && strlen($mobile) == 10 && isset($message) && trim($message) != '') {
   
   // IS CURL INSTALLED YET
   if (!function_exists('curl_init')){
    die('Sorry cURL is not installed!');
   }
  
   //$url = 'http://smsalerts.adcanopus.com/api/v4/?api_key=Ac37abb04bc63b0c63f02539905422b45&method=sms&message='.urlencode($message).'&to=91'.$mobile.'&sender=MDLIFE&format=xml';

   //$url = 'http://smsalerts.adcanopus.com/api/v4/?api_key=Ac37abb04bc63b0c63f02539905422b45&method=sms&message='.urlencode($message).'&to=91'.$mobile.'&sender='.strtoupper($sender).'&format=xml';
   
   $url = '';

   // OK cool - then let's create a new cURL resource handle
   $ch = curl_init();
     
   // Set URL to download
   curl_setopt($ch, CURLOPT_URL, $url);
   
   // Should cURL return or print out the data? (true = return, false = print)
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   
   // Timeout in seconds
   curl_setopt($ch, CURLOPT_TIMEOUT, 60);
   
   // Download the given URL, and return output
   $output = curl_exec($ch);
    return $output;
   // Close the cURL resource, and free system resources
   curl_close($ch);
   
   if (strpos(strtolower($output), 'error') !== false) {
    // STORE SUCCESS LOG FOR THE REFERENCE
    return false;
    // SEND MAIL IF ERROR COMES FMOR SMSGUPSUP
   } else {
    // STORE SUCCESS LOG FOR THE REFERENCE
    return true;
   }
   
  }
 }

?>