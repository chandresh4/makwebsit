<?php
/*
 * @author boo2mark
 */
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Enquiry_Model extends CI_Model
{

    public $_tableName;
    
    public function __construct()
    {
        parent::__construct();
        $this->_tableName = "enquiries";
        $this->load->library("email");
        
    }
    
    public function submitEnquiryFrom()
    {
        
        $insertData = [];

        $clientsInfo = $this->getClientsInfo();
        $otpCode = $this->generateCode();

        if($_POST["utm_source"]=='contactus')
        {
            $business_type = '';
            $project = '';

        }else{
            $business_type = $_POST["business_type"];
            $project = $_POST["project"];
        }

        if( 
            $_POST["utm_source"]=='contactus' || 
            $_POST["utm_source"]=='enquire_now' || 
            $_POST["utm_source"]=='architects' || 
            $_POST["utm_source"]=='carpentry' ||
            $_POST["utm_source"]=='fabrication' || 
            $_POST["utm_source"]=='bathroom_renovation' ||
            $_POST["utm_source"]=='kitchen' || 
            $_POST["utm_source"]=='flooring'

        )
        {

            if ( $_POST["message"] ) {
                $message = $_POST["message"];
            }else {
                $message = "";
            }
        
            if(
                $_POST["utm_source"]=='architects' ||
                $_POST["utm_source"]=='carpentry' ||
                $_POST["utm_source"]=='architects' ||
                $_POST["utm_source"]=='fabrication' ||
                $_POST["utm_source"]=='bathroom_renovation' ||
                $_POST["utm_source"]=='kitchen' ||
                $_POST["utm_source"]=='flooring'
            )
            {
                $service = $_POST["service"];
                $location = $_POST["location"];
            }else{

                $service = "";            
                $location = "";            

            }

        }else{
                $message = "";            
                $service = "";            
                $location = "";            

        }

        $insertEnquiry = [
            "service"=>$service,
            "location"=>$location,
            "utm_source" => $_POST["utm_source"],
            "utm_term" => $_POST["utm_term"],
            "utm_campaign" => $_POST["utm_campaign"],
            "utm_content" => $_POST["utm_content"],
            "name" => $_POST["name"],
            "mobile_no" => $_POST["mobile_no"],
            "is_optin" => "no",
            "utm_medium" => $_POST["utm_medium"],
            "ad" => $_POST["ad"],
            "adpos" => $_POST["adpos"],
            "email" => $_POST["email"],
            "city" => $_POST["city"],
			"business_type" => $business_type,
			"project" => $project,
            "ip" => $this->getIp(),
            "browser" => $clientsInfo["name"]." - ".$clientsInfo["version"],
            "operating_system" => $clientsInfo["platform"],
            "status" => $this->getDefaultStatus(),
            "remarks" => "",
            "otp_code" => $otpCode,
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
            "message" => $message
        ];

        $this->db->insert( $this->_tableName , $insertEnquiry );
        $id = $this->db->insert_id();

        if ( intval($id) > 0 ) {

            if (
                $_POST["utm_source"]!='contactus' && 
                $_POST["utm_source"]!='enquire_now' &&  
                $_POST["utm_source"]!='architects' && 
                $_POST["utm_source"]!='carpentry' && 
                $_POST["utm_source"]!='architects' && 
                $_POST["utm_source"]!='fabrication' && 
                $_POST["utm_source"]!='bathroom_renovation' && 
                $_POST["utm_source"]!='kitchen' && 
                $_POST["utm_source"]!='flooring'
            ) { 
    
                $curl_restult = $this->sendOtp( $_POST["mobile_no"], $otpCode );

            }
        }

        return $id;
    }

    public function getDefaultStatus() {

        $result = $this->db->get_where("statuses", [
            "id" => 1
        ])->row_array();

        return $result["status"];
    }


    public function matchOtp () {

        $id = intval ( $this->input->post("id"));
        $otpCode = trim($this->input->post("otp"));

        $result = $this->db->get_where( $this->_tableName, [
            "id" => $id,
            "otp_code" => $otpCode
        ])->result_array();

        if ( sizeof($result) > 0 ) {

            $this->db->where("id", $id);
            $this->db->update( $this->_tableName, ["is_optin" => "yes"] );            

            $success = "success";
        }else {
            
            $success = "fail";
            $adminMailSent = false;
            $userMailSent = false;
        }

        echo json_encode( ["status" => $success] );
        exit;
    }


    public function resendOtp () {

        $id = intval ( $this->input->post("id"));
        $otpCode = $this->generateCode();

        $result = $this->db->get_where( $this->_tableName, [
            "id" => $id
        ])->row_array();

        if ( count($result) > 0 ) {

            $mobileNo = $result["mobile_no"];
            
            //if ( strpos ( $_SERVER["HTTP_HOST"], "localhost" ) === false ) {
                $isSent = $this->sendOtp($result["mobile_no"], $otpCode );
            //}

             $isSent = true;

            if ( $isSent ) {

                $this->db->where("id", $id);
                $this->db->update( $this->_tableName, ["otp_code" => $otpCode]);

            }
        }

        echo json_encode( ["status" => "success", "msg" => "Otp Sent to mobile number"] );
        exit;
    }


    public function generateCode() {
        if ( strpos ( $_SERVER["HTTP_HOST"], "localhost" ) !== false ) {

            return 1234;
        }else {
            $otp_token = rand(1000, 9999);
            return $otp_token;
        }
    }

    public function sendOtp ($mobile, $code) {

        // send OTP via API

        // CHECK MOBILE NO IS VALID OR NOT
        if (isset($mobile) && trim($mobile) > 0 && strlen($mobile) == 10) {
            
            // IS CURL INSTALLED YET
            if (!function_exists('curl_init')){
                die('Sorry cURL is not installed!');
            }
            
            $query = urlencode($code.' is your Smak verification code');


            $curl = curl_init();
            curl_setopt_array($curl, array(
              //CURLOPT_URL => 'http://bulksms.adcanopus.com/api/v2/sms/send?access_token=fa4fab03bd27420a88fbeb3b089d0bbe&message='.$query.'&sender=SMAKIN&to=91'.$mobile.'&service=T',
              CURLOPT_URL => 'http://api.kasplomobile.com/api/v1?access_token=0af3dccad94755c8fed8f3b0296d965ed9d0151f&message='.$query.'&sender=SMAKCO&to='.$mobile.'&service=OTP&entity_id=1001517489281405669&template_id=1007949483799058149',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
            ));
            $response = curl_exec($curl);

            return $response;
            // Close the cURL resource, and free system resources
            curl_close($ch);
        
            if (strpos(strtolower($output), 'error') !== false) {
                // STORE SUCCESS LOG FOR THE REFERENCE
                return false;
                // SEND MAIL IF ERROR COMES FMOR SMSGUPSUP
            } else {
                // STORE SUCCESS LOG FOR THE REFERENCE
                return true;
            }
            
        }
        //return true;
    }

    public function getIp () {

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    function getClientsInfo() { 

        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";
      
        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
          $platform = 'linux';
        }elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
          $platform = 'mac';
        }elseif (preg_match('/windows|win32/i', $u_agent)) {
          $platform = 'windows';
        }
      
        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)){
          $bname = 'Internet Explorer';
          $ub = "MSIE";
        }elseif(preg_match('/Firefox/i',$u_agent)){
          $bname = 'Mozilla Firefox';
          $ub = "Firefox";
        }elseif(preg_match('/OPR/i',$u_agent)){
          $bname = 'Opera';
          $ub = "Opera";
        }elseif(preg_match('/Chrome/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
          $bname = 'Google Chrome';
          $ub = "Chrome";
        }elseif(preg_match('/Safari/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
          $bname = 'Apple Safari';
          $ub = "Safari";
        }elseif(preg_match('/Netscape/i',$u_agent)){
          $bname = 'Netscape';
          $ub = "Netscape";
        }elseif(preg_match('/Edge/i',$u_agent)){
          $bname = 'Edge';
          $ub = "Edge";
        }elseif(preg_match('/Trident/i',$u_agent)){
          $bname = 'Internet Explorer';
          $ub = "MSIE";
        }
      
        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
      ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
          // we have no matching number just continue
        }
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
          //we will have two since we are not using 'other' argument yet
          //see if version is before or after the name
          if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
              $version= $matches['version'][0];
          }else {
              $version= $matches['version'][1];
          }
        }else {
          $version= $matches['version'][0];
        }
      
        // check if we have a number
        if ($version==null || $version=="") {$version="?";}
      
        return array(
          'userAgent' => $u_agent,
          'name'      => $bname,
          'version'   => $version,
          'platform'  => $platform,
          'pattern'    => $pattern
        );
    } 
      

    public function renderStatusEditModal() {

        $id = $_POST["id"];
        
        $data = $this->db->get_where( $this->_tableName, [
            "id" => $id
        ])->row_array();

        $data["statusLists"] = $this->db->get("statuses")->result_array();
        $data["id"] = $id;
        $data["enquiry"] = $data;

        $this->load->view("admin/status-edit-modal", $data);

    }

    public function saveEnquiry() {

        $id = $this->input->post("id");
        $remarks = $this->input->post("remarks");
        $status = $this->input->post("status");

        $this->db->where("id", $id);
        $this->db->update( $this->_tableName, ["status" => $status, "remarks" => $remarks, "updated_at" => date("Y-m-d H:i:s") ]);
    }

    public function renderGroupByFields ( $dbFieldName, $optionName = '' ) {

        if ( $dbFieldName == 'status' ) {

            $this->db->select( "DISTINCT ( status ) as group_field" );

            $result = $this->db->get( "statuses" )->result_array();

        }else {

            $this->db->select( "DISTINCT ( $dbFieldName ) as group_field" );
            $this->db->where( " $dbFieldName !=  '' ");
    
            $result = $this->db->get( $this->_tableName )->result_array();
        }

        $options  = '<select multiple class="form-control '.$dbFieldName.'-select2-field filter-fields '.$dbFieldName.' " data-field-name="'.$dbFieldName.'" name="'.$dbFieldName.'[]">';
        $options .= '<option value="">'.$optionName.'</option>';

        foreach ( $result as $each ) {

            if ( isset($_GET["filter_".$dbFieldName]) ) {

                $filterData = explode(",", $_GET["filter_".$dbFieldName]);
            }else {
                $filterData = [];
            }

            if (  in_array ( $each["group_field"], $filterData ) ) {

                $selected = "selected";
            }else {
                $selected = "";
            }

            $options .= '<option value="'.$each["group_field"].'" '.$selected.'>'.$each["group_field"].'</option>'; 
        }

        $options .= '</select>';

        return $options;
    }

    public function getSelectedFields () {

        if ( isset($_GET["selectFields"]) ) {
            $selectFields = explode(",", $_GET["selectFields"]);
        }else {
            $selectFields = "";
        }

        return [ "selectFields" => $selectFields ];
    } 


    public function getEnquiryData ( $returnCount, $perPage, $startLimit ) {

        $selectFields = "";
        if ( isset($_GET["selectFields"]) && strlen($_GET["selectFields"]) > 0 ) {

            $selectFields = $_GET["selectFields"];
            $dateRange = $_GET["date_range"];
            $calculation = $_GET["calculation"];
            $duration = $_GET["duration"];
            $listFields = $this->db->list_fields( $this->_tableName );

            if ( $dateRange == 'today' ) {

                $start = date("Y-m-d")." 00:00:00";
                $end = date("Y-m-d")." 23:59:59";
            }

            
            if ( $dateRange == 'yesterday' ) {

                $start = date('Y-m-d', strtotime("-1 days"))." 00:00:00";
                $end =   date('Y-m-d', strtotime("-1 days"))." 23:59:59";
            }

            if ( $dateRange == 'this_month' ) {

                $start = date("Y-m-01")." 00:00:00";
                $end =   date("Y-m-30")." 23:59:59";
            }

            
            if ( $dateRange == 'custom_month' ) {

                $start = $_GET["start_date"]." 00:00:00";
                $end =   $_GET["end_date"]." 23:59:59";
            }

            if ( $dateRange == 'last_month' ) {

                $start = date('Y-m-d', strtotime('first day of last month'))." 00:00:00";
                $end = date('Y-m-d', strtotime('last day of last month'))." 23:59:59";
            }
            
            foreach ( $_GET as $key => $value ) {

                if ( $key == 'filter_search' && strlen($value) > 0 ) {

                    $value = preg_replace('/[^a-z0-9]/i', '', $value);

                    $searchQuery = "(  name LIKE '%".$value."%' OR email LIKE '%".$value."%' OR mobile_no LIKE '%".$value."%' OR status LIKE '%".$value."%' )";
                    $this->db->where( $searchQuery );

                } else if ( strpos( $key, "filter_" ) !== false ) {

                    $fieldName = str_replace("filter_", "", $key);

                    if ( strlen($value) > 0  && in_array( $fieldName, $listFields )) {

                        $combineValue = explode(",", $value);
                        $allItems = [];

                        foreach ( $combineValue as $eachValue ) {

                            $allItems[] = "'".$eachValue."'";
                        }

                        $this->db->where( "$fieldName IN (".implode(",", $allItems).")");
                    }

                }
            }

            $this->db->where(" ( created_at <= '$end' AND  created_at >='$start') ");
           

            if ( $calculation == '0' ) {

                $selectFields = $selectFields.",id";

                $this->db->select($selectFields);
                $this->db->from( $this->_tableName ); 

            }else {

                if ( $duration == 'hour' ) {

                    $groupByFields = "HOUR( created_at ), day(created_at),".$selectFields;
                    $selectFields = "date_format( created_at, '%Y-%m-%d %I:00 %p' ) as duration,".$selectFields;
                }

                if ( $duration == 'date' ) {

                    $groupByFields = "DAY( created_at ),".$selectFields;
                    $selectFields = "date_format( created_at, '%Y-%m-%d' ) as duration,".$selectFields;
                }

                if ( $duration == 'month' ) {

                    $groupByFields = "month( created_at ),".$selectFields;
                    $selectFields = "date_format( created_at, '%Y-%m' ) as duration,".$selectFields;
                }

                $this->db->select($selectFields.", count(id) as leads");
                $this->db->from( $this->_tableName );
                $this->db->group_by( $groupByFields );

            }

            if ( in_array( $_GET["sort_field"], $listFields ) ) {

                $this->db->order_by( $_GET["sort_field"]  , $_GET[$_GET["sort_field"]."_sort_direction"] );
            }else if ( $_GET["sort_field"] == 'duration' ) {

                $this->db->order_by("created_at", $_GET["duration_sort_direction"]);
            }else if ( $_GET["sort_field"] == 'leads' ) {

                $this->db->order_by("count(id)", $_GET["leads_sort_direction"]);
            }else {
                if ( $calculation == '0' ) {

                    $this->db->order_by("id", "desc");
                }else {
                    $this->db->order_by("created_at", "desc");
                }
               
            }

            if ( $returnCount ) {

                $result = $this->db->count_all_results();
            }else {
                $this->db->limit ( $perPage, $startLimit );
                $result = $this->db->get()->result_array();
            }
  
            return $result;

        }else {

            if ( $returnCount ) {

                return 0;
            }else {
                return [];
            }
        }
        
    }

    public function checkPreviousRecord () {

        if ( sizeof($_GET) == '0' ) {

            $userId = $this->session->userdata("user_id");

            $result = $this->db->get_where("enquiry_filter_history", [
                "user_id" => $userId
            ])->result_array();
    
            if ( sizeof($result) > 0 ) {
    
                redirect("admin/enquiries/listing?".$result[0]["search_history"]);
            }
        }

    }

    public function updateReports() {

        $searchParams = $this->input->post("searchParams");
        $userId = $this->session->userdata("user_id");

        if ( intval($userId) > 0 ) {

            $result = $this->db->get_where("enquiry_filter_history", [
                "user_id" => $userId
            ])->result_array();
    
            if ( sizeof($result) > 0 ) {
    
                $this->db->where("id", $result[0]["id"] );
                $this->db->update( "enquiry_filter_history", ["search_history" => $searchParams]);
            }else {
    
                $insertData = [
                    "user_id" => $userId,
                    "search_history" => $searchParams
                ];
    
                $this->db->insert( "enquiry_filter_history", $insertData );
            }
        }

    }

    public function getDashboardData() {

        $this->db->from("enquiries");
        $totalLead = $this->db->count_all_results();

        $this->db->where("status", $this->getDefaultStatus());

        $this->db->from("enquiries");
        $totalPendingLead = $this->db->count_all_results();

        $lastBeforeDays = 7;
        $renderLastDay = $lastBeforeDays - 1;

        $start = date('Y-m-d', strtotime("-".$renderLastDay." days"))." 00:00:00";
        $end   = date('Y-m-d')." 23:59:59";

        $this->db->where(" ( created_at <= '$end' AND  created_at >='$start') ");

        $groupByFields = "DAY( created_at ),";
        $selectFields = "date_format( created_at, '%d %b %Y' ) as duration";

        $this->db->select($selectFields.", count(id) as total");
        $this->db->from( $this->_tableName );
        $this->db->group_by( $groupByFields );
        $this->db->order_by("created_at", "asc");

        $result = $this->db->get()->result_array();

        $allDatesLists = [];

        foreach ( $result as $eachItem ) {

            $allDatesLists[ $eachItem["duration"] ] = $eachItem["total"];
        }

        // make sure the array has last 7 days data

        $allItems = [];

        for ( $count = 0; $count < $lastBeforeDays; $count++ ) {

            $day = 3600*24* $count ;

            $date = strtotime($start) + $day;
            $dateFormat = date( "d M Y", $date);

            if ( !isset ( $allDatesLists[ $dateFormat ] ) ) {

                $allItems[ $dateFormat ] = 0;
            }else {
                $allItems[ $dateFormat ] = $allDatesLists[ $dateFormat ];
            }
        }

        $allLabels = [];
        $allValues = [];

        foreach ( $allItems as $key => $value ) {

            $allLabels[] = "'".$key."'";
            $allValues[] = $value;
        }

        return [
            "totalLead" => $totalLead,
            "totalPendingLead" => $totalPendingLead,
            "allLabels" => "[".implode(",", $allLabels)."]",
            "allValues" => "[".implode(",", $allValues)."]",
        ];
    }
	
    public function get_enquiry_details( $id = 0 ){ //get the enquiry details
        
        $this->db->select('*');
        $this->db->from('enquiries');
        $this->db->where('mobile_no', $_POST['mobile_no']);
        $this->db->where('business_type', $_POST['business_type']);
		$this->db->where('project', $_POST['project']);
        //$this->db->where('is_optin', "yes");

        $res = $this->db->get()->result();

        return $res;
    }

    public function get_enquiry_details_by_id( $id = 0 ){ //get the enquiry details
        
        $this->db->select('*');
        $this->db->from('enquiries');
        $this->db->where('id', $id );

        $res = $this->db->get()->result();

        return $res;
    }

    public function send_mail_user($data){

        $renderData["name"] = $data[0]->name;
        $message = $this->load->view('emails/user_signup', $renderData, TRUE);

        // prepare email
        $subject = "Thank you for your Concern with Smak Concepts";    
        $from = "no-reply@smakconcepts.com";
        $fromName = "Smak Interiors";
        $to = $data[0]->email;

        $this->email->from($from, $fromName);
        $this->email->to( $to );
        $this->email->subject($subject);
        $this->email->message($message);    
            
        // send email
        $isSent = $this->email->send();

        return $this->email->print_debugger();
    }

    public function send_mail_admin($data){

        $message = $this->load->view('emails/admin_email_template', $data[0], TRUE);
        
        $business_type = $data[0]->business_type;
        $user_primary_id = $data[0]->id;

        if($business_type=='Interiors'){

            // prepare email
            $subject = "Smak Interiors User Enquiry #".$user_primary_id;    
            $from = "no-reply@smakconcepts.com";
            $fromName = "Smak Interiors";

        }else{

            // prepare email
            $subject = "Smak Concepts User Enquiry #".$user_primary_id;   
            $from = "no-reply@smakconcepts.com";
            $fromName = "Smak Concepts";

        }

        $to = "shivaji@smakconcepts.com,anil@adcanopus.com,vinodhtamilvanan@adcanopus.com";
        //$to = "anil@adcanopus.com,shaikh@adcanopus.com,vinodhtamilvanan@adcanopus.com";

        $this->email->from($from, $fromName);
        $this->email->to($to);
        //$this->email->addCustomHeader("X-GreenArrow-MailClass: smakconcepts");
        $this->email->subject($subject);
        $this->email->message($message);    
            
        // send email
        $isSent = $this->email->send();

        return $this->email->print_debugger();
    }

    
    public function send_mail_admin_contact($data){

        $message = $this->load->view('emails/admin_contact_email_template', $data[0], TRUE);
        
        $business_type = $data[0]->business_type;
        $user_primary_id = $data[0]->id;

        if($business_type=='Interiors'){

            // prepare email
            $subject = "Smak Interiors User Enquiry #".$user_primary_id;    
            $from = "no-reply@smakconcepts.com";
            $fromName = "Smak Interiors";

        }else{

            // prepare email
            $subject = "Smak Concepts User Enquiry #".$user_primary_id;   
            $from = "no-reply@smakconcepts.com";
            $fromName = "Smak Concepts";

        }

        $to = "shivaji@smakconcepts.com,anil@adcanopus.com,vinodhtamilvanan@adcanopus.com";
        //$to = "anil@adcanopus.com,shaikh@adcanopus.com,vinodhtamilvanan@adcanopus.com";

        $this->email->from($from, $fromName);
        $this->email->to($to);
        //$this->email->addCustomHeader("X-GreenArrow-MailClass: smakconcepts");
        $this->email->subject($subject);
        $this->email->message($message);    
            
        // send email
        $isSent = $this->email->send();

        return $this->email->print_debugger();
    }
  public function architect_email_template($data){

        $message = $this->load->view('emails/architect_email_template', $data[0], TRUE);
        
        $business_type = $data[0]->business_type;
        $user_primary_id = $data[0]->id;

        // prepare email
        $subject = "Smak Concepts User Enquiry #".$user_primary_id;   
        $from = "no-reply@smakconcepts.com";
        $fromName = "Smak Concepts";


        //$to = "shivaji@smakconcepts.com,anil@adcanopus.com,vinodhtamilvanan@adcanopus.com";
        $to = "vinodhtamilvanan@adcanopus.com";

        $this->email->from($from, $fromName);
        $this->email->to($to);
        //$this->email->addCustomHeader("X-GreenArrow-MailClass: smakconcepts");
        $this->email->subject($subject);
        $this->email->message($message);    
            
        // send email
        $isSent = $this->email->send();

        return $this->email->print_debugger();
    }
    
}